galileo\.agents\.deterministic package
======================================

Submodules
----------

galileo\.agents\.deterministic\.dummy module
--------------------------------------------

.. automodule:: galileo.agents.deterministic.dummy
    :members:
    :undoc-members:
    :show-inheritance:

galileo\.agents\.deterministic\.macd module
-------------------------------------------

.. automodule:: galileo.agents.deterministic.macd
    :members:
    :undoc-members:
    :show-inheritance:

galileo\.agents\.deterministic\.threshold module
------------------------------------------------

.. automodule:: galileo.agents.deterministic.threshold
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: galileo.agents.deterministic
    :members:
    :undoc-members:
    :show-inheritance:
