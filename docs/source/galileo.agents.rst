galileo\.agents package
=======================

Subpackages
-----------

.. toctree::

    galileo.agents.deterministic
    galileo.agents.rl

Module contents
---------------

.. automodule:: galileo.agents
    :members:
    :undoc-members:
    :show-inheritance:
