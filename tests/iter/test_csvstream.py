import csv
import os
import unittest

import numpy as np
import pandas as pd
import pytest

from galileo.core import DataBreak
from galileo.iter import CSVStreamer


def create_test_csv_file(rows):
    with open('test.csv', 'w+') as csvfile:
        csvwriter = csv.writer(csvfile)
        for row in rows:
            csvwriter.writerow(row)


def delete_test_csv_file():
    os.remove("test.csv")


class TestCSVStreamer(unittest.TestCase):

    def test_init(self):
        rows = [
            ['a', 'b', 'c', 'd', 'e'],
            [1, 2, 3, 4, 2],
            [1, 2, 3, 4, 1],
            [1, 2, 3, 4, 0],
        ]
        create_test_csv_file(rows)
        expected_result = [1, 2, 3, 4, 2]
        streamer = CSVStreamer(filename='test.csv',
                               header=0,
                               price_cols=['c', 'd'],
                               timer_col='e')
        self.assertTrue(
            np.all(np.isclose(streamer.next(), expected_result, atol=1e-5)))

        delete_test_csv_file()

    def test_index(self):
        rows = [
            ['in', 'a', 'b', 'c', 'd', 'e'],
            ['rowname', 1, 2, 3, 4, 2],
            ['rowname', 1, 2, 3, 4, 1],
            ['rowname', 1, 2, 3, 4, 0],
        ]
        create_test_csv_file(rows)
        expected_result = [1, 2, 3, 4, 2]
        streamer = CSVStreamer(filename='test.csv',
                               header=0,
                               price_cols=['c', 'd'],
                               timer_col='e',
                               index_col='in')
        self.assertTrue(
            np.all(np.isclose(streamer.next(), expected_result, atol=1e-5)))

        delete_test_csv_file()

    def test_index_no_header(self):
        rows = [
            ['rowname', 1, 2, 3, 4, 2],
            ['rowname', 1, 2, 3, 4, 1],
            ['rowname', 1, 2, 3, 4, 0],
        ]
        create_test_csv_file(rows)
        expected_result = [1, 2, 3, 4, 2]
        streamer = CSVStreamer(filename='test.csv',
                               header=None,
                               price_cols=[3, 4],
                               timer_col=5,
                               index_col=0)
        self.assertTrue(
            np.all(np.isclose(streamer.next(), expected_result, atol=1e-5)))

        delete_test_csv_file()

    def test_price_timer_header(self):
        rows = [
            ['a', 'b', 'c', 'd', 'e'],
            [1, 2, 3, 4, 3],
            [5, 6, 7, 8, 2],
            [9, 10, 11, 12, 1],
            [13, 14, 15, 16, 0],
            [17, 18, 19, 20, 3],
            [21, 22, 23, 24, 2],
            [25, 26, 27, 28, 1],
            [29, 30, 31, 32, 0],
        ]
        create_test_csv_file(rows)
        csvstreamer = CSVStreamer(filename='test.csv', header=0, price_cols=[
            'a', 'b', 'c', 'd'], timer_col='e')
        will_break = False
        i = 0
        for row in rows:
            if i == 0:
                i += 1
                continue  # Header
            expected_result = np.array(rows[i])
            if will_break:
                with self.assertRaises(DataBreak):
                    csvstreamer.next()
                i -= 1
            else:
                self.assertTrue(
                    np.all(np.isclose(csvstreamer.next(), expected_result, atol=1e-5)))
            will_break = expected_result[-1] == 0
            i += 1
        delete_test_csv_file()

    def test_price_timer_header_col_exchange(self):
        rows = [
            ['a', 'b', 'c', 'e', 'd'],
            [1, 2, 3, 3, 4],
            [5, 6, 7, 2, 8],
            [9, 10, 11, 1, 12],
            [13, 14, 15, 0, 16],
            [17, 18, 19, 3, 20],
            [21, 22, 23, 2, 24],
            [25, 26, 27, 1, 28],
            [29, 30, 31, 0, 32],
        ]
        create_test_csv_file(rows)
        csvstreamer_1 = CSVStreamer(filename='test.csv', header=0, price_cols=[
                                    'a', 'b', 'c', 'd'], timer_col='e')
        will_break = False
        i = 0
        for row in rows:
            if i == 0:
                i += 1
                continue  # Header
            expected_result = np.array(rows[i])
            if will_break:
                with self.assertRaises(DataBreak):
                    csvstreamer_1.next()
                i -= 1
            else:
                self.assertTrue(
                    np.all(np.isclose(csvstreamer_1.next(), expected_result, atol=1e-5)))
            will_break = expected_result[3] == 0
            i += 1
        delete_test_csv_file()

    def test_price_timer(self):
        rows = [
            [1, 2, 3, 4, 3],
            [5, 6, 7, 8, 2],
            [9, 10, 11, 12, 1],
            [13, 14, 15, 16, 0],
            [17, 18, 19, 20, 3],
            [21, 22, 23, 24, 2],
            [25, 26, 27, 28, 1],
            [29, 30, 31, 32, 0],
        ]
        create_test_csv_file(rows)
        csvstreamer_0 = CSVStreamer(filename='test.csv', header=None, price_cols=[
                                    0, 1, 2, 3], timer_col=4)
        will_break = False
        i = 0
        for row in rows:
            expected_result = np.array(rows[i])
            if will_break:
                with self.assertRaises(DataBreak):
                    csvstreamer_0.next()
                i -= 1
            else:
                self.assertTrue(
                    np.all(np.isclose(csvstreamer_0.next(), expected_result, atol=1e-5)))
            will_break = expected_result[-1] == 0
            i += 1
        delete_test_csv_file()

    def test_price_header(self):
        rows = [
            ['a', 'b', 'c', 'd'],
            [1, 2, 3, 4],
            [5, 6, 7, 8],
            [9, 10, 11, 12],
            [13, 14, 15, 16],
            [17, 18, 19, 20],
            [21, 22, 23, 24],
            [25, 26, 27, 28],
            [29, 30, 31, 32],
        ]
        create_test_csv_file(rows)
        csvstreamer_1 = CSVStreamer(filename='test.csv', header=0, price_cols=[
                                    'a', 'b', 'c', 'd'])
        i = 0
        for row in rows:
            if i == 0:
                i += 1
                continue  # Header
            expected_result = np.array(rows[i] + [len(rows) - i - 1])
            result = csvstreamer_1.next()
            self.assertTrue(
                np.all(np.isclose(result, expected_result, atol=1e-5)))
            i += 1
        delete_test_csv_file()

    @pytest.mark.filterwarnings('ignore:price_cols unspecified.')
    def test_timer_header(self):
        rows = [
            ['a', 'b', 'c', 'd', 'e'],
            [1, 2, 3, 4, 3],
            [5, 6, 7, 8, 2],
            [9, 10, 11, 12, 1],
            [13, 14, 15, 16, 0],
            [17, 18, 19, 20, 3],
            [21, 22, 23, 24, 2],
            [25, 26, 27, 28, 1],
            [29, 30, 31, 32, 0],
        ]
        create_test_csv_file(rows)
        csvstreamer_1 = CSVStreamer(
            filename='test.csv', header=0, price_cols=['a', 'b', 'c', 'd'], timer_col='e')
        will_break = False
        i = 0
        for row in rows:
            if i == 0:
                i += 1
                continue  # Header
            expected_result = np.array(rows[i])
            if will_break:
                with self.assertRaises(DataBreak):
                    csvstreamer_1.next()
                i -= 1
            else:
                self.assertTrue(
                    np.all(np.isclose(csvstreamer_1.next(), expected_result, atol=1e-5)))
            will_break = expected_result[-1] == 0
            i += 1
        delete_test_csv_file()

    @pytest.mark.filterwarnings('ignore:price_cols unspecified.')
    def test_timer(self):
        rows = [
            [1, 2, 3, 4, 3],
            [5, 6, 7, 8, 2],
            [9, 10, 11, 12, 1],
            [13, 14, 15, 16, 0],
            [17, 18, 19, 20, 3],
            [21, 22, 23, 24, 2],
            [25, 26, 27, 28, 1],
            [29, 30, 31, 32, 0],
        ]
        create_test_csv_file(rows)
        csvstreamer_0 = CSVStreamer(filename='test.csv', header=None, price_cols=[
                                    0, 1, 2, 3], timer_col=4)
        will_break = False
        i = 0
        for row in rows:
            expected_result = np.array(rows[i])
            if will_break:
                with self.assertRaises(DataBreak):
                    csvstreamer_0.next()
                i -= 1
            else:
                self.assertTrue(
                    np.all(np.isclose(csvstreamer_0.next(), expected_result, atol=1e-5)))
            will_break = expected_result[-1] == 0
            i += 1
        delete_test_csv_file()

    @pytest.mark.filterwarnings('ignore:price_cols unspecified.')
    def test_header(self):
        rows = [
            ['a', 'b', 'c', 'd'],
            [1, 2, 3, 4],
            [5, 6, 7, 8],
            [9, 10, 11, 12],
            [13, 14, 15, 16],
            [17, 18, 19, 20],
            [21, 22, 23, 24],
            [25, 26, 27, 28],
            [29, 30, 31, 32],
        ]
        create_test_csv_file(rows)
        csvstreamer_0 = CSVStreamer(filename='test.csv', price_cols=[
                                    'a', 'b', 'c', 'd'], header=0, index_col=None)
        i = 0
        for row in rows:
            if i == 0:
                i += 1
                continue  # Header
            expected_result = np.array(rows[i] + [len(rows) - i - 1])
            self.assertTrue(
                np.all(np.isclose(csvstreamer_0.next(), expected_result, atol=1e-5)))
            i += 1
        delete_test_csv_file()

    def test_price(self):
        rows = [
            [1, 2, 3, 4],
            [5, 6, 7, 8],
            [9, 10, 11, 12],
            [13, 14, 15, 16],
            [17, 18, 19, 20],
            [21, 22, 23, 24],
            [25, 26, 27, 28],
            [29, 30, 31, 32],
        ]
        create_test_csv_file(rows)
        csvstreamer_0 = CSVStreamer(
            filename='test.csv', price_cols=[0, 1, 2, 3], header=None, index_col=None)
        i = 0
        for row in rows:
            expected_result = np.array(rows[i] + [len(rows) - i - 1])
            self.assertTrue(
                np.all(np.isclose(csvstreamer_0.next(), expected_result, atol=1e-5)))
            i += 1
        delete_test_csv_file()


if __name__ == '__main__':
    unittest.main()
