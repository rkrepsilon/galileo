import unittest
import pytest

import numpy as np
from galileo.iter import TriangularWalk, WavySignal


class TestDeterministic(unittest.TestCase):
    @pytest.mark.filterwarnings('ignore:End of data reached. Rewinding.')
    def test_wavy_signal_no_timer(self):
        length = 10
        ws = WavySignal(period_1=10,
                        period_2=15,
                        epsilon=0.1,
                        length=length,
                        add_timer=False,
                        ba_spread=0.1)
        for _ in range(length):
            val = ws.next()
            self.assertEqual(len(val), 2)
        with self.assertRaises(StopIteration):
            val = ws.next()

    def test_wavy_signal_with_timer(self):
        length = 10
        ws = WavySignal(period_1=10,
                        period_2=15,
                        epsilon=0.1,
                        length=length,
                        add_timer=True,
                        ba_spread=0.1)
        for i in reversed(range(length)):
            val = ws.next()
            self.assertEqual(len(val), 3)
            self.assertEqual(val[2], i)

    def test_triangular_walk_no_timer(self):
        length = 11
        tw = TriangularWalk(nsteps=5,
                            length=length,
                            add_timer=False)
        val = tw.next()
        assert all(val == np.array([0, 0]))
        val = tw.next()
        assert all(val == np.array([0.5, 0.5]))
        val = tw.next()
        assert all(val == np.array([1, 1]))
        val = tw.next()
        assert all(val == np.array([0.5, 0.5]))
        val = tw.next()
        assert all(val == np.array([0, 0]))
        val = tw.next()
        assert all(val == np.array([-0.5, -0.5]))
        val = tw.next()
        assert all(val == np.array([-1, -1]))
        val = tw.next()
        assert all(val == np.array([-0.5, -0.5]))
        val = tw.next()
        assert all(val == np.array([0, 0]))
        val = tw.next()
        assert all(val == np.array([0.5, 0.5]))
        val = tw.next()
        assert all(val == np.array([1, 1]))

    def test_triangular_walk_with_timer(self):
        length = 10
        tw = TriangularWalk(nsteps=5,
                            length=length,
                            add_timer=True)
        for i in reversed(range(length)):
            val = tw.next()
            self.assertEqual(len(val), 3)
            self.assertEqual(val[2], i)


if __name__ == '__main__':
    unittest.main()
