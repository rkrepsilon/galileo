import unittest
import pytest

import numpy as np
from galileo.iter import AR1, BoundedRandomWalk, RandomWalk


class TestStochastic(unittest.TestCase):

    def timerlogic(self, iterator, length, expected_size_output, has_timer):
        for i in reversed(range(length)):
            val = iterator.next()
            if has_timer:
                self.assertEqual(len(val), expected_size_output + 1)
                self.assertEqual(val[-1], i)
            else:
                self.assertEqual(len(val), expected_size_output)
        with self.assertRaises(StopIteration):
            val = iterator.next()

    @pytest.mark.filterwarnings('ignore:End of data reached. Rewinding.')
    def test_random_walk_no_timer(self):
        length = 10
        add_timer = False
        rw = RandomWalk(length=length,
                        add_timer=add_timer,
                        ba_spread=0.1)
        self.timerlogic(rw, length, 2, add_timer)

    @pytest.mark.filterwarnings('ignore:End of data reached. Rewinding.')
    def test_random_walk_with_timer(self):
        length = 10
        add_timer = True
        rw = RandomWalk(length=length,
                        add_timer=add_timer,
                        ba_spread=0.1)
        self.timerlogic(rw, length, 2, add_timer)

    def test_ar1_stats(self):
        length = 200000
        add_timer = False
        ar = AR1(a=0.1,
                 length=length,
                 add_timer=add_timer,
                 ba_spread=0.1)
        vals = [ar.next()[0] for i in range(length)]
        mean = np.mean(vals)
        std = np.std(vals)
        assert np.isclose(mean, 0, atol=0.02)
        assert np.isclose(std, 1, atol=0.01)

    @pytest.mark.filterwarnings('ignore:End of data reached. Rewinding.')
    def test_ar1_output_no_timer(self):
        length = 10
        add_timer = False
        ar = AR1(a=0.1,
                 length=length,
                 add_timer=add_timer,
                 ba_spread=0.1)
        self.timerlogic(ar, length, 2, add_timer)

    @pytest.mark.filterwarnings('ignore:End of data reached. Rewinding.')
    def test_ar1_output_with_timer(self):
        length = 10
        add_timer = True
        ar = AR1(a=0.1,
                 length=length,
                 add_timer=add_timer,
                 ba_spread=0.1)
        self.timerlogic(ar, length, 2, add_timer)

    def test_bounded_rw_stats(self):
        length = 200
        add_timer = True
        brw = BoundedRandomWalk(length=length,
                                add_timer=True,
                                nsteps=10)
        vals = [brw.next()[0] for i in range(length)]
        assert all(map(lambda x: (x >= -1) & (x <= 1), vals))

    @pytest.mark.filterwarnings('ignore:End of data reached. Rewinding.')
    def test_bounded_rw_output_no_timer(self):
        length = 200
        add_timer = True
        brw = BoundedRandomWalk(length=length,
                                add_timer=True,
                                nsteps=10)
        self.timerlogic(brw, length, 2, add_timer)

    @pytest.mark.filterwarnings('ignore:End of data reached. Rewinding.')
    def test_bounded_rw_output_with_timer(self):
        length = 200
        add_timer = True
        brw = BoundedRandomWalk(length=length,
                                add_timer=True,
                                nsteps=10)
        self.timerlogic(brw, length, 2, add_timer)


if __name__ == '__main__':
    unittest.main()
