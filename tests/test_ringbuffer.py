import unittest

import numpy as np
from galileo.ringbuffer import RingBuffer

res_1 = np.array([0, 1, 2, 3], dtype=float)
res_2 = np.array([4, 5, 6, 7, 8, 9], dtype=float)


class TestRingBuffer(unittest.TestCase):

    def test_ordered(self):
        myrb = RingBuffer(6)
        for i in range(4):
            myrb.append(i)
        self.assertTrue(np.array_equal(myrb.ordered(), res_1))
        myrb = RingBuffer(6)
        for i in range(10):
            myrb.append(i)
        self.assertTrue(np.array_equal(myrb.ordered(), res_2))

    def test_str(self):
        myrb = RingBuffer(6)
        for i in range(4):
            myrb.append(i)
        repr(myrb)
        myrb = RingBuffer(6)
        for i in range(10):
            myrb.append(i)
        repr(myrb)

    def test_getitem_before_full_cycle(self):
        myrb = RingBuffer(6)
        for i in range(4):
            myrb.append(i)
        self.assertAlmostEqual(myrb[0], res_1[0])
        self.assertAlmostEqual(myrb[-1], res_1[-1])
        with self.assertRaises(IndexError):
            myrb[6]
        self.assertTrue(np.array_equal(myrb[:], res_1))
        self.assertTrue(np.array_equal(myrb[2:4], res_1[2:4]))
        self.assertTrue(np.array_equal(myrb[2:], res_1[2:]))
        self.assertTrue(np.array_equal(myrb[1:-1], res_1[1:-1]))
        self.assertTrue(np.array_equal(myrb[:-1], res_1[:-1]))
        self.assertTrue(np.array_equal(myrb[-3:], res_1[-3:]))
        self.assertTrue(np.array_equal(myrb[-4:], res_1[-4:]))
        self.assertTrue(np.array_equal(myrb[-5:], res_1[-5:]))
        self.assertTrue(np.array_equal(myrb[-3:-1], res_1[-3:-1]))
        self.assertTrue(np.array_equal(myrb[-4:-1], res_1[-4:-1]))
        self.assertTrue(np.array_equal(myrb[-5:-1], res_1[-5:-1]))
        self.assertTrue(np.array_equal(myrb[-3:2], res_1[-3:2]))
        self.assertTrue(np.array_equal(myrb[-1:2], res_1[-1:2]))
        self.assertTrue(np.array_equal(myrb[1:5:2], res_1[1:5:2]))
        self.assertTrue(np.array_equal(myrb[::2], res_1[::2]))
        self.assertTrue(np.array_equal(myrb[::1], res_1[::1]))
        self.assertTrue(np.array_equal(myrb[::3], res_1[::3]))
        self.assertTrue(np.array_equal(myrb[::-1], res_1[::-1]))
        self.assertTrue(np.array_equal(myrb[4:1:-1], res_1[4:1:-1]))

    def test_getitem_after_full_cycle(self):
        myrb = RingBuffer(6)
        for i in range(10):
            myrb.append(i)
        self.assertAlmostEqual(myrb[0], 4)
        self.assertAlmostEqual(myrb[1], 5)
        self.assertAlmostEqual(myrb[2], 6)
        self.assertAlmostEqual(myrb[3], 7)
        self.assertAlmostEqual(myrb[4], 8)
        self.assertAlmostEqual(myrb[5], 9)
        with self.assertRaises(IndexError):
            myrb[6]
        self.assertAlmostEqual(myrb[-1], 9)
        self.assertAlmostEqual(myrb[-2], 8)
        self.assertAlmostEqual(myrb[-3], 7)
        self.assertAlmostEqual(myrb[-4], 6)
        self.assertAlmostEqual(myrb[-5], 5)
        self.assertAlmostEqual(myrb[-6], 4)
        self.assertTrue(np.array_equal(myrb[:], res_2[:]))
        self.assertTrue(np.array_equal(myrb[2:5], res_2[2:5]))
        self.assertTrue(np.array_equal(myrb[2:], res_2[2:]))
        self.assertTrue(np.array_equal(myrb[2:-1], res_2[2:-1]))
        self.assertTrue(np.array_equal(myrb[:-1], res_2[:-1]))
        self.assertTrue(np.array_equal(myrb[:-4], res_2[:-4]))
        self.assertTrue(np.array_equal(myrb[-3:-1], res_2[-3:-1]))
        self.assertTrue(np.array_equal(myrb[-4:-1], res_2[-4:-1]))
        self.assertTrue(np.array_equal(myrb[-8:-1], res_2[-8:-1]))
        self.assertTrue(np.array_equal(myrb[-3:2], res_2[-3:2]))
        self.assertTrue(np.array_equal(myrb[-1:2], res_2[-1:2]))
        self.assertTrue(np.array_equal(myrb[-20:], res_2[-20:]))
        self.assertTrue(np.array_equal(myrb[2:6:3], res_2[2:6:3]))
        self.assertTrue(np.array_equal(myrb[::2], res_2[::2]))
        self.assertTrue(np.array_equal(myrb[::1], res_2[::1]))
        self.assertTrue(np.array_equal(myrb[::3], res_2[::3]))
        self.assertTrue(np.array_equal(myrb[::-1], res_2[::-1]))
        self.assertTrue(np.array_equal(myrb[5:2:-1], res_2[5:2:-1]))
        self.assertTrue(np.array_equal(myrb[6:2:-1], res_2[6:2:-1]))
        self.assertTrue(np.array_equal(myrb[6::-1], res_2[6::-1]))

    def test_setitem_after_full_cycle(self):
        myrb = RingBuffer(6)
        for i in range(10):
            myrb.append(i)
        myrb[0] = 1
        res_2[0] = 1
        self.assertTrue(np.array_equal(myrb[:], res_2))
        myrb[1] = 2
        res_2[1] = 2
        self.assertTrue(np.array_equal(myrb[:], res_2))
        with self.assertRaises(NotImplementedError):
            myrb[0:2] = [10, 11]
        with self.assertRaises(NotImplementedError):
            myrb[3:1:-1] = [0, 1]

    def test_setitem_before_full_cycle(self):
        myrb = RingBuffer(6)
        for i in range(3):
            myrb.append(i)
        myrb[0] = 1
        self.assertTrue(list(myrb) == [1, 1, 2])

    def test_ringbuffer_zero(self):
        myrb = RingBuffer(0)
        for i in range(3):
            myrb.append(i)
        self.assertTrue(np.array_equal(list(myrb), np.empty(0)))
        self.assertTrue(np.array_equal(myrb.ordered(), np.empty(0)))


if __name__ == '__main__':

    unittest.main()
