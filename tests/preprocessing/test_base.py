import unittest
from copy import copy

import numpy as np

import pandas as pd
import pytest
from galileo.preprocessing import *

states = np.array([
    [1., 2., 3., 4.],
    [2., 3., 4., 5.],
    [3., 4., 5., 6.],
    [4., 5., 6., 7.],
    [5., 6., 7., 8.],
    [6., 7., 8., 9.],
    [7., 8., 9., 10.],
    [8., 9., 10., 11.],
    [9., 10., 11., 12.],
    [10., 11., 12., 13.]
])


def assertArrEq(x, y):
    assert np.isclose(x, y).all()


class TestArithmetic(unittest.TestCase):

    def test_identity_addition(self):
        pp1 = Identity()
        pp2 = Identity()

        pp3 = pp1 + pp2
        self.assertIsInstance(pp3, Preprocessor)

        self.assertIsInstance(pp3.transform(states[0]), np.ndarray)
        assertArrEq(pp3.transform(states[0]), states[0] * 2)

    def test_identity_subtraction(self):
        pp1 = Identity()
        pp2 = Identity()

        pp3 = pp1 - pp2
        self.assertIsInstance(pp3, Preprocessor)

        self.assertIsInstance(pp3.transform(states[0]), np.ndarray)
        assertArrEq(pp3.transform(states[0]), states[0] * 0)

    def test_square(self):
        pp1 = Identity()
        pp2 = Identity()

        pp3 = pp1 * pp2
        self.assertIsInstance(pp3, Preprocessor)

        self.assertIsInstance(pp3.transform(states[0]), np.ndarray)
        assertArrEq(pp3.transform(states[0]), states[0] ** 2)

    def test_two_minus_one(self):
        pp1 = Composer([Identity(), Lambda(lambda x: 2 * x)])
        pp2 = Identity()
        pp3 = pp1 - pp2

        assertArrEq(pp3.transform(states[0]), states[0])

    def test_identity_division(self):
        pp1 = Identity()
        pp2 = Identity()

        pp3 = pp1 / pp2

        self.assertIsInstance(pp3, Preprocessor)
        self.assertIsInstance(pp3.transform(states[0]), np.ndarray)
        assertArrEq(pp3.transform(states[0]), np.ones(states[0].shape,
                                                      dtype=np.float32))

    def test_div(self):
        pp1 = Identity() + Identity()
        pp2 = Identity()

        pp3 = pp1 / pp2

        assertArrEq(pp3.transform(states[0]), 2.0 * np.ones(states[0].shape,
                                                            dtype=np.float32))

    def test_break_for_sizes_not_matching(self):
        pp1 = Chainer([Identity(), Identity()])
        pp2 = Identity()
        pp3 = pp1 - pp2
        with self.assertRaises(ValueError):
            pp3.transform(states[0])

    def test_break_for_broadcasting(self):
        pp1 = Identity()  # shape (4,)
        pp2 = Lambda(lambda x: np.squeeze(x[0]))  # shape (1,)

        pp3 = pp1 + pp2
        with self.assertRaises(ValueError):
            pp3.transform(states[0])

        # whereas this should work
        np1 = states[0]
        np2 = np.squeeze(states[0][0])
        np3 = np1 + np2

        self.assertEqual(np2.shape, ())
        self.assertEqual(np3.shape, (4,))
        assertArrEq(np3, states[0] + ([1.] * 4))


class TestIdentity(unittest.TestCase):

    def test_transform(self):
        preprocessor = Identity()
        for state in states:
            assertArrEq(state, preprocessor.transform(state))

    def test_init(self):
        preprocessor = Identity()
        self.assertEqual(preprocessor.continuous, False)
        self.assertEqual(preprocessor.calibrated_init, True)
        self.assertEqual(preprocessor.steps, 0)
        self.assertTrue(preprocessor.multi_episode)


class TestLambda(unittest.TestCase):

    @staticmethod
    def fun(x):
        return x**2

    @staticmethod
    def fun_steps(x):
        return np.sum(x, axis=0)

    def calibrate_with_tests(self, preprocessor, steps):
        for i in range(steps):
            with self.assertRaises(ValueError):
                preprocessor.transform(states[i])
            preprocessor.calibrate(states[i])
        self.assertTrue(preprocessor.calibrated)
        self.assertEqual(preprocessor._calibration_steps_count, steps)

    def output_tests_when_steps_calibrated(self, preprocessor,
                                           steps, continuous=False):
        self.assertTrue(preprocessor.calibrated)
        assertArrEq(np.array(preprocessor.calibration_state), states[:steps])
        for i in range(len(states) - steps):
            preprocessor.calibrate(states[steps + i])
            if continuous:
                assertArrEq(
                    np.array(preprocessor.calibration_state), states[i + 1:steps + 1])
                assertArrEq(preprocessor.transform(
                    states[steps + i]), sum(states[i: steps + i + 1]))
            else:
                assertArrEq(
                    np.array(preprocessor.calibration_state), states[:steps])
                assertArrEq(preprocessor.transform(
                    states[steps + i]), sum(states[: steps]) + states[steps + i])

    def test_non_continuous_with_output(self):
        preprocessor = Lambda(fun=self.fun, continuous=False, steps=0)
        for state in states:
            assertArrEq(state**2, preprocessor.transform(state))

    def test_continuous_must_have_steps(self):
        with self.assertRaises(ValueError):
            preprocessor = Lambda(fun=self.fun, continuous=True, steps=0)

    def test_output_non_continuous_with_steps(self):
        steps = 2
        preprocessor = Lambda(fun=self.fun_steps,
                              continuous=False,
                              steps=steps)
        self.calibrate_with_tests(preprocessor, steps)
        for i in range(steps, len(states)):
            self.assertTrue(preprocessor.calibrated)
            assertArrEq(states[i] + np.array([3, 5, 7, 9]),
                        preprocessor.transform(states[i]))
            preprocessor.calibrate(states[i])

    def test_reset_for_no_multi_episode(self):
        steps = 5
        preprocessor = Lambda(fun=self.fun_steps,
                              continuous=False,
                              steps=steps)
        self.calibrate_with_tests(preprocessor, steps)
        preprocessor.reset()
        self.assertFalse(preprocessor.calibrated)
        self.assertEqual(preprocessor._calibration_steps_count, 0)

    def test_reset_with_multi_episode(self):
        steps = 7
        preprocessor = Lambda(fun=self.fun_steps,
                              continuous=False,
                              steps=steps,
                              multi_episode=True)
        self.calibrate_with_tests(preprocessor, steps)
        preprocessor.reset()
        self.assertTrue(preprocessor.calibrated)
        self.assertEqual(preprocessor._calibration_steps_count, steps)
        preprocessor.reset()
        self.assertTrue(preprocessor.calibrated)
        self.assertEqual(preprocessor._calibration_steps_count, steps)

    def test_non_continuous_with_steps_and_reset(self):
        steps = 5
        continuous = False
        preprocessor = Lambda(fun=self.fun_steps,
                              continuous=continuous,
                              steps=steps)
        self.calibrate_with_tests(preprocessor, steps)
        preprocessor.reset()
        self.assertFalse(preprocessor.calibrated)
        self.assertEqual(preprocessor._calibration_steps_count, 0)
        self.calibrate_with_tests(preprocessor, steps)
        self.output_tests_when_steps_calibrated(preprocessor,
                                                steps, continuous)

    def test_continuous_with_steps(self):
        preprocessor = Lambda(fun=self.fun_steps, continuous=True, steps=2)
        for i in range(len(states)):
            if preprocessor.calibrated:
                assertArrEq(np.sum(
                    states[i - 2:i + 1], axis=0), preprocessor.transform(states[i]))
            preprocessor.calibrate(states[i])


class TestSelector(unittest.TestCase):
    def test_all_true(self):
        preprocessor = Selector(selection=[True] * 4)
        for state in states:
            assertArrEq(state,  preprocessor.transform(state))
        assertArrEq(preprocessor.mask, [True] * 4)

    def test_all_false(self):
        preprocessor = Selector(selection=[False] * 4)
        for state in states:
            self.assertTrue(len(preprocessor.transform(state)) == 0)
        assertArrEq(preprocessor.mask, [False] * 4)

    def test_mixed(self):
        preprocessor = Selector(selection=[True, True, True, False])
        for state in states:
            assertArrEq(state[:-1],  preprocessor.transform(state))
        assertArrEq(preprocessor.mask, [True, True, True, False])

    def test_inverted_mixed(self):
        preprocessor = Selector(
            selection=[True, True, True, False], invert=True)
        for state in states:
            assertArrEq(state[-1:],  preprocessor.transform(state))
        assertArrEq(preprocessor.mask, [False] * 3 + [True])

    def test_mixed_unordered(self):
        preprocessor = Selector(selection=[True, False, True, False])
        for state in states:
            assertArrEq(np.array(
                [state[0], state[2]]), preprocessor.transform(state))
        assertArrEq(preprocessor.mask, [True, False, True, False])

    def test_inveted_mixed_unordered(self):
        preprocessor = Selector(
            selection=[True, False, True, False], invert=True)
        for state in states:
            assertArrEq(np.array(
                [state[1], state[3]]), preprocessor.transform(state))
        assertArrEq(preprocessor.mask, [False, True, False, True])

    def test_selection(self):
        preprocessor = Selector(selection=[2, 3])
        for state in states:
            assertArrEq(np.array(
                [state[2], state[3]]), preprocessor.transform(state))

    def test_unordered_signed_selection(self):
        preprocessor = Selector(selection=[3, 2])
        with self.assertRaises(ValueError):
            preprocessor.transform(states[0])

    def test_inverted_selection(self):
        preprocessor = Selector(selection=[2, 3], invert=True)
        for state in states:
            assertArrEq(np.array(
                [state[0], state[1]]), preprocessor.transform(state))

    def test_signed_selection(self):
        preprocessor = Selector(selection=[-2, -1])
        for state in states:
            assertArrEq(np.array(
                [state[2], state[3]]), preprocessor.transform(state))

    def test_unordered_signed_selection(self):
        preprocessor = Selector(selection=[-1, -2])
        with self.assertRaises(ValueError):
            preprocessor.transform(states[0])

    def test_int_selection(self):
        preprocessor = Selector(selection=0)
        for state in states:
            self.assertTrue(state[0], preprocessor.transform(state))
        preprocessor = Selector(selection=-1)
        for state in states:
            self.assertTrue(state[-1], preprocessor.transform(state))


class TestComposer(unittest.TestCase):

    def test_single_preprocessor(self):
        preprocessors = [Lambda(fun=lambda x: x**2, continuous=False, steps=0)]
        preprocessor = compose(preprocessors)
        for state in states:
            assertArrEq(preprocessor.transform(state),
                        preprocessors[0].transform(state))

    def test_multiple_preprocessors_all_continuous(self):
        preprocessors = [
            Lambda(fun=lambda x: (x[0] + x[1])**2, continuous=True, steps=1),
            Lambda(fun=lambda x: x[1] - x[0], continuous=True, steps=1)
        ]
        preprocessor = compose(preprocessors)
        for i in range(len(states)):
            if preprocessor.calibrated:
                assertArrEq(preprocessor.transform(
                    states[i]), (states[i - 1] + states[i])**2 - (states[i - 2] + states[i - 1])**2)
            preprocessor.calibrate(states[i])

    def test_multiple_preprocessors_none_continuous(self):
        preprocessors = [
            Lambda(fun=lambda x: (x[0] + x[1])**2, continuous=False, steps=1),
            Lambda(fun=lambda x: x[1] - x[0], continuous=False, steps=1)
        ]
        preprocessor = compose(preprocessors)
        for i in range(len(states)):
            if preprocessor.calibrated:
                assertArrEq(preprocessor.transform(
                    states[i]), (states[0] + states[i])**2 - (states[0] + states[1])**2)
            preprocessor.calibrate(states[i])

    def test_multiple_preprocessors_mixed(self):
        preprocessors = [
            Lambda(fun=lambda x: x**2, continuous=False, steps=0),
            Lambda(fun=lambda x: x[0]**(1. / 2), continuous=False, steps=1)
        ]
        preprocessor = compose(preprocessors)
        for i in range(len(states)):
            if preprocessor.calibrated:
                assertArrEq(preprocessor.transform(states[i]), states[0])
            preprocessor.calibrate(states[i])

    def test_composing_continuous(self):
        preprocessor = compose(
            [
                Lambda(lambda x: x[1] - x[0], steps=1, continuous=True),
                Lambda(lambda x: np.log(x))
            ]
        )
        for state in states:
            if preprocessor.calibrated:
                self.assertTrue(
                    np.all(preprocessor.transform(state) == [0] * 4))
            preprocessor.calibrate(state)

    def test_composing_not_continuous(self):
        preprocessor = compose(
            [
                Lambda(lambda x: x[1] - x[0], steps=1, continuous=False),
                Lambda(lambda x: np.log(x))
            ]
        )
        for i, state in enumerate(states):
            if preprocessor.calibrated:
                self.assertTrue(
                    np.all(preprocessor.transform(state) == [np.log(i)] * 4))
            preprocessor.calibrate(state)

    def test_reset_with_no_multi_episode(self):
        steps = 7
        preprocessor = compose(
            [
                Lambda(lambda x: x[1] - x[0], steps=steps, continuous=False),
                Identity()
            ]
        )
        for i in range(len(states)):
            preprocessor.calibrate(states[i])
        preprocessor.reset()
        self.assertFalse(preprocessor.calibrated)
        self.assertEqual(preprocessor._calibration_steps_count, 0)
        for preproc in preprocessor.preprocessors:
            if preproc.steps > 0:
                self.assertFalse(preproc.calibrated)
                self.assertEqual(preproc._calibration_steps_count, 0)

    def test_reset_with_mixed_multi_episode(self):
        steps = 5
        preprocessor = compose(
            [
                Lambda(lambda x: x[1] - x[0], steps=steps -
                       2, continuous=False, multi_episode=True),
                Lambda(lambda x: x[1] - x[0], steps=2,
                       continuous=False, multi_episode=False)
            ]
        )
        self.assertEqual(preprocessor.steps, steps)
        for i in range(len(states)):
            preprocessor.calibrate(states[i])
        self.assertEqual(preprocessor._calibration_steps_count, steps)
        preprocessor.reset()
        self.assertFalse(preprocessor.calibrated)
        self.assertEqual(preprocessor._calibration_steps_count, steps - 2)
        for preproc in preprocessor.preprocessors:
            if preproc.multi_episode == False:
                self.assertFalse(preproc.calibrated)
                self.assertEqual(preproc._calibration_steps_count, 0)
            else:
                self.assertTrue(preproc.calibrated)
                self.assertEqual(preproc._calibration_steps_count, steps - 2)

    def test_resume_after_reset(self):
        steps = 5
        preprocessor = compose(
            [
                Lambda(lambda x: x[1] - x[0], steps=steps -
                       2, continuous=False, multi_episode=True),
                Lambda(lambda x: x[1] - x[0], steps=2,
                       continuous=False, multi_episode=False)
            ]
        )
        for i in range(len(states)):
            preprocessor.calibrate(states[i])
        preprocessor.reset()
        self.assertTrue(preprocessor.preprocessors[0].calibrated)
        for i in range(len(states)):
            if i < 2:
                self.assertFalse(preprocessor.preprocessors[1].calibrated)
                with self.assertRaises(ValueError):
                    preprocessor.transform(states[i])
            else:
                self.assertTrue(preprocessor.preprocessors[1].calibrated)
                preprocessor.transform(states[i])
            preprocessor.calibrate(states[i])


class TestDiff(unittest.TestCase):
    def test_order_1(self):
        preprocessor = diff(order=1)
        for i in range(len(states)):
            if preprocessor.calibrated:
                assertArrEq(preprocessor.transform(states[i]), [1] * 4)
            preprocessor.calibrate(states[i])

    def test_order_2(self):
        preprocessor = diff(order=2)
        for i in range(len(states)):
            if preprocessor.calibrated:
                assertArrEq(preprocessor.transform(states[i]), [0] * 4)
            preprocessor.calibrate(states[i])


class TestRollingMean(unittest.TestCase):

    def test_init(self):
        window_size = 30
        preprocessor = rolling_mean(window_size)
        self.assertEqual(
            preprocessor.calibration_state.maxlen, window_size - 1)
        prices = list(range(100))
        for price in prices[:window_size - 1]:
            preprocessor.calibrate(price)
            # self.assertEqual(preprocessor.calibration_state.index, price)
            # self.assertEqual(preprocessor.calibration_state.appends, price + 1)
        for price in prices[window_size - 1:]:
            preprocessor.calibrate(price)
            # self.assertEqual(preprocessor.calibration_state.index,
            # price % (window_size - 1))
            # self.assertEqual(
            # preprocessor.calibration_state.appends, window_size - 1)

    def test_order_1(self):
        preprocessor = rolling_mean()
        prices = list(range(120))
        for price in prices:
            expected_mean = np.mean(range(price - 59, price + 1))
            if preprocessor.calibrated:
                self.assertEqual(
                    preprocessor.transform(price), expected_mean)
            preprocessor.calibrate(price)


class TestChainer(unittest.TestCase):

    def test_single_preprocessor(self):
        preprocessors = [Lambda(fun=lambda x: x**2, continuous=False, steps=0)]
        preprocessor = chain(preprocessors)
        for state in states:
            assertArrEq(preprocessor.transform(state),
                        preprocessors[0].transform(state))

    def test_multiple_preprocessors_all_continuous(self):
        preprocessors = [
            Lambda(fun=lambda x: (x[0] + x[1])**2, continuous=True, steps=1),
            Lambda(fun=lambda x: x[1] - x[0], continuous=True, steps=1)
        ]
        preprocessor = chain(preprocessors)
        for i in range(len(states)):
            if preprocessor.calibrated:
                self.assertTrue(
                    all(preprocessor.transform(states[i]) ==
                        np.concatenate(
                            [
                                (states[i - 1] + states[i])**2,
                                states[i] - states[i - 1]
                            ]
                    )
                    )
                )
            preprocessor.calibrate(states[i])

    def test_multiple_preprocessors_none_continuous(self):
        preprocessors = [
            Lambda(fun=lambda x: (x[0] + x[1])**2, continuous=False, steps=1),
            Lambda(fun=lambda x: (x[0] + x[1]) **
                   (1. / 2), continuous=False, steps=1)
        ]
        preprocessor = chain(preprocessors)
        for i in range(len(states)):
            if preprocessor.calibrated:
                self.assertTrue(
                    all(preprocessor.transform(states[i]) ==
                        np.concatenate(
                            [
                                (states[0] + states[i])**2,
                                (states[0] + states[i])**(1. / 2.)
                            ]
                    )
                    )
                )
            preprocessor.calibrate(states[i])

    def test_multiple_preprocessors_mixed(self):
        preprocessors = [
            Lambda(fun=lambda x: (x[0] + x[1])**2, continuous=True, steps=1),
            Lambda(fun=lambda x: (x[0] + x[1])**2, continuous=False, steps=1)
        ]
        preprocessor = chain(preprocessors)
        for i in range(len(states)):
            if preprocessor.calibrated:
                self.assertTrue(
                    all(
                        preprocessor.transform(states[i]) ==
                        np.concatenate(
                            [
                                (states[i] + states[i - 1])**2,
                                (states[0] + states[i])**2
                            ]
                        )
                    )
                )
            preprocessor.calibrate(states[i])

    def test_chaining_continuous(self):
        preprocessor = chain(
            [
                Lambda(lambda x: x[1] - x[0], steps=1, continuous=True),
                Lambda(lambda x: np.log(x))
            ]
        )
        for state in states:
            if preprocessor.calibrated:
                self.assertTrue(
                    np.all(preprocessor.transform(state) == [1] * 4 + list(map(np.log, state))))
            preprocessor.calibrate(state)

    def test_chaining_not_continuous(self):
        preprocessor = chain(
            [
                Lambda(lambda x: x[1] - x[0], steps=1, continuous=False),
                Lambda(lambda x: np.log(x))
            ]
        )
        for i, state in enumerate(states):
            if preprocessor.calibrated:
                self.assertTrue(
                    np.all(preprocessor.transform(state) == [i] * 4 + list(map(np.log, state))))
            preprocessor.calibrate(state)

    def test_reset_with_no_multi_episode(self):
        steps = 7
        preprocessor = chain(
            [
                Lambda(lambda x: x[1] - x[0], steps=steps, continuous=False),
                Identity()
            ]
        )
        for i in range(len(states)):
            preprocessor.calibrate(states[i])
        preprocessor.reset()
        self.assertFalse(preprocessor.calibrated)
        self.assertEqual(preprocessor._calibration_steps_count, 0)
        for preproc in preprocessor.preprocessors:
            if preproc.steps > 0:
                self.assertFalse(preproc.calibrated)
                self.assertEqual(preproc._calibration_steps_count, 0)
            else:
                self.assertTrue(preproc.calibrated)
                self.assertEqual(preproc._calibration_steps_count, 0)

    def test_reset_with_mixed_multi_episode(self):
        not_multi_episode_steps = 1
        multi_episode_steps = 7
        preprocessor = chain(
            [
                Lambda(lambda x: x[1] - x[0],
                       steps=multi_episode_steps,
                       continuous=False,
                       multi_episode=True),
                Lambda(lambda x: x[1] - x[0],
                       steps=not_multi_episode_steps,
                       continuous=False,
                       multi_episode=False)
            ]
        )
        for i in range(len(states)):
            preprocessor.calibrate(states[i])
        preprocessor.reset()
        self.assertFalse(preprocessor.calibrated)
        self.assertEqual(preprocessor._calibration_steps_count, 6)
        for preproc in preprocessor.preprocessors:
            if preproc.multi_episode == False:
                self.assertFalse(preproc.calibrated)
                self.assertEqual(preproc._calibration_steps_count, 0)
            else:
                self.assertTrue(preproc.calibrated)
                self.assertEqual(preproc._calibration_steps_count, 7)

    def test_pursue_after_reset_mixed_not_multi_episode(self):
        not_multi_episode_steps = 1
        multi_episode_steps = 7
        preprocessor = chain(
            [
                Lambda(lambda x: x[1] - x[0],
                       steps=multi_episode_steps,
                       continuous=False,
                       multi_episode=True),
                Lambda(lambda x: x[1] - x[0],
                       steps=not_multi_episode_steps,
                       continuous=False,
                       multi_episode=False)
            ]
        )
        for i in range(len(states)):
            preprocessor.calibrate(states[i])
        preprocessor.reset()
        with self.assertRaises(ValueError):
            preprocessor.transform(states[0])
        preprocessor.calibrate(states[0])
        for i in range(1, len(states)):
            preprocessor.calibrate(states[i])
            preprocessor.transform(states[-1])

    def test_pursue_after_reset_no_multi_episode_different_steps(self):
        no_multi_episode_setps_1 = 2
        no_multi_episode_steps_2 = 3
        preprocessor = chain(
            [
                Lambda(lambda x: x[1] - x[0],
                       steps=no_multi_episode_setps_1,
                       continuous=False,
                       multi_episode=False),
                Lambda(lambda x: x[1] - x[0],
                       steps=no_multi_episode_steps_2,
                       continuous=False,
                       multi_episode=False)
            ]
        )
        for i in range(len(states)):
            preprocessor.calibrate(states[i])
        preprocessor.reset()
        with self.assertRaises(ValueError):
            preprocessor.transform(states[0])
        preprocessor.calibrate(states[0])
        preprocessor.calibrate(states[1])
        with self.assertRaises(ValueError):
            preprocessor.transform(states[0])
        preprocessor.calibrate(states[2])
        for i in range(3, len(states)):
            preprocessor.calibrate(states[i])
            preprocessor.transform(states[-1])

    def calibrate_chainer_with_tests(self, chainer, steps, expected_value=None):
        for i in range(steps):
            with self.assertRaises(ValueError):
                chainer.transform(states[i & len(states)])
            chainer.calibrate(states[i])
        self.assertTrue(chainer.calibrated)
        self.assertEqual(chainer._calibration_steps_count, chainer.steps)
        for preproc in chainer.preprocessors:
            self.assertTrue(chainer.calibrated)
            self.assertEqual(chainer._calibration_steps_count, chainer.steps)
        if expected_value is None:
            chainer.transform(states[0])
        else:
            assertArrEq(chainer.transform(states[0]), expected_value)

    def test_chainer_of_chainer_reset_with_no_multi_episode(self):
        steps1 = 2
        steps2 = 4
        preprocessor = chain(
            [
                Lambda(lambda x: x[2] - x[0], steps=steps1, continuous=False),
                Identity(),
                chain(
                    [
                        Identity(),
                        Lambda(lambda x: x[4] - x[0],
                               steps=steps2, continuous=False)
                    ]
                )
            ]
        )
        self.calibrate_chainer_with_tests(preprocessor, max([steps1, steps2]))
        preprocessor.reset()
        self.assertFalse(preprocessor.calibrated)
        self.assertFalse(preprocessor._calibration_steps_count, 0)
        for preproc in preprocessor.preprocessors:
            if not preproc.multi_episode:
                self.assertFalse(preproc.calibrated)
                self.assertEqual(preproc._calibration_steps_count, 0)
            else:
                self.assertTrue(preproc.calibrated)
                self.assertEqual(
                    preproc._calibration_steps_count, preproc.steps)
        self.calibrate_chainer_with_tests(preprocessor, max([steps1, steps2]))

    def test_chainer_of_chainer_reset_with_multi_episode(self):
        steps1 = 2
        steps2 = 4
        preprocessor = chain(
            [
                Lambda(lambda x: x[2] - x[0], steps=steps1, continuous=False),
                Identity(),
                chain(
                    [
                        Identity(),
                        Lambda(lambda x: x[4] - x[0], steps=steps2,
                               continuous=False, multi_episode=True)
                    ]
                )
            ]
        )
        self.calibrate_chainer_with_tests(preprocessor, max([steps1, steps2]))
        preprocessor.reset()
        self.assertFalse(preprocessor.calibrated)
        self.assertEqual(preprocessor._calibration_steps_count, steps1)
        for preproc in preprocessor.preprocessors:
            if not preproc.multi_episode:
                self.assertEqual(preproc.calibrated,
                                 preproc.calibrated_init)
                self.assertEqual(preproc._calibration_steps_count, 0)
            else:
                self.assertTrue(preproc.calibrated)
                self.assertEqual(
                    preproc._calibration_steps_count, preproc.steps)
        self.calibrate_chainer_with_tests(preprocessor, steps1)
        preprocessor = chain(
            [
                Lambda(lambda x: x[2] - x[0], steps=steps1, continuous=False),
                Identity(),
                chain(
                    [
                        Identity(),
                        Lambda(lambda x: x[4] - x[0], steps=steps2,
                               continuous=False, multi_episode=True)
                    ]
                )
            ]
        )
        self.assertFalse(preprocessor.calibrated)
        self.assertEqual(preprocessor._calibration_steps_count, 0)
        for preproc in preprocessor.preprocessors:
            if preproc.steps > 0:
                self.assertFalse(preproc.calibrated)
                self.assertEqual(preproc._calibration_steps_count, 0)
            else:
                self.assertTrue(preproc.calibrated)
        self.calibrate_chainer_with_tests(preprocessor, max([steps1, steps2]))

    def test_chainer_commutativity(self):

        preprocessor_1 = chain(
            [
                diff(order=1),
                diff(order=2)
            ]
        )

        preprocessor_2 = chain(
            [
                diff(order=2),
                diff(order=1)
            ]
        )

        for i, state in enumerate(states):
            if i == 2:
                self.assertTrue(preprocessor_1.calibrated)
                self.assertTrue(preprocessor_2.calibrated)
                p1_state = preprocessor_1.transform(state)
                p2_state = preprocessor_2.transform(state)
                p2_state_t = np.concatenate(
                    [p2_state[len(p2_state) / 2:], p2_state[:len(p2_state) / 2]])
                assertArrEq(p1_state, p2_state_t)
            preprocessor_1.calibrate(state)
            preprocessor_2.calibrate(state)


class TestStacker(unittest.TestCase):
    @staticmethod
    def stack(preprocessors):
        return Stacker(preprocessors, axis=0)

    def test_stacking_on_last_axis(self):
        # Modelled only onl test_stacking_not_continuous
        preprocessor = Stacker(
            [
                Lambda(lambda x: x[1] - x[0], steps=1, continuous=False),
                Lambda(lambda x: np.log(x))
            ],
            axis=-1
        )
        for i, state in enumerate(states):
            if preprocessor.calibrated:
                assertArrEq(preprocessor.transform(state),
                            np.stack([[i] * 4, map(np.log, state)], axis=-1)
                            )
            preprocessor.calibrate(state)

    def test_single_preprocessor(self):
        preprocessors = [Lambda(fun=lambda x: x**2, continuous=False, steps=0)]
        preprocessor = self.stack(preprocessors)
        for state in states:
            assertArrEq(preprocessor.transform(state),
                        preprocessors[0].transform(state))

    def test_multiple_preprocessors_all_continuous(self):
        preprocessors = [
            Lambda(fun=lambda x: (x[0] + x[1])**2, continuous=True, steps=1),
            Lambda(fun=lambda x: x[1] - x[0], continuous=True, steps=1)
        ]
        preprocessor = self.stack(preprocessors)
        for i in range(len(states)):
            if preprocessor.calibrated:
                assertArrEq(preprocessor.transform(states[i]), np.stack(
                    [(states[i - 1] + states[i])**2, states[i] - states[i - 1]], axis=0))
            preprocessor.calibrate(states[i])

    def test_multiple_preprocessors_none_continuous(self):
        preprocessors = [
            Lambda(fun=lambda x: (x[0] + x[1])**2, continuous=False, steps=1),
            Lambda(fun=lambda x: (x[0] + x[1]) **
                   (1. / 2), continuous=False, steps=1)
        ]
        preprocessor = self.stack(preprocessors)
        for i in range(len(states)):
            if preprocessor.calibrated:
                assertArrEq(preprocessor.transform(states[i]), np.stack(
                    [(states[0] + states[i])**2, (states[0] + states[i])**(1. / 2.)], axis=0))
            preprocessor.calibrate(states[i])

    def test_multiple_preprocessors_mixed(self):
        preprocessors = [
            Lambda(fun=lambda x: (x[0] + x[1])**2, continuous=True, steps=1),
            Lambda(fun=lambda x: (x[0] + x[1])**2, continuous=False, steps=1)
        ]
        preprocessor = self.stack(preprocessors)
        for i in range(len(states)):
            if preprocessor.calibrated:
                assertArrEq(preprocessor.transform(states[i]), np.stack(
                    [(states[i] + states[i - 1])**2, (states[0] + states[i])**2], axis=0))

            preprocessor.calibrate(states[i])

    def test_stacking_continuous(self):
        preprocessor = self.stack(
            [
                Lambda(lambda x: x[1] - x[0], steps=1, continuous=True),
                Lambda(lambda x: np.log(x))
            ]
        )
        for state in states:
            if preprocessor.calibrated:
                assertArrEq(preprocessor.transform(state),
                            np.stack([[1] * 4, map(np.log, state)]))
            preprocessor.calibrate(state)

    def test_stacking_not_continuous(self):
        preprocessor = self.stack(
            [
                Lambda(lambda x: x[1] - x[0], steps=1, continuous=False),
                Lambda(lambda x: np.log(x))
            ]
        )
        for i, state in enumerate(states):
            if preprocessor.calibrated:
                assertArrEq(preprocessor.transform(state),
                            np.stack([[i] * 4, map(np.log, state)]))
            preprocessor.calibrate(state)

    def test_reset_with_no_multi_episode(self):
        steps = 7
        preprocessor = chain(
            [
                Lambda(lambda x: x[1] - x[0], steps=steps, continuous=False),
                Identity()
            ]
        )
        for i in range(len(states)):
            preprocessor.calibrate(states[i])
        preprocessor.reset()
        self.assertFalse(preprocessor.calibrated)
        self.assertEqual(preprocessor._calibration_steps_count, 0)
        for preproc in preprocessor.preprocessors:
            if preproc.steps > 0:
                self.assertFalse(preproc.calibrated)
                self.assertEqual(preproc._calibration_steps_count, 0)
            else:
                self.assertTrue(preproc.calibrated)
                self.assertEqual(preproc._calibration_steps_count, 0)

    def test_reset_with_mixed_multi_episode(self):
        not_multi_episode_steps = 1
        multi_episode_steps = 7
        preprocessor = self.stack(
            [
                Lambda(lambda x: x[1] - x[0],
                       steps=multi_episode_steps,
                       continuous=False,
                       multi_episode=True),
                Lambda(lambda x: x[1] - x[0],
                       steps=not_multi_episode_steps,
                       continuous=False,
                       multi_episode=False)
            ]
        )
        for i in range(len(states)):
            preprocessor.calibrate(states[i])
        preprocessor.reset()
        self.assertFalse(preprocessor.calibrated)
        self.assertEqual(preprocessor._calibration_steps_count, 6)
        for preproc in preprocessor.preprocessors:
            if preproc.multi_episode == False:
                self.assertFalse(preproc.calibrated)
                self.assertEqual(preproc._calibration_steps_count, 0)
            else:
                self.assertTrue(preproc.calibrated)
                self.assertEqual(preproc._calibration_steps_count, 7)

    def test_pursue_after_reset_mixed_not_multi_episode(self):
        not_multi_episode_steps = 1
        multi_episode_steps = 7
        preprocessor = self.stack(
            [
                Lambda(lambda x: x[1] - x[0],
                       steps=multi_episode_steps,
                       continuous=False,
                       multi_episode=True),
                Lambda(lambda x: x[1] - x[0],
                       steps=not_multi_episode_steps,
                       continuous=False,
                       multi_episode=False)
            ]
        )
        for i in range(len(states)):
            preprocessor.calibrate(states[i])
        preprocessor.reset()
        with self.assertRaises(ValueError):
            preprocessor.transform(states[0])
        preprocessor.calibrate(states[0])
        for i in range(1, len(states)):
            preprocessor.calibrate(states[i])
            preprocessor.transform(states[-1])

    def test_pursue_after_reset_no_multi_episode_different_steps(self):
        no_multi_episode_setps_1 = 2
        no_multi_episode_steps_2 = 3
        preprocessor = self.stack(
            [
                Lambda(lambda x: x[1] - x[0],
                       steps=no_multi_episode_setps_1,
                       continuous=False,
                       multi_episode=False),
                Lambda(lambda x: x[1] - x[0],
                       steps=no_multi_episode_steps_2,
                       continuous=False,
                       multi_episode=False)
            ]
        )
        for i in range(len(states)):
            preprocessor.calibrate(states[i])
        preprocessor.reset()
        with self.assertRaises(ValueError):
            preprocessor.transform(states[0])
        preprocessor.calibrate(states[0])
        preprocessor.calibrate(states[1])
        with self.assertRaises(ValueError):
            preprocessor.transform(states[0])
        preprocessor.calibrate(states[2])
        for i in range(3, len(states)):
            preprocessor.calibrate(states[i])
            preprocessor.transform(states[-1])

    def calibrate_stacker_with_tests(self, stacker, steps, expected_value=None):
        for i in range(steps):
            with self.assertRaises(ValueError):
                stacker.transform(states[i & len(states)])
            stacker.calibrate(states[i])
        self.assertTrue(stacker.calibrated)
        self.assertEqual(stacker._calibration_steps_count, stacker.steps)
        for preproc in stacker.preprocessors:
            self.assertTrue(stacker.calibrated)
            self.assertEqual(stacker._calibration_steps_count, stacker.steps)
        if expected_value is None:
            stacker.transform(states[0])
        else:
            assertArrEq(stacker.transform(states[0]), expected_value)

    def test_stacker_of_stacker_reset_with_no_multi_episode(self):
        steps1 = 2
        steps2 = 4
        preprocessor = self.stack(
            [
                Lambda(lambda x: x[2] - x[0], steps=steps1, continuous=False),
                Identity(),
                self.stack(
                    [
                        Identity(),
                        Lambda(lambda x: x[4] - x[0],
                               steps=steps2, continuous=False)
                    ]
                )
            ]
        )
        self.calibrate_stacker_with_tests(preprocessor, max([steps1, steps2]))
        preprocessor.reset()
        self.assertFalse(preprocessor.calibrated)
        self.assertFalse(preprocessor._calibration_steps_count, 0)
        for preproc in preprocessor.preprocessors:
            if not preproc.multi_episode:
                self.assertFalse(preproc.calibrated)
                self.assertEqual(preproc._calibration_steps_count, 0)
            else:
                self.assertTrue(preproc.calibrated)
                self.assertEqual(
                    preproc._calibration_steps_count, preproc.steps)
        self.calibrate_stacker_with_tests(preprocessor, max([steps1, steps2]))

    def test_stacker_of_stacker_reset_with_multi_episode(self):
        steps1 = 2
        steps2 = 4
        preprocessor = self.stack(
            [
                Lambda(lambda x: x[2] - x[0], steps=steps1, continuous=False),
                Identity(),
                self.stack(
                    [
                        Identity(),
                        Lambda(lambda x: x[4] - x[0], steps=steps2,
                               continuous=False, multi_episode=True)
                    ]
                )
            ]
        )
        self.calibrate_stacker_with_tests(preprocessor, max([steps1, steps2]))
        preprocessor.reset()
        self.assertFalse(preprocessor.calibrated)
        self.assertEqual(preprocessor._calibration_steps_count, steps1)
        for preproc in preprocessor.preprocessors:
            if not preproc.multi_episode:
                self.assertFalse(preproc.calibrated)
                self.assertEqual(preproc._calibration_steps_count, 0)
            else:
                self.assertTrue(preproc.calibrated)
                self.assertEqual(
                    preproc._calibration_steps_count, preproc.steps)
        self.calibrate_stacker_with_tests(preprocessor, steps1)

        preprocessor = self.stack(
            [
                Lambda(lambda x: x[2] - x[0], steps=steps1, continuous=False),
                Identity(),
                self.stack(
                    [
                        Identity(),
                        Lambda(lambda x: x[4] - x[0], steps=steps2,
                               continuous=False, multi_episode=True)
                    ]
                )
            ]
        )

        self.assertFalse(preprocessor.calibrated)
        self.assertEqual(preprocessor._calibration_steps_count, 0)
        for preproc in preprocessor.preprocessors:
            if preproc.steps > 0:
                self.assertFalse(preproc.calibrated)
                self.assertEqual(preproc._calibration_steps_count, 0)
            else:
                self.assertTrue(preproc.calibrated)
        self.calibrate_stacker_with_tests(preprocessor, max([steps1, steps2]))


class TestEMA(unittest.TestCase):
    def test_default_init(self):
        preprocessor = EMA(10)
        self.assertEqual(preprocessor.steps, 0)
        self.assertEqual(preprocessor._tau, 10 / np.log(2))

    def test_custom_init(self):
        preprocessor = EMA(10, 5)
        self.assertEqual(preprocessor.steps, 5)
        self.assertEqual(preprocessor._tau, 10 / np.log(2))

    def test_calibrate(self):
        window = 5
        preprocessor = EMA(steps=10, window=window)
        alpha = 1 - np.exp(- np.log(2) / window)
        seq = [float(x) for x in range(preprocessor.steps)]

        # Estimate EMA after calibration
        est_s = seq[0]
        s = seq[0]
        for x in seq:
            est_s = alpha * x + (1 - alpha) * est_s
            preprocessor.calibrate(x)

        # Compare
        self.assertEqual(preprocessor._ema[0], est_s)

    def test_call_transform_before_calibration(self):
        pp = EMA(window=10, steps=20)

        for step in range(19):
            pp.calibrate(states[0])

        with self.assertRaises(ValueError):
            pp.transform(states[0])

        pp.calibrate(states[0])
        pp.transform(states[0])

    def test_transform_with_no_calibration(self):
        window = 10
        pp = EMA(window=window, steps=0)
        s = 1.0

        ema = pp.transform(s)
        est_ema = s

        self.assertAlmostEqual(ema, est_ema)

    def test_transform_with_custom_calibration(self):
        window = 5
        steps = 5
        alpha = 1 - np.exp(- np.log(2) / window)
        pp = EMA(window=window,
                 steps=steps)

        self.assertEqual(pp.steps, steps)
        est_ema = states[0]
        for step in range(pp.steps):
            x = states[step % len(states)]
            pp.calibrate(x)
            est_ema = alpha * x + (1 - alpha) * est_ema

        ema = pp.transform(states[0])
        pp.calibrate(states[0])
        est_ema = alpha * states[0] + (1 - alpha) * est_ema
        assertArrEq(ema, est_ema)

    def test_multiple_transforms_with_no_calibration(self):
        window = 5
        steps = 0
        alpha = 1 - np.exp(- np.log(2) / window)
        pp = EMA(window=window,
                 steps=steps)

        est_ema = states[0]

        for step in range(20):
            x = states[step % len(states)]
            ema = pp.transform(x)
            pp.calibrate(x)
            est_ema = alpha * x + (1 - alpha) * est_ema
            assertArrEq(ema, est_ema)

        self.assertEqual(ema.shape, (4,))

    def test_multiple_transforms_with_custom_calibration(self):
        window = 5
        steps = 5
        alpha = 1 - np.exp(- np.log(2) / window)
        pp = EMA(window=window,
                 steps=steps)

        self.assertEqual(pp.steps, steps)
        est_ema = states[0]
        for step in range(pp.steps):
            x = states[step % len(states)]
            pp.calibrate(x)
            est_ema = alpha * x + (1 - alpha) * est_ema

        for step in range(20):
            s = states[step % len(states)]
            ema = pp.transform(s)
            pp.calibrate(s)
            est_ema = alpha * s + (1 - alpha) * est_ema

            assertArrEq(ema, est_ema)
        self.assertEqual(ema.shape, (4,))

    def test_scalar_input_transforms(self):
        states = np.arange(20)

        window = 5
        steps = 5
        alpha = 1 - np.exp(- np.log(2) / window)
        pp = EMA(window=window,
                 steps=steps)

        self.assertEqual(pp.steps, steps)
        est_ema = states[0]
        for step in range(pp.steps):
            x = states[step % len(states)]
            pp.calibrate(x)
            est_ema = alpha * x + (1 - alpha) * est_ema

        for step in range(3):
            ema = pp.transform(states[step])
            pp.calibrate(states[step])
            est_ema = alpha * states[step] + (1 - alpha) * est_ema

            self.assertAlmostEqual(ema, est_ema)

        self.assertEqual(ema.shape, (1,))

    def test_over_many_episodes(self):
        states = np.arange(20)

        window = 5
        steps = 5
        alpha = 1 - np.exp(- np.log(2) / window)
        pp = EMA(window=window,
                 steps=steps)

        self.assertEqual(pp.steps, steps)

        for day in range(2):
            print day
            est_ema = states[0]
            for step in range(pp.steps):
                x = states[step % len(states)]
                pp.calibrate(x)
                est_ema = alpha * x + (1 - alpha) * est_ema

            for step in range(3):
                ema = pp.transform(states[step])
                pp.calibrate(states[step])
                est_ema = alpha * states[step] + (1 - alpha) * est_ema
                self.assertAlmostEqual(ema, est_ema)
            pp.end_episode()


class TestFfill(unittest.TestCase):

    def test_calibrate(self):

        data = np.array([
            [
                [0.0000, np.nan, 1.0000, 2.0000],
                [2.0000, np.nan, 1.0000, 3.0000],
                [np.nan, 2.0000, np.nan, 3.0000],
                [1.0000, np.nan, 1.0000, 5.0000]
            ],
            [
                [5.0000, 1.0000, 1.0000, np.nan],
                [np.nan, -1.000, 4.0000, np.nan],
                [np.nan, 0.0000, np.inf, 1.0000],
                [0.0000, np.nan, 1.0000, 3.0000]
            ]
        ])

        ffill = Ffill(nullfill=2)

        for day_data in data:
            for i, obs in enumerate(day_data):
                if i == 0:
                    expected = copy(obs)
                    expected[np.logical_not(np.isfinite(expected))] = 2
                    last_obs = expected
                else:
                    expected = copy(obs)
                    expected[np.logical_not(np.isfinite(expected))
                             ] = last_obs[np.logical_not(np.isfinite(expected))]
                    last_obs = expected
                assertArrEq(ffill.transform(obs), expected)
            ffill.end_episode()


class TestCumsum(unittest.TestCase):

    def test_calibrate(self):

        data = np.array([
            [
                [0.0000, 1.0000, 1.0000, 2.0000],
                [2.0000, 1.0000, 1.0000, 3.0000],
                [1.0000, 2.0000, 1.0000, 3.0000],
                [1.0000, 1.0000, 1.0000, 5.0000]
            ],
            [
                [5.0000, 1.0000, 1.0000, 1.0000],
                [1.0000, -1.000, 4.0000, 1.0000],
                [1.0000, 0.0000, 0.0000, 1.0000],
                [0.0000, 1.0000, 1.0000, 3.0000]
            ]
        ])

        cumsum = Cumsum()

        for day_data in data:
            for i, obs in enumerate(day_data):
                if i == 0:
                    expected = obs
                    last_obs = obs
                else:
                    expected += obs
                    last_obs = obs
                assertArrEq(cumsum.transform(obs), expected)
            cumsum.end_episode()

    def test_cumsum_diff(self):

        data = np.array([
            [
                [0.0000, 1.0000, 1.0000, 2.0000],
                [2.0000, 1.0000, 1.0000, 3.0000],
                [1.0000, 2.0000, 1.0000, 3.0000],
                [1.0000, 1.0000, 1.0000, 5.0000]
            ],
            [
                [5.0000, 1.0000, 1.0000, 1.0000],
                [1.0000, -1.000, 4.0000, 1.0000],
                [1.0000, 0.0000, 0.0000, 1.0000],
                [0.0000, 1.0000, 1.0000, 3.0000]
            ]
        ])

        pp = compose([Cumsum(), diff()])

        for day_data in data:
            for i, obs in enumerate(day_data):
                if pp.calibrated:
                    assertArrEq(pp.transform(obs), obs)
            pp.end_episode()


class TestEMSTD(unittest.TestCase):

    def test(self):
        preprocessor = EMSTD(10, dt=1, steps=0)
        y = np.arange(100) % 10
        intermediate_expected = (pd.Series(y) - pd.Series(y).ewm(halflife=10, adjust=False).mean()).map(lambda x : x**2)
        expected = intermediate_expected.ewm(halflife=10, adjust=False).mean().map(lambda x : x**0.5)
        for i, state in enumerate(y):
            intermediate_result = (preprocessor.preprocessors[0].transform([state]))
            self.assertEqual(intermediate_result, intermediate_expected[i])
            result = preprocessor.transform([state])
            self.assertEqual(result, expected[i])
            preprocessor.calibrate([state])


if __name__ == '__main__':
    unittest.main()
