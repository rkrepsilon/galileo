import unittest
from copy import copy

import numpy as np

import pytest
from galileo.preprocessing import *
from sklearn.linear_model import LinearRegression

states = np.array([
    [1., 2., 3., 4.],
    [2., 3., 4., 5.],
    [3., 4., 5., 6.],
    [4., 5., 6., 7.],
    [5., 6., 7., 8.],
    [6., 7., 8., 9.],
    [7., 8., 9., 10.],
    [8., 9., 10., 11.],
    [9., 10., 11., 12.],
    [10., 11., 12., 13.]
])


def assertArrEq(x, y):
    assert np.isclose(x, y).all()


class TestTransformer(unittest.TestCase):

    def calibrate_with_tests(self, preprocessor, steps):
        for i in range(steps):
            with self.assertRaises(ValueError):
                preprocessor.transform(states[i])
            preprocessor.calibrate(states[i])
        self.assertTrue(preprocessor.calibrated)
        self.assertEqual(preprocessor._calibration_steps_count, steps)
        assertArrEq(np.array(preprocessor.calibration_state), states[:steps])

    def output_tests_when_calibrated(self, preprocessor,
                                     steps, continuous=False):
        self.assertTrue(preprocessor.calibrated)
        assertArrEq(np.array(preprocessor.calibration_state), states[:steps])
        preproc = copy(preprocessor.initial_estimator)
        for i in range(len(states) - steps):
            preprocessor.calibrate(states[steps + i])
            if continuous:
                if preprocessor.rolling:
                    assertArrEq(
                        np.array(preprocessor.calibration_state), states[i + 1:steps + i + 1])
                    preproc.fit(states[i + 1:steps + i + 1])
                else:
                    preproc = copy(preprocessor.initial_estimator)
                    preproc.fit(states[:steps + 1 + i])
            else:
                assertArrEq(np.array(preprocessor.calibration_state),
                            states[:steps])
                preproc.fit(states[:steps])
            expected_states = preproc.transform([states[steps + i]])[0]
            t_state = preprocessor.transform(states[steps + i])
            assertArrEq(t_state,  expected_states)
            self.assertTrue(len(t_state.shape), 1)

    def test_init(self):
        steps = 4
        for continuous in [True, False]:
            for rolling in [True, False]:
                for multi_episode in [True, False]:
                    if not continuous and rolling:
                        with self.assertRaises(ValueError):
                            preprocessor = Transformer(
                                estimator=MinMaxScaler(),
                                continuous=continuous,
                                steps=steps,
                                rolling=rolling,
                                multi_episode=multi_episode)
                    else:
                        preprocessor = Transformer(
                            estimator=MinMaxScaler(),
                            continuous=continuous,
                            steps=steps,
                            rolling=rolling,
                            multi_episode=multi_episode)
                        self.assertEqual(preprocessor.continuous, continuous)
                        self.assertEqual(preprocessor.steps, steps)
                        # self.assertEqual(
                        #     preprocessor.calibration_state.size, steps)
                        self.assertEqual(preprocessor.rolling, rolling)
                        self.assertFalse(
                            preprocessor.initial_estimator is preprocessor.estimator)
                        self.assertEqual(
                            preprocessor.multi_episode, multi_episode)

    def test_calibration_and_output(self):
        for continuous, rolling in [(True, True), (True, False), (False, False)]:
            for multi_episode in [True, False]:
                estimators = [MinMaxScaler(), MaxAbsScaler(),
                              StandardScaler()]
                if rolling:
                    estimators += [RobustScaler(), QuantileTransformer()]
                for steps in range(1, 4):
                    for estimator in estimators:
                        estimator_ = copy(estimator)
                        preprocessor = Transformer(
                            estimator=estimator_,
                            continuous=continuous,
                            multi_episode=multi_episode,
                            steps=steps,
                            rolling=rolling)
                        self.calibrate_with_tests(preprocessor, steps)
                        self.output_tests_when_calibrated(
                            preprocessor, steps, continuous)

    def test_reset_with_calibration_and_output(self):
        multi_episode = False
        for continuous, rolling in [(True, True), (True, False), (False, False)]:
            estimators = [MinMaxScaler(), MaxAbsScaler(), StandardScaler()]
            if rolling:
                estimators += [RobustScaler(), QuantileTransformer()]
            for steps in range(1, 4):
                for estimator in estimators:
                    estimator_ = copy(estimator)
                    preprocessor = Transformer(
                        estimator=estimator_, continuous=continuous, steps=steps, rolling=rolling, multi_episode=multi_episode)
                    self.assertFalse(preprocessor.multi_episode)
                    self.calibrate_with_tests(preprocessor, steps)
                    self.output_tests_when_calibrated(
                        preprocessor, steps, continuous)
                    preprocessor.reset()
                    self.assertFalse(preprocessor.calibrated)
                    self.assertEqual(preprocessor._calibration_steps_count, 0)
                    self.calibrate_with_tests(preprocessor, steps)
                    self.output_tests_when_calibrated(
                        preprocessor, steps, continuous)

        multi_episode = True
        for continuous, rolling in [(True, True), (True, False), (False, False)]:
            estimators = [MinMaxScaler(), MaxAbsScaler(), StandardScaler()]
            if rolling:
                estimators += [RobustScaler(), QuantileTransformer()]
            for steps in range(1, 4):
                for estimator in estimators:
                    estimator_ = copy(estimator)
                    preprocessor = Transformer(
                        estimator=estimator_, continuous=continuous, steps=steps, rolling=rolling, multi_episode=multi_episode)
                    self.assertTrue(preprocessor.multi_episode)
                    self.calibrate_with_tests(preprocessor, steps)
                    self.output_tests_when_calibrated(
                        preprocessor, steps, continuous)
                    preprocessor.reset()
                    self.assertTrue(preprocessor.calibrated)
                    self.assertEqual(
                        preprocessor._calibration_steps_count, steps)

    def test_numerically(self):
        steps = 2
        preprocessor = Transformer(
            estimator=MinMaxScaler(), continuous=False, steps=steps, rolling=False)
        self.calibrate_with_tests(preprocessor, steps)
        for state in states[2:]:
            expected_result = (state - states[0]) / (states[1] - states[0])
            t_state = preprocessor.transform(state)
            assertArrEq(t_state,  expected_result)
            self.assertEqual(t_state.shape, expected_result.shape)


class TestPredictor(unittest.TestCase):

    def test_init(self):
        pass

    @pytest.mark.filterwarnings('ignore:internal gelsd driver lwork query error, required iwork dimension not returned. This is likely the result of LAPACK bug 0038, fixed in LAPACK 3.2.2 (released July 21, 2010). Falling back to \'gelss\' driver.')
    def test_fit_transform(self):
        for maxlen in [1, 2]:
            for rolling in [True, False]:
                preprocessor = Predictor(
                    estimator=LinearRegression(),
                    continuous=rolling,
                    steps=5,
                    rolling=rolling,
                    multi_episode=False,
                    time_distributed=False,
                    maxlen=maxlen,
                    target_fun=lambda x: 2 * x
                )
                for i in range(5):
                    self.assertFalse(preprocessor.calibrated)
                    self.assertEqual(preprocessor._calibration_steps_count, i)
                    preprocessor.calibrate(states[i])

                assertArrEq(
                    np.array(preprocessor.calibration_state), states[:5])
                self.assertTrue(preprocessor.calibrated)
                self.assertEqual(preprocessor._calibration_steps_count, 5)

                for i in range(5, len(states) - 1):
                    self.assertTrue(preprocessor.calibrated)
                    self.assertEqual(preprocessor._calibration_steps_count, 5)
                    assertArrEq(preprocessor.transform(
                        states[i]), 2 * states[i])
                    preprocessor.calibrate(states[i])

    def test_look_ahead(self):
        for rolling in [True, False]:
            preprocessor = Predictor(
                estimator=LinearRegression(),
                continuous=rolling,
                steps=5,
                rolling=rolling,
                multi_episode=False,
                maxlen=2,
                lookahead_vector=[1],
                target_fun=lambda x: 2 * x
            )
            for i in range(5):
                self.assertFalse(preprocessor.calibrated)
                self.assertEqual(preprocessor._calibration_steps_count, i)
                preprocessor.calibrate(states[i])

            assertArrEq(np.array(preprocessor.calibration_state), states[:5])
            self.assertTrue(preprocessor.calibrated)
            self.assertEqual(preprocessor._calibration_steps_count, 5)

            for i in range(5, len(states) - 1):
                self.assertTrue(preprocessor.calibrated)
                self.assertEqual(preprocessor._calibration_steps_count, 5)
                result = preprocessor.transform(states[i])
                expected = 2 * states[i + 1]
                assertArrEq(result, expected)
                preprocessor.calibrate(states[i])

    def test_look_ahead_multi(self):
        for rolling in [True, False]:
            preprocessor = Predictor(
                estimator=LinearRegression(),
                continuous=rolling,
                steps=5,
                rolling=rolling,
                multi_episode=False,
                maxlen=2,
                lookahead_vector=[1, 2],
                target_fun=lambda x: 2 * x
            )
            for i in range(5):
                self.assertFalse(preprocessor.calibrated)
                self.assertEqual(preprocessor._calibration_steps_count, i)
                preprocessor.calibrate(states[i])

            assertArrEq(np.array(preprocessor.calibration_state), states[:5])
            self.assertTrue(preprocessor.calibrated)
            self.assertEqual(preprocessor._calibration_steps_count, 5)

            for i in range(5, len(states) - 2):
                self.assertTrue(preprocessor.calibrated)
                self.assertEqual(preprocessor._calibration_steps_count, 5)
                result = preprocessor.transform(states[i])
                expected = 2 * np.stack([states[i + 1], states[i + 2]])
                assertArrEq(result, expected)
                preprocessor.calibrate(states[i])
