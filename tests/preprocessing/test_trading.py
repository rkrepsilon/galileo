import os
import unittest

import numpy as np

import pytest
from galileo.preprocessing import *

vector_states = [
    [1., 2., 3., 4.],
    [2., 3., 4., 5.],
    [3., 4., 5., 6.],
    [4., 5., 6., 7.],
    [5., 6., 7., 8.]
]

scalar_states = np.arange(10, dtype=np.float64)

# MACD Defaults
SH = 12
LO = 26
SI = 9
STEPS = 0


def assertArrEq(testcase, x, y, atol=1e-08):
    if not np.isclose(x, y, atol=atol).all():
        print("Expected", x)
        print("Received", y)
    return testcase.assertTrue(
        np.isclose(x, y, atol=atol).all()
    )


class TestReturns(unittest.TestCase):

    def test_returns_init(self):
        for mode in ['normal', 'log']:
            preprocessor = returns(mode=mode)
            self.assertTrue(preprocessor.continuous)
            self.assertEqual(preprocessor.steps, 1)

    def test_calibrate_before_transform(self):
        for mode in ['normal', 'log']:
            preprocessor = returns(mode=mode)
            with self.assertRaises(ValueError):
                preprocessor.transform(vector_states[0])
            preprocessor.calibrate(vector_states[0])
            preprocessor.transform(vector_states[0])

    def test_calibration_steps_counts_maxes_at_step(self):
        for mode in ['normal', 'log']:
            preprocessor = returns(mode=mode)
            preprocessor.calibrate(vector_states[0])
            self.assertEqual(preprocessor._calibration_steps_count, 1)
            for k in range(1, 5):
                preprocessor.calibrate(vector_states[k])
                self.assertEqual(preprocessor._calibration_steps_count, 1)

    def test_reset(self):
        for mode in ['normal', 'log']:
            preprocessor = returns(mode=mode)
            preprocessor.calibrate(vector_states[0])
            self.assertEqual(preprocessor._calibration_steps_count, 1)
            preprocessor.reset()
            self.assertEqual(preprocessor._calibration_steps_count, 0)
            with self.assertRaises(ValueError):
                preprocessor.transform(vector_states[0])

    def test_returns_calibration_state(self):
        for mode in ['normal', 'log']:
            preprocessor = returns(mode=mode)
            expected_calibration_state = np.array(vector_states[0])
            for i in range(5):
                state = vector_states[i]
                preprocessor.calibrate(state)
                self.assertTrue(
                    np.all(np.isclose(preprocessor.calibration_state[-1],
                                      expected_calibration_state)))
                expected_calibration_state += 1

    def test_returns_normal_transform(self):
        preprocessor = returns('normal')
        preprocessor.calibrate(vector_states[0])
        for i in range(1, 5):
            expected_transformed_state = np.array(
                vector_states[i]) / np.array(vector_states[i - 1]) - 1
            state = vector_states[i]
            self.assertTrue(
                np.all(np.isclose(preprocessor.transform(state), expected_transformed_state)))
            preprocessor.calibrate(state)

    def test_returns_log_transform(self):
        preprocessor = returns('log')
        preprocessor.calibrate(vector_states[0])
        for i in range(1, 5):
            expected_transformed_state = np.log(
                np.array(vector_states[i]) / np.array(vector_states[i - 1]))
            state = vector_states[i]
            self.assertTrue(
                np.all(np.isclose(preprocessor.transform(state), expected_transformed_state)))
            preprocessor.calibrate(state)


class TestSpread(unittest.TestCase):

    def test_spread_init(self):
        preprocessor = spread([2, -1])
        self.assertTrue(preprocessor.calibrated)
        self.assertEqual(preprocessor.steps, 0)
        self.assertFalse(preprocessor.continuous)

    def test_spread_calibration_state(self):
        preprocessor = spread([2, -1])
        for i in range(2):
            state = vector_states[i]
            preprocessor.calibrate(state)
            # self.assertTrue(preprocessor.calibration_state.size == 0)

    def test_spread_output(self):
        preprocessor = spread([1, -1])
        expected_spread = [-3, -1]
        for i in range(5):
            state = vector_states[i]
            self.assertTrue(
                np.all(np.isclose(preprocessor.transform(state), expected_spread)))
            preprocessor.calibrate(state)

    def test_resest(self):
        preprocessor = spread([1, -1])
        expected_spread = [-3, -1]
        for i in range(5):
            state = vector_states[i]
            self.assertTrue(
                np.all(np.isclose(preprocessor.transform(state), expected_spread)))
            preprocessor.calibrate(state)
            preprocessor.reset()


class TestSqueeze(unittest.TestCase):

    def test_axis_none(self):
        preprocessor = squeeze()
        for i in range(4):
            state = np.array(vector_states[i])
            t_state = preprocessor.transform([state])
            self.assertTrue(
                np.all(np.isclose(t_state, state)))
            self.assertEqual(t_state.shape, state.shape)

    def test_axis_one(self):
        preprocessor = squeeze(axis=(1,))
        for i in range(4):
            state = np.array(vector_states[i])
            t_state = preprocessor.transform(state.reshape([2, 1, 2]))
            expected_state = state.reshape([2, 2])
            self.assertTrue(np.all(np.isclose(t_state, expected_state)))
            self.assertEqual(t_state.shape, expected_state.shape)


class TestBidAskSpread(unittest.TestCase):

    def test_spread_init(self):
        preprocessor = bid_ask_spread()
        self.assertTrue(preprocessor.calibrated)
        self.assertEqual(preprocessor.steps, 0)
        self.assertFalse(preprocessor.continuous)

    def test_spread_init(self):
        preprocessor = bid_ask_spread()
        for i in range(4):
            state = vector_states[i]
            expected_spread = np.array([i])
            self.assertTrue(
                np.all(np.isclose(
                    preprocessor.transform([state[0], state[i]]), expected_spread)))


class TestMACDSignal(unittest.TestCase):

    def test_macd_init_default(self):
        pp = MACDSignal(steps=20, window_short=SH,
                        window_long=LO, window_signal=SI)

        self.assertTrue(pp.continuous)
        self.assertFalse(pp.calibrated_init)
        self.assertEqual(pp.steps, 20)
        self.assertEqual(pp._calibration_steps_count, 0)

    def test_macd_init_with_no_calibration(self):
        pp = MACDSignal(steps=0)
        self.assertEqual(pp.steps, 0)

    def test_window_length_error(self):
        with self.assertRaises(ValueError):
            pp = MACDSignal(steps=STEPS, window_short=LO,
                            window_long=SH, window_signal=SI)

    def test_multiple_transforms_on_scalar_data(self):
        pp = MACDSignal(steps=STEPS, window_short=SH,
                        window_long=LO, window_signal=SI)

        alpha_sh = 1 - np.exp(- np.log(2) / SH)
        alpha_lo = 1 - np.exp(- np.log(2) / LO)
        alpha_si = 1 - np.exp(- np.log(2) / SI)
        s_sh = scalar_states[0]
        s_lo = scalar_states[0]
        s_si = s_sh - s_lo

        for i in range(10):
            x = scalar_states[i % len(scalar_states)]
            s_sh = alpha_sh * x + (1 - alpha_sh) * s_sh
            s_lo = alpha_lo * x + (1 - alpha_lo) * s_lo
            s_si = alpha_si * (s_sh - s_lo) + (1 - alpha_si) * s_si
            macd_out = pp.transform(x)
            pp.calibrate(x)

        # Check outputs are two scalars
        self.assertEqual(macd_out.shape, (2,))

        # Check results true within tolerance (1 decimal)
        self.assertAlmostEqual(macd_out[0], s_sh - s_lo)
        self.assertAlmostEqual(macd_out[1], s_si)

    def test_multiple_transforms_on_vector_data(self):
        pp = MACDSignal(steps=STEPS, window_short=SH,
                        window_long=LO, window_signal=SI)
        states = np.asarray(vector_states)

        alpha_sh = 1 - np.exp(- np.log(2) / SH)
        alpha_lo = 1 - np.exp(- np.log(2) / LO)
        alpha_si = 1 - np.exp(- np.log(2) / SI)
        s_sh = states[0]
        s_lo = states[0]
        s_si = s_sh - s_lo

        for i in range(10):
            x = states[i % len(vector_states)]
            s_sh = alpha_sh * x + (1 - alpha_sh) * s_sh
            s_lo = alpha_lo * x + (1 - alpha_lo) * s_lo
            s_si = alpha_si * (s_sh - s_lo) + (1 - alpha_si) * s_si
            macd_out = pp.transform(x)
            pp.calibrate(x)

        # Check outputs are two scalars
        self.assertEqual(macd_out.shape, (8,))

        # Check results true within tolerance (1 decimal)
        # print(macd_out, s_sh-s_lo, s_si)
        assertArrEq(self, macd_out[:4], s_sh - s_lo)
        assertArrEq(self, macd_out[4:], s_si)

    def test_calibrate(self):

        steps = 10
        pp = MACDSignal(steps=steps, window_short=SH,
                        window_long=LO, window_signal=SI)

        self.assertEqual(pp.steps, steps)
        alpha_sh = 1 - np.exp(- np.log(2) / SH)
        alpha_lo = 1 - np.exp(- np.log(2) / LO)
        alpha_si = 1 - np.exp(- np.log(2) / SI)

        s_sh = scalar_states[0]
        s_lo = scalar_states[0]

        # Calibrate the estimates
        for i in range(pp.steps):
            x = scalar_states[i % len(scalar_states)]
            s_sh = alpha_sh * x + (1 - alpha_sh) * s_sh
            s_lo = alpha_lo * x + (1 - alpha_lo) * s_lo
            if i == steps / 2:
                s_si = s_sh - s_lo
            if i > steps / 2:
                s_si = alpha_si * (s_sh - s_lo) + (1 - alpha_si) * s_si

        for i in range(pp.steps):
            # Calibrate the preprocessor
            pp.calibrate(scalar_states[i % len(scalar_states)])

        # Transform
        x = scalar_states[(pp.steps + 1) % len(scalar_states)]
        s_sh = alpha_sh * x + (1 - alpha_sh) * s_sh
        s_lo = alpha_lo * x + (1 - alpha_lo) * s_lo
        s_si = alpha_si * (s_sh - s_lo) + (1 - alpha_si) * s_si
        res = pp.transform(x)
        self.assertAlmostEqual(res[0], s_sh - s_lo)
        self.assertAlmostEqual(res[1], s_si)

    def test_transform_with_calibration(self):
        steps = 10
        pp = MACDSignal(steps=steps, window_short=SH,
                        window_long=LO, window_signal=SI)

        # Calibrate the estimates
        alpha_sh = 1 - np.exp(- np.log(2) / SH)
        alpha_lo = 1 - np.exp(- np.log(2) / LO)
        alpha_si = 1 - np.exp(- np.log(2) / SI)
        s_sh = scalar_states[0]
        s_lo = scalar_states[0]
        for i in range(pp.steps):
            x = scalar_states[i % len(scalar_states)]
            s_sh = alpha_sh * x + (1 - alpha_sh) * s_sh
            s_lo = alpha_lo * x + (1 - alpha_lo) * s_lo
            if i == steps / 2:
                s_si = s_sh - s_lo
            if i > steps / 2:
                s_si = alpha_si * (s_sh - s_lo) + (1 - alpha_si) * s_si

        # Calibrate PP
        for i in range(pp.steps):
            x = scalar_states[i % len(scalar_states)]
            pp.calibrate(x)

        # Transform
        for i in range(3):
            x = scalar_states[i % len(scalar_states)]
            s_sh = alpha_sh * x + (1 - alpha_sh) * s_sh
            s_lo = alpha_lo * x + (1 - alpha_lo) * s_lo
            s_si = alpha_si * (s_sh - s_lo) + (1 - alpha_si) * s_si
            macd_out = pp.transform(x)
            pp.calibrate(x)

            # Check results true within tolerance (1 decimal)
            self.assertAlmostEqual(macd_out[0], s_sh - s_lo)
            self.assertAlmostEqual(macd_out[1], s_si)

    def test_require_calibrate_before_transform(self):
        preprocessor = MACDSignal(steps=STEPS)
        mid_prices = range(preprocessor.steps + 1)

        for k in range(preprocessor.steps):
            with self.assertRaises(ValueError):
                preprocessor.transform(mid_prices[k])
            preprocessor.calibrate(mid_prices[k])
        preprocessor.transform(preprocessor.steps)

    def test_reset(self):
        preprocessor = MACDSignal(steps=STEPS)
        mid_prices = range(preprocessor.steps + 1)

        for k in range(preprocessor.steps):
            preprocessor.calibrate(mid_prices[k])
        preprocessor.reset()
        self.assertEqual(preprocessor._calibration_steps_count, 0)
        for k in range(preprocessor.steps):
            with self.assertRaises(ValueError):
                preprocessor.transform(mid_prices[k])
            preprocessor.calibrate(mid_prices[k])
        preprocessor.transform(preprocessor.steps)

    def test_calibration_steps_counts_maxes_at_step(self):
        preprocessor = MACDSignal(steps=STEPS)
        mid_prices = 10
        for k in range(preprocessor.steps):
            preprocessor.calibrate(mid_prices)
        for _ in range(5):
            preprocessor.calibrate(mid_prices)
            self.assertEqual(preprocessor._calibration_steps_count,
                             preprocessor.steps)


class TestIntraDaySeasonality(unittest.TestCase):

    def test_init(self):
        pass

    def test_calibrate_transform(self):

        days_data = np.array([
            [
                [0, 1],
                [14400, 2],
                [28800, 3],
                [43200, 4],
                [57600, 3],
                [72000, 2]
            ],
            [
                [0, 0],
                [14400, 3],
                [28800, 2],
                [43200, 5],
                [57600, 2],
                [72000, 3]
            ],
            [
                [0, 2],
                [14400, 1],
                [28800, 4],
                [43200, 3],
                [57600, 4],
                [72000, 1]
            ]
        ])
        intradayseasonality = IntraDaySeasonality(
            episodes=3,
            frequency=14400
        )

        # First test IntraDaySeasonality
        for day_data in days_data:
            for observation in day_data:
                intradayseasonality.calibrate(observation)
            intradayseasonality.end_episode()

        expected = [1, 2, 3, 3 + 1. / 3, 3, 2]
        for day_data in days_data:
            for i, observation in enumerate(day_data):
                assertArrEq(self, expected[i],
                            intradayseasonality.transform(observation))
                intradayseasonality.calibrate(observation)
            intradayseasonality.end_episode()

    def test_calibrate_transform(self):
        days_data = np.array([
            [
                [0, 1],
                [14400, 2],
                [28800, 3],
                [43200, 4],
                [57600, 3],
                [72000, 2]
            ],
            [
                [0, 0],
                [14400, 3],
                [28800, 2],
                [43200, 5],
                [57600, 2],
                [72000, 3]
            ],
            [
                [0, 2],
                [14400, 1],
                [28800, 4],
                [43200, 3],
                [57600, 4],
                [72000, 1]
            ]
        ])
        intradayseasonality = IntraDaySeasonality(
            episodes=3,
            frequency=14400
        )

        for day_data in days_data:
            for observation in day_data:
                intradayseasonality.calibrate(observation)
            intradayseasonality.end_episode()

        expected = [1, 2, 3, 4, 3, 2]
        test_data = [[0, np.nan],
                     [14400, np.nan],
                     [28800, np.nan],
                     [43200, np.nan],
                     [57600, np.nan],
                     [72000, np.nan]]

        for i, observation in enumerate(test_data):
            assertArrEq(
                self, expected[i], intradayseasonality.transform(observation))

    def test_calibrate_transform_jump(self):

        days_data = np.array([
            [
                [0, 1],
                [28800, 3],
                [43200, 4],
                [57600, 3],
                [72000, 2]
            ],
            [
                [0, 0],
                [28800, 2],
                [43200, 5],
                [57600, 2],
                [72000, 3]
            ],
            [
                [0, 2],
                [28800, 4],
                [43200, 3],
                [57600, 4],
                [72000, 1]
            ]
        ])
        intradayseasonality = IntraDaySeasonality(
            episodes=3,
            frequency=14400
        )

        for day_data in days_data:
            for observation in day_data:
                intradayseasonality.calibrate(observation)
            intradayseasonality.end_episode()

        expected = [1, 2, 3, 4, 3, 2]
        test_data = [[0, np.nan],
                     [14400, np.nan],
                     [28800, np.nan],
                     [43200, np.nan],
                     [57600, np.nan],
                     [72000, np.nan]]

        for i, observation in enumerate(test_data):
            assertArrEq(
                self, expected[i], intradayseasonality.transform(observation))

    def test_calibrate_transform_not_aligned(self):

        shift = 3600
        days_data = np.array([
            [
                [0, 1],
                [14400, 2],
                [28800, 3],
                [43200, 4],
                [57600, 3],
                [72000, 2]
            ],
            [
                [0 + shift, 1.25],
                [14400 + shift, 2.25],
                [28800 + shift, 3.25],
                [43200 + shift, 3.75],
                [57600 + shift, 2.75],
                [72000 + shift, 1.75]
            ],
            [
                [2 * shift, 1.5],
                [14400 + 2 * shift, 2.5],
                [28800 + 2 * shift, 3.5],
                [43200 + 2 * shift, 3.5],
                [57600 + 2 * shift, 2.5],
                [72000 + 2 * shift, 1.5]
            ]
        ])

        intradayseasonality = IntraDaySeasonality(
            episodes=3,
            frequency=14400
        )

        # First test IntraDaySeasonality
        for day_data in days_data:
            for observation in day_data:
                intradayseasonality.calibrate(observation)
            intradayseasonality.end_episode()

        expected = [1, 2, 3, (3.5 + 4 + 3.625) / 3, 3, 2]
        for day_data in days_data[:1]:
            for i, observation in enumerate(day_data):
                assertArrEq(self, expected[i],
                            intradayseasonality.transform(observation))

    def test_calibrate_transform_irregular(self):
        day_train = np.array(
            [
                [0, 1],
                [14400, 2],
                [21600, 2.5],
                [28800, 3],
                [43200, 4],
                [57600, 3],
                [72000, 2]
            ])

        day_test = np.array(
            [
                [0, 1],
                [14400, 2],
                [28800, 3],
                [43200, 4],
                [57600, 3],
                [72000, 2]
            ])

        intradayseasonality = IntraDaySeasonality(
            episodes=1,
            frequency=14400
        )

        for observation in day_train:
            intradayseasonality.calibrate(observation)
        intradayseasonality.end_episode()

        expected = [1, 2, 3, 4, 3, 2]
        for i, observation in enumerate(day_test):
            assertArrEq(self, expected[i],
                        intradayseasonality.transform(observation))


if __name__ == '__main__':
    unittest.main()
