import csv
import os
import unittest

import numpy as np

import pytest
from galileo.envs import SpreadTrading
from galileo.iter import CSVStreamer, WavySignal


def create_test_csv_file(rows):
    with open('test.csv', 'w+') as csvfile:
        csvwriter = csv.writer(csvfile)
        for row in rows:
            csvwriter.writerow(row)


def delete_test_csv_file():
    os.remove("test.csv")


class TestSpreadTrading(unittest.TestCase):
    data_iterator = WavySignal(period_1=3,
                               period_2=1,
                               epsilon=0,
                               length=10,
                               add_timer=False,
                               ba_spread=0.001)
    st = SpreadTrading(
        data_iterator=data_iterator,
        spread_coefficients=[1],
        max_entries=5)

    def test_init(self):
        state = self.st.reset()
        assert self.st.data_iterator == self.data_iterator
        assert self.st._spread_coefficients == [1]
        assert self.st._first_render
        assert self.st._max_entries == 5
        assert self.st.n_actions == 3
        assert len(self.st._signal_history) == 1

    def test_buy_hold_sell(self):
        # Buy
        state = self.st.reset()
        expected_entry_price = state[1]
        expected_position = 1.
        state, _, _, _ = self.st.step(np.array([0, 1, 0]))
        self.assertAlmostEqual(
            state[0], state[1] - self.st.data_iterator._ba_spread)
        self.assertAlmostEqual(state[-1], expected_position)
        self.assertAlmostEqual(self.st._entry_price, expected_entry_price)
        # Hold
        state, reward, _, _ = self.st.step(np.array([1, 0, 0]))
        self.assertAlmostEqual(state[-1], expected_position)
        self.assertAlmostEqual(self.st._entry_price, expected_entry_price)
        # Sell
        expected_exit_price = state[0]
        unrealised_pnl = state[-2]
        expected_position = 0.
        state, reward, _, _ = self.st.step(np.array([0, 0, 1]))
        self.assertAlmostEqual(state[-1], expected_position)
        self.assertAlmostEqual(
            reward, expected_exit_price - expected_entry_price)
        self.assertAlmostEqual(reward, unrealised_pnl)

    def test_sell_hold_buy(self):
        # Buy
        state = self.st.reset()
        expected_entry_price = state[0]
        state, _, _, _ = self.st.step(np.array([0, 0, 1]))
        expected_position = -1.
        self.assertAlmostEqual(
            state[0], state[1] - self.st.data_iterator._ba_spread)
        self.assertAlmostEqual(state[-1], expected_position)
        self.assertAlmostEqual(self.st._entry_price, expected_entry_price)
        # Hold
        state, _, _, _ = self.st.step(np.array([1, 0, 0]))
        self.assertAlmostEqual(state[-1], expected_position)
        self.assertAlmostEqual(self.st._entry_price, expected_entry_price)
        # Sell
        expected_exit_price = state[1]
        expected_position = 0.
        state, reward, _, _ = self.st.step(np.array([0, 1, 0]))
        self.assertAlmostEqual(state[-1], 0.)
        self.assertAlmostEqual(
            reward, expected_entry_price - expected_exit_price)

    def test_buy_buy_sell(self):
        state = self.st.reset()
        # Buy
        expected_entry_price = state[1] / 2.
        state, _, _, _ = self.st.step(np.array([0, 1, 0]))
        # Buy
        expected_entry_price += state[1] / 2.
        state, _, _, _ = self.st.step(np.array([0, 1, 0]))
        self.assertAlmostEqual(self.st._entry_price, expected_entry_price)
        expected_position = 2.
        self.assertAlmostEqual(state[-1], expected_position)
        # Sell
        expected_exit_price = state[0]
        expected_position = 0.
        unrealised_pnl = state[-2]
        state, reward, _, _ = self.st.step(np.array([0, 0, 1]))
        self.assertAlmostEqual(state[-1], expected_position)
        self.assertAlmostEqual(
            reward, expected_exit_price - expected_entry_price)
        self.assertAlmostEqual(reward, unrealised_pnl)

    def test_sell_sell_buy(self):
        state = self.st.reset()
        # Sell
        expected_entry_price = state[0] / 2.
        state, _, _, _ = self.st.step(np.array([0, 0, 1]))
        # Sell
        expected_entry_price += state[0] / 2.
        state, _, _, _ = self.st.step(np.array([0, 0, 1]))
        self.assertAlmostEqual(self.st._entry_price, expected_entry_price)
        expected_position = -2.
        self.assertAlmostEqual(state[-1], expected_position)
        # Buy
        expected_exit_price = state[1]
        expected_position = 0.
        unrealised_pnl = state[-2]
        state, reward, _, _ = self.st.step(np.array([0, 1, 0]))
        self.assertAlmostEqual(state[-1], expected_position)
        self.assertAlmostEqual(
            reward, expected_entry_price - expected_exit_price)
        self.assertAlmostEqual(reward, unrealised_pnl)

    def test_unrealisedpnl_rewards(self):
        # Buy
        state = self.st.reset()
        entry_price = state[1]
        state, reward, _, _ = self.st.step(np.array([0, 1, 0]))
        expected_reward = state[0] - entry_price
        unrealized_pnl = state[-2]
        state, reward, _, _ = self.st.step(np.array([0, 0, 1]))
        self.assertAlmostEqual(reward, expected_reward)
        self.assertAlmostEqual(unrealized_pnl, expected_reward)
        # Sell
        state = self.st.reset()
        entry_price = state[0]
        state, reward, _, _ = self.st.step(np.array([0, 0, 1]))
        expected_reward = entry_price - state[1]
        unrealized_pnl = state[-2]
        state, reward, _, _ = self.st.step(np.array([0, 1, 0]))
        self.assertAlmostEqual(reward, expected_reward)
        self.assertAlmostEqual(unrealized_pnl, expected_reward)

    def test_reset(self):
        pass

    def test_red_zone(self):
        """ We test that the position gets automatically
        closed if we reach end of episode, as if we had market on close order
        """
        max_entries = 4
        red_zone_start = 20
        data_iterator = WavySignal(period_1=3,
                                   period_2=1,
                                   epsilon=0,
                                   length=100,
                                   add_timer=True)

        st = SpreadTrading(data_iterator=data_iterator,
                           spread_coefficients=[1],
                           max_entries=max_entries,
                           red_zone_start=red_zone_start)
        state = st.reset()
        timer = [state[2]]
        for k in range(99):
            state, reward, done, _ = st.step([0, 1, 0])
            timer.append(state[2])
        timer = np.array(timer)
        self.assertEqual(timer[int(-red_zone_start / 2 - 1)], 0.5)
        self.assertTrue((timer[int(-red_zone_start / 2):] < .5).all())
        self.assertTrue((np.diff(timer) <= 0).all())


class TestCSVSpreadTrading(unittest.TestCase):

    def test_csv_streamer(self):
        testrow = [0, 1, 23, 2, 3, 45]
        headrow = ['b1', 'a1', 'v1', 'timer', 'b2', 'a2', 'v2']
        with open('test.csv', 'w+') as csvfile:
            csvwriter = csv.writer(csvfile)
            csvwriter.writerow(headrow)
            for i in range(10):
                csvwriter.writerow(testrow[:3] + [9 - i] + testrow[3:])

        data_iterator = CSVStreamer(filename='test.csv',
                                    header=0,
                                    price_cols=['b2', 'a2', 'b1', 'a1'],
                                    timer_col='timer'
                                    )
        st = SpreadTrading(data_iterator=data_iterator,
                           spread_coefficients=[1, -1])
        expected_state = np.array([0., 1., 23., 9., 2., 3., 45., 0., 0.])
        state = st.reset()
        self.assertTrue(np.all(np.isclose(state, expected_state, atol=1e-5)))
        state, _, _, _ = st.step([0, 1, 0])
        expected_state[-2] = -2
        expected_state[-1] = 1
        expected_state[3] = 8
        self.assertTrue(np.all(np.isclose(state, expected_state, atol=1e-5)))
        state, _, _, _ = st.step([0, 0, 1])
        expected_state[-1] = 0
        expected_state[-2] = 0
        expected_state[3] = 7
        self.assertTrue(np.all(np.isclose(state, expected_state, atol=1e-5)))
        os.remove('test.csv')

    def test_csv_streamer_with_red_zone(self):
        testrow = [0, 1, 23, 2, 3, 45]
        headrow = ['b1', 'a1', 'v1', 'timer', 'b2', 'a2', 'v2']
        red_zone_start = 20
        with open('test.csv', 'w+') as csvfile:
            csvwriter = csv.writer(csvfile)
            csvwriter.writerow(headrow)
            for i in range(100):
                csvwriter.writerow(testrow[:3] + [99 - i] + testrow[3:])

        data_iterator = CSVStreamer(filename='test.csv',
                                    header=0,
                                    price_cols=['b2', 'a2', 'b1', 'a1'],
                                    timer_col='timer'
                                    )
        st = SpreadTrading(data_iterator=data_iterator,
                           spread_coefficients=[1, -1],
                           red_zone_start=red_zone_start)
        state = st.reset()
        timer = [state[3]]
        for k in range(99):
            state, reward, done, _ = st.step([0, 1, 0])
            timer.append(state[3])
        timer = np.array(timer)
        self.assertEqual(timer[int(-red_zone_start / 2 - 1)], 0.5)
        self.assertTrue((timer[int(-red_zone_start / 2):] < .5).all())
        self.assertTrue((timer <= 1).all())
        self.assertTrue((np.diff(timer) <= 0).all())

    @pytest.mark.filterwarnings('ignore:price_columns unspecified.')
    @pytest.mark.filterwarnings('ignore:End of data reached. Rewinding')
    def test_force_flat_on_close(self):
        """ We test that the position gets automatically
        closed if we reach end of episode, as if we had market on close order
        """
        rows = [
            [1, 2],
            [5, 6],
            [9, 10],
            [13, 14],
            [17, 18],
            [21, 22],
            [25, 26],
            [29, 30]
        ]
        create_test_csv_file(rows)
        data_iterator = CSVStreamer(filename='test.csv',
                                    header=None,
                                    price_cols=[0, 1])
        max_entries = 4
        st = SpreadTrading(data_iterator=data_iterator,
                           spread_coefficients=[1],
                           max_entries=max_entries)
        state = st.reset()
        expected_pnl_on_close = 21.0
        done = False
        while not done:
            state, reward, done, _ = st.step([0, 1, 0])
            if done:
                self.assertAlmostEqual(reward, expected_pnl_on_close)
            else:
                self.assertAlmostEqual(reward, 0.)
        with self.assertRaises(StopIteration):
            state = st.reset()
        state = st.reset()
        done = False
        while not done:
            state, reward, done, _ = st.step([0, 1, 0])
            if done:
                self.assertAlmostEqual(reward, expected_pnl_on_close)
            else:
                self.assertAlmostEqual(reward, 0.)
        delete_test_csv_file()


if __name__ == '__main__':
    unittest.main()
