import unittest

from galileo.core import Memory
import numpy as np


class TestMemory(unittest.TestCase):
    def test_create_memory_with_additional_keys(self):
        """
        create an empty memory of size 100 with two extra keys
        assert size of memory is 100,
        assert number of keys is 5 + 2 =7
        """
        test_mem = Memory(100, extra_keys=['add_key_1', 'add_key_2'])
        self.assertEqual(len(test_mem.dict.keys()), 7)
        self.assertEqual(test_mem.size, 100)

    def test_add(self):
        """
        For each key create an array of data [0,1,2,3,4,5]
        Assert for each key, sum of data is 15
        """
        test_keys = ['state', 'action', 'reward', 'next_state', 'terminal']
        test_mem = Memory(6)
        expected_result = np.array([0, 1, 2, 3, 4, 5])
        for i in range(6):
            [test_mem.add(key, i) for key in test_keys]
        for key in test_keys:
            self.assertTrue(np.array_equal(test_mem.dict[key].ordered(),
                                           expected_result))

    def test_update_after_full_cycle(self):
        """
        For each key create an array of data [6,7,2,3,4,5]
        replace oldest item (here 2) by 2
        Assert for each key, sum of data is 30
        """
        test_keys = ['state', 'action', 'reward', 'next_state', 'terminal']
        test_mem = Memory(6)
        for i in range(8):
            [test_mem.add(key, i) for key in test_keys]
        expected_result = np.array([2, 3, 4, 5, 6, 7])
        self.assertTrue(np.array_equal(test_mem.dict[test_keys[0]].ordered(),
                                       expected_result))
        test_mem.update(test_keys[0], 0, 5)
        expected_result[0] = 5
        self.assertTrue(np.array_equal(test_mem.dict[test_keys[0]].ordered(),
                                       expected_result))
        test_mem.update(test_keys[0], -1, 1)
        expected_result[-1] = 1
        self.assertTrue(np.array_equal(test_mem.dict[test_keys[0]].ordered(),
                                       expected_result))

    def test_update_before_full_cycle(self):
        """
        For each key create an array of data [0,1,2]
        replace oldest item (here 1) by 3
        replace newest item (here 0) by 5
        """
        test_keys = ['state', 'action', 'reward', 'next_state', 'terminal']
        test_mem = Memory(6)
        expected_result = np.array([0, 1, 2])
        for i in range(3):
            [test_mem.add(key, i) for key in test_keys]
        self.assertTrue(np.array_equal(test_mem.dict[test_keys[0]].ordered(),
                                       expected_result))
        test_mem.update(test_keys[0], 0, 5)
        expected_result[0] = 5
        self.assertTrue(np.array_equal(test_mem.dict[test_keys[0]].ordered(),
                                       expected_result))
        test_mem.update(test_keys[0], -1, 3)
        expected_result[-1] = 3
        self.assertTrue(np.array_equal(test_mem.dict[test_keys[0]].ordered(),
                                       expected_result))

    def test_get_recent_transitions(self):
        """
        For each key create an array of data [0,1,2,3,4,5]
        make maxlen=2
        check get_recent_tansition outputs index 1, 3, and 5 correctly
        In particular check that transition['state'] = [previous_state,new_state]
        in that order
        """
        test_keys = ['state', 'action', 'reward', 'next_state', 'terminal']
        expected_transition = {}
        expected_transition['state'] = [[0, 1], [2, 3], [4, 5]]
        expected_transition['next_state'] = [[1, 2], [3, 4], [5, 6]]
        expected_transition['terminal'] = [False, False, True]
        expected_transition['action'] = [1, 3, 5]
        expected_transition['reward'] = [1, 3, 5]
        test_mem = Memory(6)
        for i in range(6):
            [test_mem.add(key, i) for key in test_keys]
            [test_mem.update('next_state', i, i + 1) for key in test_keys]
            [test_mem.update('terminal', i, False) for key in test_keys]
        test_mem.update('terminal', -1, True)
        transitions = test_mem.get_transitions([1, 3, 5], 2)
        for keys in test_keys:
            self.assertTrue(
                (transitions[keys] == expected_transition[keys]).all())

    def test_get_index(self):
        """
        For each key create of size 2, add [0,1,2]
        Assert that the index is 0
        """
        test_keys = ['state', 'action', 'reward', 'next_state', 'terminal']
        test_mem = Memory(2)
        test_mem._transitions_keys
        expected_result = 0
        for i in range(3):
            [test_mem.add(key, i) for key in test_keys]
        self.assertEqual(test_mem.get_index(), expected_result)

    def test_current_size(self):
        """
        For each key create of size 10, add [0,1,2,3,4]
        Assert that the size of memory is 5
        """
        test_keys = ['state', 'action', 'reward', 'next_state', 'terminal']
        test_mem = Memory(10)
        expected_result = 5
        for i in range(5):
            [test_mem.add(key, i) for key in test_keys]
        self.assertEqual(test_mem.get_current_size(), expected_result)

    def test_get_epsiodes_indices(self):
        """
        building memory sample of size 20 with increasing ints for all keys
        except for Terminal where we have boolean.
        We have multiple episode, which is forced by having index 8 and 15 be True
        as well as the latest index (here 25%20=5 ) to be true
        [ F F F F F T*  F F T  F F F F F F T  F F F F ] to get
        where the star is the position of the index.
        we then sample the memory for the latest 20 index (full memory) which sould
        read the memrory backwards starting from the start index.
        the episodes should then be of size 3, 7 and 10
        ep_indexes are (0,2), (3,9), (10,19)
        """
        test_mem = Memory(20)
        test_keys = ['state', 'action', 'reward', 'next_state']
        ends_of_episodes = [8, 15, 25]
        for i in range(26):
            [test_mem.add(key, i) for key in test_keys]
            if i in ends_of_episodes:
                test_mem.add("terminal", True)
            else:
                test_mem.add("terminal", False)
        ep_indexes_expected = [(0, 2), (3, 9), (10, 19)]
        ep_indexes = test_mem.get_episodes_indices(test_mem.dict['terminal'])
        self.assertSequenceEqual(ep_indexes, ep_indexes_expected)

    def test_get_valid_indices(self):
        """
        building same memory sample as before
        ep_indexes are (0,2), (3,9), (10,19)
        By taking maxlen= 3 the first index should be cut out of the sample.
         and we should for valid indices range(5,10), range(10,20)
        """
        test_mem = Memory(20)
        test_keys = ['state', 'action', 'reward', 'next_state']
        ends_of_episodes = [8, 15, 25]
        maxlen = 3
        for i in range(26):
            [test_mem.add(key, i) for key in test_keys]
            if i in ends_of_episodes:
                test_mem.add("terminal", True)
            else:
                test_mem.add("terminal", False)
        valid_indices_expected = list(range(5, 10)) + list(range(12, 20))
        valid_indices = list(test_mem.get_valid_indices(maxlen))
        self.assertSequenceEqual(valid_indices, valid_indices_expected)

    def test_get_valid_indices_with_horizon(self):
        """
        building same memory sample as before
        ep_indexes are (0,2), (3,9), (10,19)
        By taking maxlen= 3 the first index should be cut out of the sample.
         and we should for valid indices range(5,10), range(10,20)
        """
        test_mem = Memory(20)
        test_keys = ['state', 'action', 'reward', 'next_state']
        ends_of_episodes = [8, 15, 25]
        maxlen = 1
        horizon = 3
        for i in range(26):
            [test_mem.add(key, i) for key in test_keys]
            if i in ends_of_episodes:
                test_mem.add("terminal", True)
            else:
                test_mem.add("terminal", False)
        valid_indices_expected = list(range(3, 7)) + list(range(10, 17))
        valid_indices = list(test_mem.get_valid_indices(maxlen, horizon))
        self.assertSequenceEqual(valid_indices, valid_indices_expected)

    def test_get_valid_indices_with_horizon_and_maxlen(self):
        """
        building same memory sample as before
        ep_indexes are (0,2), (3,9), (10,19)
        By taking maxlen= 3 the first index should be cut out of the sample.
         and we should for valid indices range(5,10), range(10,20)
        """
        test_mem = Memory(20)
        test_keys = ['state', 'action', 'reward', 'next_state']
        ends_of_episodes = [8, 15, 25]
        maxlen = 2
        horizon = 2
        for i in range(26):
            [test_mem.add(key, i) for key in test_keys]
            if i in ends_of_episodes:
                test_mem.add("terminal", True)
            else:
                test_mem.add("terminal", False)
        valid_indices_expected = list(range(4, 8)) + list(range(11, 18))
        valid_indices = list(test_mem.get_valid_indices(maxlen, horizon))
        self.assertSequenceEqual(valid_indices, valid_indices_expected)

if __name__ == '__main__':

    unittest.main()
