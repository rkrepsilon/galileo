import unittest

import numpy as np

from galileo.utils import *


class TestUtils(unittest.TestCase):
    def test_calc_spread(self):
        spread_coefficients = [1, -0.1]
        prices = np.array([1, 2, 10, 20])
        spread_price = (-1, 1)
        assert calc_spread(prices, spread_coefficients) == spread_price

    def test_markout(self):
        y = np.array([0, 1, 2, 3, 4, 5, 4, 3, 2, 1, 0, 1])
        a = np.array([1, 0, 0, 1, 0, -1, 0, 0, 1, 1, 0, 1])
        m = np.array([
            [1, 2, 3],
            [1, 2, 1],
            [1, 2, 3],
            [-1, -2, -1]
        ])
        assert (markout(prices=y, actions=a, horizon=3) == m).all()

    def test_lagged_features(self):
        X = np.array([
            [0, 4, 7, 5, 5],
            [5, 3, 3, 4, 7],
            [2, 1, 9, 6, 1],
            [9, 8, 2, 8, 7],
            [5, 0, 7, 0, 1],
            [1, 4, 0, 6, 0],
            [9, 0, 8, 2, 2],
            [9, 3, 2, 8, 9],
            [4, 8, 6, 7, 0],
            [3, 7, 6, 0, 9]
        ])
        X_1_td = np.array([[x] for x in X])

        assert (lagged_features(X, maxlen=1,
                                time_distributed=False) == X).all()
        assert (lagged_features(X, maxlen=1,
                                time_distributed=True) == X_1_td).all()

        X_2_ntd = np.array([
            [0, 4, 7, 5, 5, 5, 3, 3, 4, 7],
            [5, 3, 3, 4, 7, 2, 1, 9, 6, 1],
            [2, 1, 9, 6, 1, 9, 8, 2, 8, 7],
            [9, 8, 2, 8, 7, 5, 0, 7, 0, 1],
            [5, 0, 7, 0, 1, 1, 4, 0, 6, 0],
            [1, 4, 0, 6, 0, 9, 0, 8, 2, 2],
            [9, 0, 8, 2, 2, 9, 3, 2, 8, 9],
            [9, 3, 2, 8, 9, 4, 8, 6, 7, 0],
            [4, 8, 6, 7, 0, 3, 7, 6, 0, 9],
        ])

        assert (lagged_features(X, maxlen=2,
                                time_distributed=False) == X_2_ntd).all()

        X_2_td = np.array([
            [[0, 4, 7, 5, 5], [5, 3, 3, 4, 7]],
            [[5, 3, 3, 4, 7], [2, 1, 9, 6, 1]],
            [[2, 1, 9, 6, 1], [9, 8, 2, 8, 7]],
            [[9, 8, 2, 8, 7], [5, 0, 7, 0, 1]],
            [[5, 0, 7, 0, 1], [1, 4, 0, 6, 0]],
            [[1, 4, 0, 6, 0], [9, 0, 8, 2, 2]],
            [[9, 0, 8, 2, 2], [9, 3, 2, 8, 9]],
            [[9, 3, 2, 8, 9], [4, 8, 6, 7, 0]],
            [[4, 8, 6, 7, 0], [3, 7, 6, 0, 9]],
        ])

        assert (lagged_features(X, maxlen=2,
                                time_distributed=True) == X_2_td).all()

    def test_lookahead_targets(self):
        y = np.array([
            [5, 5],
            [4, 7],
            [6, 1],
            [8, 7],
            [0, 1],
            [6, 0],
            [2, 2],
            [8, 9],
            [7, 0]
        ])

        lookahead_vector = [0]
        assert (lookahead_targets(
            y, lookahead_vector, lambda x: x[-1]) == y).all()

        lookahead_vector = [0, 2]
        y_02_last = np.array([
            [5, 5, 6, 1],
            [4, 7, 8, 7],
            [6, 1, 0, 1],
            [8, 7, 6, 0],
            [0, 1, 2, 2],
            [6, 0, 8, 9],
            [2, 2, 7, 0]
        ])
        assert (lookahead_targets(
            y, lookahead_vector, lambda x: x[-1]) == y_02_last).all()

        lookahead_vector = [0]
        assert (lookahead_targets(
            y, lookahead_vector, lambda x: np.sum(x, axis=0)) == y).all()

        lookahead_vector = [0, 2]
        y_02_sum = np.array([
            [5, 5, 15, 13],
            [4, 7, 18, 15],
            [6, 1, 14, 9],
            [8, 7, 14, 8],
            [0, 1, 8, 3],
            [6, 0, 16, 11],
            [2, 2, 17, 11]
        ])
        lookahead_vector = [0, 2]
        print lookahead_targets(
            y, lookahead_vector, lambda x: np.sum(x, axis=0))
        assert (lookahead_targets(
            y, lookahead_vector, lambda x: np.sum(x, axis=0)) == y_02_sum).all()


if __name__ == '__main__':
    unittest.main()
