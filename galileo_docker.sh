#!/bin/sh

function usage {
    test -n "$1" && { echo "Error: $1"; echo; }
    echo "
Usage:

`basename $0` [OPTS] PATH_TO_EXPERIMENTS

A docker entry point to run a Galileo job locally or on the cluster; or to run the reporting tool.

Options:
    -e, --experiment   Name of the experiment
    -n, --name         Name of the run within this experiment
    -a, --append       Will append to the experiment if already exist
    -d, --debug        Will enable the program to run in debug mode
    -p, --profile      Will output a profile to galileo.prof
    -l, --local        Will install the local galileo version
    -b, --backend      Backend to use (default: ${default_backend})
    -m, --mount        Extra directories to mount (default: ${dirs_to_mount})
    -v, --version      Galileo version to use (default: ${default_version})
    -i, --ui           Will launch the user interface
    -r, --routing      Ports routing from localhost to container (default: ${ports_routing})
    -j, --jupyter      Will run a jupyter notebook.

Cluster options
    -c, --cluster      Will run Galileo on the mesos cluster
    --max_runtime      Cluster: Max runtime (default: ${max_runtime})
    --memory           Cluster: Max memory usage (default: ${memory})
    --cpus             Cluster: Requested number of CPUs (default: ${cpus})
    "
    
    exit 1
}



# defaults
default_backend=theano
default_version=latest
dirs_to_mount=()
ports_routing=()

debug=0
cluster=0
max_runtime=21600000
memory=4096
cpus=1
profile=0

profile_output=galileo.prof
current_dir=`pwd`
launch_ui=0
local=0
append=0
backend=$default_backend
version=$default_version

jupyter=0
jupyter_port=8888

realpath(){
    current_dir=`pwd`
    if [[ -d "$1" ]]; then
        cd "$1"
        echo "$(pwd -P)"
    else
        cd "$(dirname "$1")"
        echo "$(pwd -P)/$(basename "$1")"
    fi
    cd ${current_dir}
}

while :
do
    case $1 in
        
        -h | --help)
            usage
        ;;
        
        -n | --name)
            name=$2
            shift 2
        ;;
        
        -e | --experiment)
            experiment=$2
            shift 2
        ;;
        
        -a | --append)
            append=1
            shift 1
        ;;
        
        -d | --debug)
            debug=1
            shift 1
        ;;
        
        -p | --profile)
            profile=1
            shift 1
        ;;
        
        -i | --ui)
            launch_ui=1
            ports_routing+='8050:8050'
            shift 1
        ;;
        
        -r | --routing)
            ports_routing+=($2)
            shift 2
        ;;
        
        -l | --local)
            local=1
            shift 1
        ;;
        
        -b | --backend)
            backend=$2
            shift 2
        ;;
        
        -m | --mount)
            dirs_to_mount+=($2)
            shift 2
        ;;
        
        -v | --version)
            version=$2
            shift 2
        ;;
        
        -c | --cluster)
            cluster=1
            shift 1
        ;;
        
        --max_runtime)
            max_runtime=$2
            shift 2
        ;;
        
        --memory)
            memory=$2
            shift 2
        ;;
        
        --cpus)
            cpus=$2
            shift 2
        ;;
        
        -j | --jupyter)
            jupyter=1
            ports_routing+='8888:8888'
            shift 1
        ;;
        
        --)
            shift
            break
        ;;
        
        -*)
            usage "Unknown option '$1'"
        ;;
        
        *)
            break
    esac
done

if [ ${cluster} -eq 1 ]; then
    if [ ${local} -eq 1 ]; then
        echo "Running on the cluster. Ignoring --local option."
        local=0
    fi
    if [ ${profile} -eq 1 ]; then
        echo "Running on the cluster. Ignoring --profile option."
        profile=0
    fi
fi

# Assign the experiments directory

experiments_directory=$1
if [ ${jupyter} -eq 0 ]; then
    test -z "${experiments_directory}" && usage "Experiments directory is required."
fi

experiments_directory=`realpath ${experiments_directory}`
GID=`id -g`
docker_options="--rm --privileged  --name ${USER}-`uuidgen` --entrypoint=/bin/bash -v /home:/home -v ${current_dir}:${current_dir} -w ${current_dir}"

if [ ${jupyter} -eq 0 ]; then
    docker_options="${docker_options} -v ${experiments_directory}:${experiments_directory}"
fi
if [ $debug -eq 1 ] && [ $local -eq 1 ] && [ $cluster -eq 0 ]; then
    docker_options="${docker_options} -it"
fi

ports_routing_final=()
for port_routing in ${ports_routing[@]}
do
    ports=(${port_routing//:/ })
    host_port=${ports[0]}
    container_port=${ports[1]}
    isfree=$(netstat -taln | grep $host_port)
    while [[ -n "$isfree" ]]; do
        host_port=$[host_port+1]
        isfree=$(netstat -taln | grep $host_port)
    done
    docker_options="${docker_options} -p 0.0.0.0:${host_port}:${container_port}"
    i_port=$[i_port+1]
    ports_routing_final+=(${host_port}:${container_port})
done

if [ ${cluster} -ne 1 ]; then
    for dir_to_mount in ${dirs_to_mount[@]}
    do
        thisdir=`realpath ${dir_to_mount}`
        docker_options="${docker_options} -v ${thisdir}:${thisdir}:ro"
    done
fi

if [ $launch_ui -eq 1 ]; then
    galileo_cmd="galileo report ${experiments_directory}"
elif [ $jupyter -eq 1 ]; then
    galileo_cmd="galileo jupyter"
else
    test -z "${experiment}" && usage "Experiment name must be specified."
    experiment_directory="${experiments_directory}/${experiment}"
    galileo_options=""
    if [ -n "${name}" ]; then
        galileo_options="${galileo_options} --name ${name}"
    fi
    if [ $append -eq 1 ]; then
        galileo_options="${galileo_options} --append"
    fi
    if [ $profile -eq 1 ]; then
        galileo_options="${galileo_options} --profile ${profile_output}"
    fi
    galileo_cmd="galileo run ${galileo_options} ${experiment_directory}"
fi

user_cmd="export KERAS_BACKEND=${backend}; export THEANO_FLAGS=base_compiledir=${current_dir};"
if [ $local -eq 1 ]; then
    user_cmd="${user_cmd} su-exec ${USER} rm -rf venv; su-exec ${USER} virtualenv --system-site-packages venv; source ./venv/bin/activate; su-exec ${USER} pip install -r requirements.txt; su-exec ${USER} python setup.py install --force;"
    if [ $jupyter -eq 1 ]; then
        user_cmd="${user_cmd} su-exec ${USER} pip install --no-deps --ignore-installed ipython==5.5.0; ./venv/bin/ipython kernel install;"
    fi
fi
docker_img="rkr/galileo.core:${version}"

if [ ${cluster} -eq 1 ]; then
    mounts_with_perm=""
    for dir_to_mount in ${dirs_to_mount[@]}
    do
        mounts_with_perm="${mounts_with_perm} ${dir_to_mount}:ro"
    done
    cluster_options="--user ${USER} --command '${user_cmd} ${galileo_cmd}' --version ${docker_img}"
    cluster_options="${cluster_options} --max_runtime ${max_runtime}"
    cluster_options="${cluster_options} --memory ${memory}"
    cluster_options="${cluster_options} --cpus ${cpus}"
    cluster_options="${cluster_options} --mount ${current_dir}:rw ${experiment_directory}:rw ${mounts_with_perm}"
    cluster_options="${cluster_options} --uid ${UID} --gid ${GID}"
    run_cmd="docker run --privileged -it --rm --entrypoint 'bash' -v ${current_dir}:/workspace -w /workspace ${docker_img} -c \"python bin/run_galileo_cluster.py ${cluster_options}\""
else
    group_id=`id -g`
    group_name=`id -g -n`
    user_id=`id -u`
    user_name=`id -u -n`
    create_user="groupadd -g ${group_id} ${group_name}; useradd -g ${group_id} -u ${user_id} ${user_name};"
    docker_cmd="${create_user} ${user_cmd} su-exec ${USER} ${galileo_cmd}"
    run_cmd="docker pull ${docker_img}; docker run ${docker_options} ${docker_img} -c \"${docker_cmd}\""
fi


echo "Running galileo with settings:"
echo "  - Galileo image: ${docker_img}"
echo "  - Use local: ${local}"
echo "  - Backend: ${backend}"
if [ $jupyter -eq 1 ]; then
    echo "Launching Jupyter notebook."
elif [ $launch_ui -eq 1 ]; then
    echo "Running reporting interface with settings:"
    echo "  - Experiments directory: ${experiments_directory}"
else
    echo "Running one job with settings:"
    echo "  - Experiments directory: ${experiments_directory}"
    echo "  - Experiment name: ${experiment}"
    echo "  - Run name: ${name}"
    echo "  - Append: ${append}"
    if [ ${cluster} -eq 0 ]; then
        echo "  - Debug: ${debug}"
        echo "  - Profile: ${profile}"
    else
        echo " Running on cluster with settings:"
        echo "  - max_runtime: ${max_runtime}"
        echo "  - memory: ${memory}"
        echo "  - cpus: ${cpus}"
    fi
fi
if [ ${#ports_routing_final[@]} -ne 0 ]; then
    echo "  - Ports routing:"
    for port_routing in ${ports_routing_final[@]}
    do
        echo "    - ${port_routing}"
    done
fi


echo "${run_cmd}"
eval ${run_cmd}
