""" Script to delete mesos job from the console.
"""

import argparse
import datetime
import six

from datetime import datetime

import cook.jobclient as jobclient

def main():
    """ function to delete a running job on the cluster.
    """

    parser = argparse.ArgumentParser(
        description="Kill a Galileo job on the cluster.")
    parser.add_argument(
        "-u", "--user", help="Username on the cluster", required=True)
    parser.add_argument("-i", "--job_id", help="Id of job to kill.", required=True)
    args = parser.parse_args()

    client = jobclient.JobClient("http://hkgdrs01.rnd.rkr.hkg:12321",
                                 http_user=args.user,
                                 http_password="")
    try:
        info = client.query([args.job_id])
    except:
        raise ValueError("Job does not exist, \
            check uuid{} is correct".format(args.job.id))
    submit_time = datetime.fromtimestamp(
        info[0]['submit_time'] / 1000).strftime('%Y-%m-%d %H:%M:%S')
    print("Trying to delete job launched at {} with following command line:\
        \n {}".format(submit_time, info[0]['command']))
    var = None
    while var not in ['y', 'n']:
        var = six.moves.input("Confirm? (y/n)")
    if var == 'y':
        client.delete([args.job_id])
        print("Job deleted.")

if __name__ == "__main__":
    main()
