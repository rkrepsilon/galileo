import argparse
import os
import pprint

import cook.jobclient as jobclient

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Run a set of Galileo research jobs on the cluster. Normally this script is triggered from galileo_docker.sh")
    parser.add_argument(
        "-u", "--user", help="Username on the cluster", required=True)
    parser.add_argument(
        "-i", "--uid", help="User ID in Docker container", required=True)
    parser.add_argument(
        "-g", "--gid", help="Group ID in Docker container", required=True)
    parser.add_argument(
        "-m", "--mount", help="Directories to mount in the Docker image", nargs="*")
    parser.add_argument(
        "-c", "--command", help="The bash command to run once Docker has started", required=True)
    parser.add_argument(
        "-v", "--version", help="Galileo Docker Image version to use", default="rkr/galileo.core:latest")

    # Options for running the job with Cook
    parser.add_argument(
        "--max_runtime", help="Cook option: Maximum retries if the job fails", default=21600000, type=int)
    parser.add_argument(
        "--memory", help="Cook option: Memory required by the job", default=4096, type=int)
    parser.add_argument(
        "--cpus", help="Cook option: Number of CPUs required by the job", default=1, type=int)
    args = parser.parse_args()

    cl = jobclient.JobClient(
        "http://hkgdrs01.rnd.rkr.hkg:12321", http_user=args.user, http_password="")

    job_config = [{
        "name": "%s_galileo_experiment" % args.user,
        "command": args.command,
        "max_retries": 1,
        "max_runtime": args.max_runtime,
        "mem": args.memory,
        "cpus": args.cpus,
        "container": {
            "type": "DOCKER",
            "docker": {
                "image": args.version,
                "network": "HOST",
                "force-pull-image": True,
                "parameters": [{"key": "user", "value": str(args.uid) + ":" + str(args.gid)}]
            },
            "volumes": []
        }
    }]
    if args.mount is not None:
        for mount in args.mount:
            directory = mount.split(':')[0]
            permission = mount.split(':')[1]
            job_config[0]['container']['volumes'].append({
                "container-path": directory,
                "host-path": directory,
                "mode": permission
            })
    pprint.pprint(job_config)

    jobs = cl.submit(job_config)
    jobs_info = cl.query(jobs)
    framework_id = jobs_info[0]['framework_id']
    print("Running jobs (this is the job id, not the task_id in the Mesos UI):")
    print(jobs)
    print("Check their status in the Cook framework: http://hkgdrs01.rnd.rkr.hkg:5050/#/frameworks/%s" % framework_id)
