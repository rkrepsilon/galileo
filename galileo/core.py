import glob
import json as json
import os
import shutil
import tarfile
import warnings
from copy import copy, deepcopy

import numpy as np
import six

from .ringbuffer import RingBuffer
from .utils import get_class_name, get_params_dict, load_brain


class DataBreak(Exception):
    pass


class Agent(object):
    """Agent base class.

    """

    def __init__(self):
        """Initialisation class
        """
        self.total_frames_seen = 0
        self.episode_frames_seen = 0
        self.episodes_seen = 0

    def act(self, state):
        """Action function.

        This function takes a state (from an environment) as an argument and
        returns an action.

        Args:
            state (numpy.array): state vector

        Returns:
            np.array: numpy array of the action to take
        """
        self.total_frames_seen += 1
        self.episode_frames_seen += 1
        return self._act(state)

    def _act(self, state):
        raise NotImplementedError()

    def end(self):
        """End of episode logic.
        """
        self._end()
        self.episodes_seen += 1
        self.episode_frames_seen = 0

    def _end(self):
        pass


class RLAgent(Agent):
    """Abstract class for a reinforcement learning agent.
    """

    learner = True

    def __init__(self, memory, brain, default_action, maxlen=1, random_action_fun=None, reward_clip=[-np.inf, np.inf], n_skip_frames=0):
        """Agent base initialisation function

        Args:
            memory (galileo.core.memory): Memory instance
            brain (galileo.core.brain): Brain instance or path to brain.
            default_action (list): Action (ohe) to take when we can't predict and we are not exploring
            maxlen (int): history of states to consider for prediction
            random_action_fun (function): Function choosing the action to take when we are exploring
            reward_clip (list): List of two elements defining the lower and upper limit for the reward clipping
            n_skip_frames (list): Number of frames to skip in between each action
        """
        super(RLAgent, self).__init__()
        self.memory = memory
        self.brain = load_brain(brain) if (type(brain) is str) else brain
        self.reward_clip = reward_clip
        self.maxlen = maxlen
        self._default_action = default_action
        self.n_skip_frames = n_skip_frames
        if random_action_fun is None:
            self._random_action_fun = lambda: np.random.multinomial(
                1, np.ones(self.brain.output_size) / float(self.brain.output_size))
        else:
            self._random_action_fun = random_action_fun
        self.warmup = False

    def _should_skip_action(self):
        should_check_for_skipping = self.n_skip_frames > 0
        should_predict = (self.episode_frames_seen %
                          (self.n_skip_frames + 1)) == 0
        return should_check_for_skipping and not should_predict

    def _act(self, state):
        """Action function.

        Take a state (from an environment, possibly preprocessed) as an argument and
        return an action.

        Args:
            state (numpy.array): state vector

        Returns:
            np.array: numpy array of the action to take
        """
        self.memory.add('state', state)
        # Order matters - check whether we should skip before generating a random move during warmup
        if self._should_skip_action() or (self.episode_frames_seen < self.maxlen):
            action = self._default_action
        elif self.warmup:
            action = self._random_action_fun()
        else:
            action = self._predict_action(
                recent_states=self.memory.dict['state'][-self.maxlen:][np.newaxis, :])
        self.memory.add('action', action)
        return action

    def observe(self, state, reward, terminal):
        """Observe function.

        This function takes a state, a reward and a terminal boolean and stores it in the memory.
        Extra logic can be added in _observe

        Args:
            state (numpy.array): state vector
            reward (float): reward value
            terminal (bool): whether the game is over or not
        """
        reward = max(reward, self.reward_clip[0])
        reward = min(reward, self.reward_clip[1])
        self.memory.add('reward', reward)
        self.memory.add('next_state', state)
        self.memory.add('terminal', terminal)
        self._observe(state, reward, terminal)

    def _observe(self, state, reward, terminal):
        pass

    def learn(self):
        """Learn function.

        This function samples some transitions from the memory, learns on this batch and returns a loss value.
        This is only used for learning agents.

        Returns:
            (tuple): Loss on the batch and boolean (True if has learned, False otherwise)
        """
        return self._learn()

    def _predict_action(self, recent_states):
        raise NotImplementedError()

    def _learn(self):
        raise NotImplementedError()


class Brain(object):
    """Abstract class for a brain used by an agent. Implementations can be
    agent or environment specific, depending on action.
    """

    def __init__(self, output_size, input_shape=None, loading=False, verbose=True, **kwargs):
        """Initialisation function.

        Args:
            output_size (int): Number of actions the brain is predicting.
            input_shape (tuple): Shape of the input space. If None, will be infered at train or predict time.
            loading (bool): Whether we are loading a model or not
            verbose (bool): Will print a summary of the brain when compliled
        """
        params = get_params_dict(locals())
        self._object_dict = {
            "class_name": get_class_name(self),
            "config": params
        }
        # Make shapes explicitely public
        self.input_shape = input_shape
        self.output_size = output_size
        # Should be implemented by subclasses
        self.verbose = verbose
        self.models = {}
        self.compiled = False
        self.built = False
        if (not loading) and (input_shape is not None):
            self._build()
            self._compile()
            if self.verbose:
                self.summary()

    def _compile(self):
        raise NotImplementedError()

    def _build(self):
        raise NotImplementedError()

    def _build_compile_from_state(self, state):
        self.input_shape = tuple(state.shape[i]
                                 for i in range(1, len(state.shape)))
        self._object_dict['config']['input_shape'] = self.input_shape
        if (not self.built):
            self._build()
        self._compile()
        if self.verbose:
            self.summary()

    def train(self, state, *args, **kwargs):
        """Training function.

        Args:
            state (numpy.array): input of the network

        Returns:
            float: value of the loss
        """
        if (self.input_shape is None):
            self._build_compile_from_state(state)
        return self._train(state, *args, **kwargs)

    def _train(self, state, *args, **kwargs):
        raise NotImplementedError()

    def predict(self, state, *args, **kwargs):
        """Predict function.

        Args:
            state (numpy.array): state vector

        Returns:
            numpy.array: output array (can have different meanings depending on the brain type)
        """
        if (self.input_shape is None):
            self._build_compile_from_state(state)
        return self._predict(state, *args, **kwargs)

    def _predict(self, state):
        raise NotImplementedError()

    def save(self, filepath):
        """Save the brain.

        Args:
            filepath (str): Brain filepath.
        """
        path = '/'.join(filepath.split('/')[:-1])
        path = os.path.join(path, '')
        brain_path = os.path.join(path, 'brain')
        if os.path.exists(brain_path):
            shutil.rmtree(brain_path)
        os.mkdir(brain_path)
        self._save_params(brain_path)
        self._save_models(brain_path)
        tar = tarfile.open(filepath, "w:gz")
        for name in glob.glob(os.path.join(brain_path, '*')):
            tar.add(name, arcname=os.path.basename(name))
        shutil.rmtree(brain_path)
        tar.close()

    def _save_params(self, path):
        path = os.path.join(path, '')
        with open(path + 'brain_dict.json', 'w') as file_:
            json.dump(self._object_dict, file_, indent=4)

    @staticmethod
    def _save_model_object(model, filepath, inference):
        """ ML Library specific
        Args:
            model (object): Model object
            filepath (filepath): Filepath of the model to save
            inference (bool): Will dump the model to a tf graph for inference
        """
        raise NotImplementedError()

    @staticmethod
    def _load_model_object(filepath):
        """ ML Library specific
        Args:
            filepath (filepath): Filepath of the model to save
        """
        raise NotImplementedError()

    def _save_models(self, path):
        """This saves the models dictionary and models objects to files.
        """
        path = os.path.join(path, '')

        models_dict = {}
        for key in self.models.keys():
            self._save_model_object(
                model=self.models[key]['object'],
                filepath=os.path.join(path, key + '.model'),
                inference=self.models[key]['inference']
            )
            models_dict[key] = {}
            models_dict[key]['filename'] = key + '.model'
            models_dict[key]['trainable'] = self.models[key]['trainable']
            models_dict[key]['inference'] = self.models[key]['inference']
        with open(path + 'models_dict.json', 'w') as file_:
            json.dump(models_dict, file_, indent=4)

    def summary(self):
        raise NotImplementedError()


class Memory(object):
    """Abstract class for a memory used by an agent.
    """

    _transitions_keys = ['state', 'action', 'reward', 'next_state', 'terminal']

    def __init__(self, size, extra_keys=[]):
        """Initialisation function

        Args:
            size (int): size of the memory (maximum number of transitions to store)
            extra_keys (list): list of extra keys to consider
        """
        self.size = size
        self._keys = copy(self._transitions_keys)
        self._keys += extra_keys
        self.dict = {key: RingBuffer(size=size) for key in self._keys}

    def add(self, key, val):
        """Add value to a key of the memory.

        Args:
            key (str): key to add element to, e.g. 'state'.
            val (object): value to append to memory
        """
        assert key in self._keys
        self.dict[key].append(val)

    def update(self, key, index, val):
        """Update a value for a key at a given index.

        Args:
            key (str): key to add element to, e.g. 'state'.
            index (int): index to modify
            value (float): new td error
        """
        self.dict[key][index] = val

    def sample(self, size):
        """Sample transitions

        Args:
            size (int): number of transitions to sample from the memory

        Returns:
            list: List of sampled transitions
        """
        raise NotImplementedError()

    def get_transitions(self, indices, maxlen=1, keys=None):
        """Get recent transitions.

        Args:
            indices (list): Indices to sample (indexed with respect to the buffer structure)
            maxlen (int): Length of states history (for POMDP)
            key (str): key to get.

        Returns:
            list: List of maxlen recent transitions
        """
        indices = np.array(indices)
        assert len(indices) > 0
        if keys is None:
            keys = self._keys

        indices_with_maxlen = np.column_stack(
            [indices - maxlen + i + 1 for i in range(maxlen)]
        )
        if maxlen != 1:
            # Check if we have appended the next terminal yet or not
            within_frame = int((self.dict['state'].index -
                                self.dict['terminal'].index) % self.size)
            assert within_frame in (0, 1),\
                'We cannot have more than 1 element of difference between state and terminal'

            assert not np.any(
                self.dict['terminal'].ordered()[
                    indices_with_maxlen[:, within_frame:-1]]
            )

        transitions_sequences = {}
        for key in keys:
            if key in ['state', 'next_state']:
                transitions_sequences[key] = \
                    self.dict[key].ordered()[indices_with_maxlen]
            else:
                transitions_sequences[key] = \
                    self.dict[key].ordered()[indices[:, np.newaxis]]
        for key in keys:
            if key not in ('state', 'next_state'):
                transitions_sequences[key] = np.squeeze(
                    transitions_sequences[key], axis=1)

        return transitions_sequences

    def get_valid_indices(self, maxlen, horizon=0):
        """Find all indices in the memory that can be sampled with the maxlen and horizon constraints

        Args:
            maxlen (int): Number of stacked states.
            horizon (int): Number of states to remove from end of episode.

        Returns:
            (numpy.array): Array of valid indices.
        """
        episodes_indices = self.get_episodes_indices(
            self.dict["terminal"], maxlen + horizon)
        indices = np.array([])
        for start, finish in episodes_indices:
            shifted_start = (start + maxlen - 1) % self.size
            shifted_finish = (finish - horizon) % self.size
            if shifted_start < shifted_finish:
                indices = np.concatenate(
                    [
                        indices,
                        np.arange(shifted_start, shifted_finish + 1)
                    ]
                )
            else:
                indices = np.concatenate(
                    [
                        indices,
                        np.arange(shifted_start, self.size),
                        np.arange(shifted_finish + 1)
                    ]
                )
        return indices.astype(int)

    def get_index(self):
        """Get current index.

        Since we work with a buffer, we need to know what is the current index
        if we want to manipulate the memory.

        Returns:
            int: current index of the memory
        """
        # Only use this function after a full transition has been stored
        indices = list(map(lambda x: x.index, self.dict.values()))
        assert all(np.array(indices) == indices[0])
        return indices[0]

    def get_current_size(self):
        """Get current size
        """
        # Only use this function after a full transition has been stored
        lengths = list(map(lambda x: len(x), self.dict.values()))
        assert all(np.array(lengths) == lengths[0])
        return lengths[0]

    def get_episodes_indices(self, terminal, min_size=None):
        """Computes the positions of the first and the last frame of an episode.
        in a terminal vector
        Args:
            terminal (galileo.core.RingBuffer): buffer of terminal values.
            min_size (int): minimum episode_size under which we discard the episode
        Returns
            (list): list of tuples with first and last episode index
        """
        terminal_ = terminal.ordered()
        terminal_[-1] = True  # End of memory always terminal
        episode_ends = np.where(terminal_)[0]
        episode_starts = (np.concatenate([[episode_ends[-1]], episode_ends[:-1]]) + 1)\
            % self.get_current_size()
        episodes_indices = list(zip(episode_starts, episode_ends))
        # episodes smaller than min_size owing to partial overwrite are removed
        if min_size != None:
            # changing list while iterating is dangerous, copy to avoid issues.
            for indices in episodes_indices[:]:
                if (indices[1] - indices[0]) % self.get_current_size() < min_size:
                    episodes_indices.remove(indices)
        return episodes_indices


class Env(object):
    """Abstract class for an environment. Simplified OpenAI API.
    """

    def __init__(self):
        self.n_actions = None
        self.state_shape = None

    def step(self, action):
        """Run one timestep of the environment's dynamics.
        Accepts an action and returns a tuple (observation, reward, done, info).

        Args:
            action (numpy.array): action array

        Returns:
            tuple:
                - observation (numpy.array): Agent's observation of the current environment.
                - reward (float) : Amount of reward returned after previous action.
                - done (bool): Whether the episode has ended, in which case further step() calls will return undefined results.
                - info (str): Contains auxiliary diagnostic information (helpful for debugging, and sometimes learning).
        """
        raise NotImplementedError()

    def reset(self):
        """Reset the state of the environment and returns an initial observation.

        Returns:
            numpy.array: The initial observation of the space. Initial reward is assumed to be 0.
        """
        raise NotImplementedError()

    def render(self):
        """Render the environment.
        """
        raise NotImplementedError()


class DataIterator(object):
    """Parent class for a data iterator. Do not use directly.
    A children class should simply implement the generator method.

    Args:
        length (int): episode length
        add_timer (int): add timer to the signals

    """

    multi_episode = False
    is_finite = True

    def __init__(self, length, add_timer):
        self._data_break = False
        self._generator = self.generator()
        self.length = length
        self.has_timer = add_timer
        self.has_index = False

    def __iter__(self):
        return self

    def __next__(self):
        if self._data_break:
            self._data_break = False
            raise DataBreak
        try:
            return six.advance_iterator(self._generator)
        except StopIteration as e:
            self.end()
            raise StopIteration

    next = __next__

    def generator(self):
        raise NotImplementedError()

    def rewind(self):
        self._data_break = False
        self._generator = self.generator()

    def end(self):
        warnings.warn("End of data reached. Rewinding.")
        self.rewind()
