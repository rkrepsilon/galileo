import gym


class CartPole(object):
    def __init__(self):
        self.env = gym.make("CartPole-v0")
        self.state_shape = self.env.observation_space.shape
        self.n_actions = self.env.action_space.n

    def step(self, action):
        return self.env.step(list(action).index(1))

    def reset(self):
        return self.env.reset()

    def render(self, **kwargs):
        return self.env.render()

    def seed(self, val):
        return self.env.seed(val)
