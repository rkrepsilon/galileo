import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt

from ..core import DataBreak, Env
from ..ringbuffer import RingBuffer
from ..utils import calc_spread

plt.style.use('dark_background')
mpl.rcParams.update({
    "font.size": 15,
    "axes.labelsize": 15,
    "lines.linewidth": 1,
    "lines.markersize": 8,
    "grid.color": 'w',
    "grid.linestyle": '-.',
    "grid.linewidth": 0.25
})


class SpreadTrading(Env):
    """Class for a discrete (buy/hold/sell) spread trading environment.
    """

    _actions = {
        'hold': np.array([1, 0, 0]),
        'buy': np.array([0, 1, 0]),
        'sell': np.array([0, 0, 1])
    }

    def __init__(self,
                 data_iterator,
                 spread_coefficients,
                 trading_penalty=0,
                 flat_penalty=0,
                 exposure_penalty=0,
                 max_entries=1,
                 red_zone_start=0,
                 plot_length=100):
        """Initialisation function

        Args:
            data_iterator (galileo.core.DataIterator): A data
                generator object yielding a 1D array of bid-ask prices.
            spread_coefficients (list): A list of signed integers defining
                how much of each product to buy (positive) or sell (negative)
                when buying or selling the spread.
            the data generator should handle episodes ending through a DataBreak exception
            trading_penalty (float): penalty for trading
            flat_penalty (float): penalty for being flat
            exposure_penalty (float): penalty for being exposed
            max_entries (int): maximum consecutive entries (-1 means no limit)
            red_zone_start (int): tick at which we the launch the red zone timer
            plot_length (int): length of the plot window
        """

        assert data_iterator.n_products == len(spread_coefficients)
        self.data_iterator = data_iterator
        self._spread_coefficients = spread_coefficients
        self._first_render = True
        self._trading_penalty = trading_penalty
        self._flat_penalty = flat_penalty
        self._exposure_penalty = exposure_penalty
        self._plot_length = plot_length
        assert (max_entries > 0) |\
               (max_entries == -1), 'max_entries should be a positive integer or -1.'
        if max_entries == -1:
            self._max_entries = np.inf
        else:
            self._max_entries = max_entries
        self.n_actions = 3
        self.red_zone_start = red_zone_start
        if (self.red_zone_start > 0) and self.data_iterator.has_timer != True:
            raise ValueError(
                "iterator must output a timer for the red zone logic")

        self.state_shape = (np.shape(self.data_iterator.next())[0] + 2,)
        self.rewind()

    def reset(self):
        """Reset the trading environment. Reset rewards, Data iterator...

        Returns:
            observation (numpy.array): observation of the state
        """
        self._iteration = 0
        if not self.data_iterator.is_finite:
            self.rewind()
        self._total_reward = 0
        self._total_pnl = 0
        self._entry_price = 0
        self._closed_plot = False
        self._unrealised_pnl = 0
        self._position = 0
        current_signal = np.array(self.data_iterator.next())
        if self.data_iterator.has_timer and self.red_zone_start > 0:
            current_signal[self.data_iterator.timer_col_index] = \
                self._hourglass(
                    current_signal[self.data_iterator.timer_col_index])
        self._signal_history = RingBuffer(self._plot_length)
        self._action_history = RingBuffer(self._plot_length)
        self._valid_action_history = RingBuffer(self._plot_length)
        self._signal_history.append(current_signal)
        self._action_history.append(self._actions['hold'])
        self._valid_action_history.append(True)
        observation = self._get_observation()
        return observation

    def step(self, action):
        """Take an action (buy/sell/hold) and computes the immediate reward.

        Args:
            action (numpy.array): Action to be taken, one-hot encoded.

        Returns:
            tuple:
                - observation (numpy.array): Agent's observation of the current environment.
                - reward (float) : Amount of reward returned after previous action.
                - terminal (bool): Whether the episode has ended, in which case further step() calls will return undefined results.
                - info (dict): Contains auxiliary diagnostic information (helpful for debugging, and sometimes learning).

        """
        action = np.array(action)
        assert any([(action == x).all() for x in self._actions.values()])
        self._action_history.append(action[np.newaxis, :])
        self._valid_action_history.append(True)
        self._iteration += 1
        terminal = False
        instant_pnl = 0
        info = {}
        reward = 0
        spread_price = calc_spread(
            self._signal_history[-1][self.data_iterator.price_cols_indices],
            self._spread_coefficients
        )
        if all(action == self._actions['buy']):
            reward -= self._trading_penalty
            if (self._position >= 0) & (self._position < self._max_entries):
                self._position += 1
                self._entry_price = float(self._position - 1) / self._position \
                    * self._entry_price + spread_price[1] / float(self._position)
            elif self._position < 0:
                instant_pnl = self._entry_price - spread_price[1]  # Ask
                self._position = 0
                self._entry_price = 0
            else:  # Not valid action due to max_entries
                self._valid_action_history[-1] = False

        elif all(action == self._actions['sell']):
            reward -= self._trading_penalty
            if (self._position <= 0) & (self._position > -self._max_entries):
                self._position -= 1
                self._entry_price = float(abs(self._position) - 1) / abs(self._position)\
                    * self._entry_price + spread_price[0] / float(abs(self._position))
            elif self._position > 0:
                instant_pnl = spread_price[0] - self._entry_price  # Bid
                self._position = 0
                self._entry_price = 0
            else:  # Not valid action due to max_entries
                self._valid_action_history[-1] = False
        reward += instant_pnl
        self._total_pnl += instant_pnl
        if self._position == 0:
            reward -= self._flat_penalty
        reward -= self._exposure_penalty * abs(self._position)
        self._total_reward += reward

        # Game over logic
        try:
            current_signal = np.array(self.data_iterator.next())
            if self.data_iterator.has_timer and self.red_zone_start > 0:
                current_signal[self.data_iterator.timer_col_index] = self._hourglass(
                    current_signal[self.data_iterator.timer_col_index])
            self._signal_history.append(current_signal[np.newaxis, :])
            current_spread_price = calc_spread(
                current_signal[self.data_iterator.price_cols_indices],
                self._spread_coefficients
            )
            if self._position > 0:
                self._unrealised_pnl = current_spread_price[0] - \
                    self._entry_price
            elif self._position < 0:
                self._unrealised_pnl = self._entry_price - \
                    current_spread_price[1]
            else:
                self._unrealised_pnl = 0
        except (DataBreak, StopIteration) as e:
            terminal = True
            info['status'] = type(e)
        if self._closed_plot:
            info['status'] = 'Closed plot'

        observation = self._get_observation()

        if terminal and (self._position != 0):
            reward += self._unrealised_pnl
            self._unrealised_pnl = 0
            self._position = 0
            observation = self._get_observation()
            self._total_reward += reward
        return observation, reward, terminal, info

    def _handle_close(self, evt):
        self._closed_plot = True

    def render(self, savefig=False, filename='myfig'):
        """Matlplotlib rendering of each step.

        Args:
            savefig (bool): Whether to save the figure as an image or not.
            filename (str): Name of the image file.
            self._plot_length (int): Number of data points to plot.
        """

        if self._first_render:
            self._f, self._ax = plt.subplots(
                len(self._spread_coefficients) + int(
                    len(self._spread_coefficients) > 1),
                sharex=True)
            if len(self._spread_coefficients) == 1:
                self._ax = [self._ax]
                self._lines = [{}]
            else:
                self._lines = [{}
                               for i in range(len(self._spread_coefficients) + 1)]
            self._f.canvas.mpl_connect('close_event', self._handle_close)
            self._f.set_size_inches(
                12, 5 + 1.5 * len(self._spread_coefficients))
            for ax in self._ax:
                ax.grid(True)
            plt.show(block=False)
            self._f.canvas.draw()
        # Plot the spreads
        len_to_plot = min(self._plot_length, len(self._signal_history))
        padding_len = self._plot_length - len_to_plot
        if len(self._spread_coefficients) > 1:
            for prod_index in range(len(self._spread_coefficients)):
                bid = [self._signal_history[-i - 1][self.data_iterator.price_cols_indices[2 * prod_index]]
                       for i in range(len_to_plot)[::-1]]
                ask = [self._signal_history[-i - 1][self.data_iterator.price_cols_indices[2 * prod_index + 1]]
                       for i in range(len_to_plot)[::-1]]

                bid = [np.nan] * padding_len + list(bid)
                ask = [np.nan] * padding_len + list(ask)
                self._plot_product(bid, ask, prod_index, False)

        # Spread price
        bid, ask = zip(
            *map(lambda x: calc_spread(x[self.data_iterator.price_cols_indices], self._spread_coefficients), self._signal_history[-self._plot_length:])
        )
        bid = [np.nan] * padding_len + list(bid)
        ask = [np.nan] * padding_len + list(ask)
        self._plot_product(bid, ask, -1, True)

        buys_x = np.where(
            (self._action_history[-self._plot_length:]
             == self._actions['buy']).all(axis=1)
        )[0] + padding_len - 1
        buys_y = list(map(lambda i: ask[i], buys_x))

        sells_x = np.where(
            (self._action_history[-self._plot_length:]
             == self._actions['sell']).all(axis=1)
        )[0] + padding_len - 1
        sells_y = list(map(lambda i: bid[i], sells_x))

        cancelled_actions = np.where(
            map(lambda x: not x, self._valid_action_history[-self._plot_length:]))[0] + padding_len - 1
        cancelled_buys_x = list(set(buys_x) & set(cancelled_actions))
        cancelled_buys_y = list(map(lambda i: ask[i], cancelled_buys_x))
        cancelled_sells_x = list(set(sells_x) & set(cancelled_actions))
        cancelled_sells_y = list(map(lambda i: bid[i], cancelled_sells_x))

        if self._first_render:
            self._lines[-1]['buys'], = \
                self._ax[-1].plot(buys_x, buys_y,
                                  color='lawngreen', marker='o', linewidth=0)
            self._lines[-1]['sells'], = \
                self._ax[-1].plot(sells_x, sells_y,
                                  color='orangered', marker='o', linewidth=0)
            self._lines[-1]['cancelled_buys'], = \
                self._ax[-1].plot(cancelled_buys_x, cancelled_buys_y,
                                  color='white', marker='x', linewidth=0)
            self._lines[-1]['cancelled_sells'], = \
                self._ax[-1].plot(cancelled_sells_x, cancelled_sells_y,
                                  color='white', marker='x', linewidth=0)
        self._lines[-1]['buys'].set_xdata(buys_x)
        self._lines[-1]['buys'].set_ydata(buys_y)
        self._lines[-1]['sells'].set_xdata(sells_x)
        self._lines[-1]['sells'].set_ydata(sells_y)
        self._lines[-1]['cancelled_buys'].set_xdata(cancelled_buys_x)
        self._lines[-1]['cancelled_buys'].set_ydata(cancelled_buys_y)
        self._lines[-1]['cancelled_sells'].set_xdata(cancelled_sells_x)
        self._lines[-1]['cancelled_sells'].set_ydata(cancelled_sells_y)
        self._ax[-1].draw_artist(self._ax[-1].patch)
        self._ax[-1].draw_artist(self._lines[-1]['buys'])
        self._ax[-1].draw_artist(self._lines[-1]['sells'])
        self._ax[-1].draw_artist(self._lines[-1]['cancelled_buys'])
        self._ax[-1].draw_artist(self._lines[-1]['cancelled_sells'])

        plt.xlim(0, self._plot_length - 1)
        ticks_interval = int(self._plot_length / 10)
        plt.xticks(
            range(ticks_interval - self._iteration % ticks_interval - 1,
                  ticks_interval - self._iteration % ticks_interval + self._plot_length - 1)[::ticks_interval],
            range(ticks_interval - self._plot_length + self._iteration - self._iteration % ticks_interval,
                  ticks_interval + self._iteration - self._iteration % ticks_interval)[::ticks_interval]
        )
        plt.suptitle('Cumulated Reward: ' + "%.2f" % self._total_reward + ' ~ '
                     + 'Cumulated PnL: ' + "%.2f" % self._total_pnl + ' ~ ' +
                     'Position: ' + str(self._position) + ' ~ ' +
                     'Unrealised PnL: ' + "%.2f" % self._unrealised_pnl)
        self._f.tight_layout()
        plt.subplots_adjust(top=0.85)
        self._f.canvas.draw()

        try:
            # make sure that the GUI framework has a chance to run its event loop
            # and clear any GUI events.  This needs to be in a try/except block
            # because the default implementation of this method is to raise
            self._f.canvas.flush_events()
        except NotImplementedError:
            pass

        if savefig:
            plt.savefig(filename)
            # Next lint necessary because of matplotlib bug: https://github.com/matplotlib/matplotlib/pull/9131
            self._f.set_size_inches(
                12, 5 + 1.5 * len(self._spread_coefficients))
        self._first_render = False

    def _plot_product(self, bid, ask, prod_index, is_spread):
        if self._first_render:
            self._lines[prod_index]['bid'], =\
                self._ax[prod_index].plot(bid, color='white')
            self._lines[prod_index]['ask'], =\
                self._ax[prod_index].plot(ask, color='white')
            if is_spread:
                self._ax[prod_index].set_title('Spread')
            else:
                self._ax[prod_index].set_title('Product {} (spread coef {})'.format(
                    prod_index, str(self._spread_coefficients[prod_index])))
        self._lines[prod_index]['bid'].set_ydata(bid)
        self._lines[prod_index]['ask'].set_ydata(ask)
        ymin = np.nanmin(bid)
        ymax = np.nanmax(ask)
        ylim = ymin - (ymax - ymin) * 0.1, ymax + (ymax - ymin) * 0.1
        self._ax[prod_index].set_ylim(ylim)
        self._ax[prod_index].draw_artist(self._ax[prod_index].patch)
        self._ax[prod_index].draw_artist(self._lines[prod_index]['bid'])
        self._ax[prod_index].draw_artist(self._lines[prod_index]['ask'])

    def _get_observation(self):
        """Concatenate all necessary elements to create the observation.

        Returns:
            (numpy.array): observation array.
        """
        return np.concatenate(
            [
                self._signal_history[-1],
                np.array([self._unrealised_pnl]),
                np.array([self._position])
            ]
        )

    @staticmethod
    def random_action_fun():
        """The default random action for exploration.
        We hold 80% of the time and buy or sell 10% of the time each.

        Returns:
            (numpy.array): array with a 1 on the action index, 0 elsewhere.
        """
        return np.random.multinomial(1, [0.8, 0.1, 0.1])

    def _hourglass(self, ticks_left):
        """Calculates a proxy counter function which output is between 1 and 0.
           The closer the number is to zero, the closer we are to the end of episode.
           We decide at what time the hourglass starts decreasing from 1 to zero, and is
           .5 at half red_zone_start

        Args:
            counter (int): number of time stamps left
            start_hourglass (int): time at which

        Returns:
            (float): proxy for how close we are to end of episode
        """
        return (1 / 2.) * (1
                           - np.tanh(6. / self.red_zone_start
                                     * (self.red_zone_start / 2. - 1.0 * ticks_left)))

    @property
    def has_index(self):
        return self.data_iterator.has_index

    @property
    def index(self):
        return self.data_iterator.index

    def rewind(self):
        self.data_iterator.rewind()
