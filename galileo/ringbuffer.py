import numpy as np


class RingBuffer(object):
    """Circular buffer.

    Inspired from http://stackoverflow.com/questions/4151320/efficient-circular-buffer
    """

    def __init__(self, size, dtype='f'):
        self.data = np.array([])
        self.index = -1
        self.size = size
        self.appends = 0

    def append(self, x):
        if self.size > 0:
            if self.appends == 0:
                if isinstance(x, list):
                    x = np.array(x)
                if isinstance(x, np.ndarray):
                    self.data = np.empty(
                        shape=(self.size,) + x.shape, dtype=x.dtype)
                else:
                    self.data = np.empty(shape=self.size, dtype=type(x))
            self.index = (self.index + 1) % (self.size)
            self.data[self.index] = x
            if self.appends < self.size:
                self.appends += 1

    def ordered(self):
        if self.appends < self.size:
            return self.data[:self.appends]
        idx = (self.index + 1 + np.arange(self.size)) % self.size
        return self.data[idx]

    def __getslice__(self, i, j):
        # for consisttent behaviour with list
        i = i - self.size if i < 0 else i
        j = j - self.size if j < 0 else j
        return self.__getitem__(slice(i, j))

    def __setslice__(self, i, j, val):
        return self.__setitem__(slice(i, j), val)

    def __getitem__(self, key):
        if isinstance(key, slice):
            return self.ordered()[key]
        if isinstance(key, tuple):
            raise IndexError("Cannot access states sub elements")
        else:
            if self.appends == self.size:
                if np.any(key >= self.size) or np.any(key < - self.size):
                    raise IndexError(" list index out of RingBuffer range")
                key = (key + self.index + 1) % self.size
            else:
                if np.any(key >= self.appends) or np.any(key < -self.appends):
                    raise IndexError("list index out of RingBuffer range ")
                key = key % self.appends
            return self.data[key]

    def __setitem__(self, key, val):
        if isinstance(key, slice):
            raise NotImplementedError("Slicing not implemented for setitem")
        if isinstance(key, tuple):
            raise IndexError("Cannot access states sub elements")
        if self.appends == self.size:
            key = (key + self.index + 1) % self.size
        else:
            key = key % self.appends
        self.data[key] = val

    def __len__(self):
        return self.appends

    def __str__(self):
        return str(self.ordered())

    def __repr__(self):
        return self.__str__()
