#!/usr/bin/python
"""Run a script or launch the reporting UI.

Usage:
    galileo run [--append] [--profile=<filename>] [--name=<name>] <experiment_path>
    galileo report [<experiments_path>]
    galileo jupyter
    galileo --help

Commands:
    run                                 Run a galileo job
    report                              Launches galileo reporting tool
    jupyter                             Launches jupyter

Arguments:
    <experiment_path>                   Experiment (unique) directory path
    <experiments_path>                  Experiments (root) directory path

Options:
    -h --help                           Show this screen.
    -a --append                         Will append previous log files for this experiment.
    -p <filename> --profile=<filename>  Profiling report filepath.
    -n <name> --name=<name>             Name of the experiment.
"""

import cProfile
import os

import petname
from docopt import docopt


def main():
    args = docopt(__doc__)
    append = args["--append"]
    experiments_path = args["<experiments_path>"]
    experiment_path = args["<experiment_path>"]
    if args["--name"] is None:
        name = petname.generate(separator='_')
    else:
        name = args["--name"]
    if args["--profile"] is None:
        profile = False
    else:
        profile = True
        profile_out = args["--profile"]

    if args['run']:
        from .runner import run_job
        experiment_path = os.path.abspath(experiment_path)
        if profile:
            cProfile.runctx('run_job(experiment_path, name, append)',
                            globals(),
                            locals(),
                            filename=profile_out)
        else:
            run_job(experiment_path, name, append)
    elif args['report']:
        experiments_path = os.path.abspath(experiments_path)
        from .report import reporting
        reporting(experiments_path)

    elif args['jupyter']:
        os.system('jupyter notebook')


if __name__ == "__main__":
    main()
