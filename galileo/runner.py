import filecmp
import itertools
import json
import os
import re
import sys
import warnings
from copy import deepcopy

import numpy as np

import sklearn
import yaml
from keras import backend as K
from yaml.resolver import Resolver

from .agents.rl import *
from .brains import *
from .checkpoints import *
from .core import Brain
from .envs import *
from .iter import *
from .memories import *
from .preprocessing import *
from .report import get_experiments_directories
from .utils import build_object

sys.stdout = os.fdopen(sys.stdout.fileno(), 'w')

# YAML magic to prevent parsing of datetimes.
# See https://stackoverflow.com/questions/23812676/pyyaml-parses-900-as-int
for ch in list(u'-+0123456789'):
    del Resolver.yaml_implicit_resolvers[ch]
Resolver.add_implicit_resolver(
    u'tag:yaml.org,2002:int',
    re.compile(ur'''^(?:[-+]?0b[0-1_]+
    |[-+]?0o?[0-7_]+
    |[-+]?(?:0|[1-9][0-9_]*)
    |[-+]?0x[0-9a-fA-F_]+)$''', re.X),
    list(u'-+0123456789'))
Resolver.add_implicit_resolver(
    u'tag:yaml.org,2002:float',
    re.compile(ur'''^(?:[-+]?(?:[0-9][0-9_]*)\.[0-9_]*(?:[eE][-+][0-9]+)?
    |\.[0-9_]+(?:[eE][-+][0-9]+)?
    |[-+]?[0-9][0-9_]*(?::[0-5]?[0-9])+\.[0-9_]*
    |[-+]?\.(?:inf|Inf|INF)
    |\.(?:nan|NaN|NAN))$''', re.X),
    list(u'-+0123456789.'))


def safe_append(input_dict, key, val):
    try:
        input_dict[key].append(val)
    except:
        input_dict[key] = [val]


def get_run_log_elements(index, episodes_counter, total_frames_seen, average_reward, total_reward):
    run_log_elements = [
        {
            'name': 'episode',
            'format': '{:d}',
            'value': episodes_counter
        },
        {
            'name': 'frame',
            'format': '{:d}',
            'value': total_frames_seen
        },
        {
            'name': 'average reward',
            'format': '{:+.2E}',
            'value': average_reward
        },
        {
            'name': 'total reward',
            'format': '{:+.2E}',
            'value': total_reward
        }
    ]
    if index is not None:
        run_log_elements = [
            {
                'name': 'index',
                'format': '{}',
                'value': index
            }
        ] + run_log_elements
    return run_log_elements


def print_log(phase, log_elements):
    log_line = phase + ': ' + \
        ' | '.join([(el['name'] + ': ' + el['format']).format(el['value'])
                    for el in log_elements])
    print(log_line)


class Runner(object):
    def __init__(self, env, agent, checkpoint=TmpCheckpoint(), preprocessor=Identity(), calibration_action_fun=None):
        """Initialisation function.

        Args:
            env (galileo.core.Env): Environment to run.
            agent (galileo.core.Agent): Agent to use.
            checkpoint (galileo.checkpoints.Checkpoint): Class that controls how and when to save checkpoints for training/testing
            preprocessor (galileo.preprocessing.Preprocessor): Preprocessor for the state.
            calibration_action_fun (function): Function generating the action during calibration.
        """
        self.env = env
        self.agent = agent
        self.checkpoint = checkpoint
        self.preprocessor = preprocessor
        if calibration_action_fun is None:
            if hasattr(self.agent, '_random_action_fun'):
                self._calibration_action_fun = self.agent._random_action_fun
            else:
                self._calibration_action_fun = lambda:\
                    np.eye(self.env.n_actions)[np.random.choice(
                        self.env.n_actions,
                        p=np.ones(self.env.n_actions) / self.env.n_actions)]
                warnings.warn(
                    "No calibration function declared, all actions have equal probability to be chosen.")
        else:
            self._calibration_action_fun = calibration_action_fun
        self.epoch_frames_seen = 0

    def _calibrate(self, verbose=False):
        """Calibrate preprocessor (initial only, ongoing calibration is part of _run_one_episode) usefull when preprocessor is multi_episode
        """
        state = self.env.reset()
        episodes_counter = 0
        preprocessor_calibrated = False
        while not preprocessor_calibrated:
            action = self._calibration_action_fun()
            self.preprocessor.calibrate(state)
            preprocessor_calibrated = self.preprocessor.calibrated
            state, reward, terminal, info = self.env.step(action)
            if terminal or preprocessor_calibrated:
                self.preprocessor.end_episode()
                # This if corresponds to the case where the calibration finished in the end_episode function.
                if not preprocessor_calibrated and self.preprocessor.calibrated:
                    break
                if verbose:
                    calibration_log = get_run_log_elements(
                        self.index, episodes_counter, 0, 0, 0)
                    calibration_log_string = "{} ({}) / {} ({})".format(
                        self.preprocessor._calibration_steps_count,
                        self.preprocessor._calibration_episodes_count,
                        self.preprocessor.steps,
                        self.preprocessor.episodes
                    )
                    calibration_log += [
                        {
                            'name': 'calibration steps',
                            'format': '{:s}',
                            'value': calibration_log_string
                        }
                    ]
                    print_log(
                        phase="CALIBRATION",
                        log_elements=calibration_log
                    )
                episodes_counter += 1
                try:
                    state = self.env.reset()
                except StopIteration:
                    warnings.warn(
                        "The calibration phase has used all of available data. Rewinding.")
                    self.env.rewind()
        if hasattr(self.env, 'rewind'):
            self.env.rewind()
        if hasattr(self.agent, '_default_action'):
            self._calibration_action_fun = lambda: self.agent._default_action
        self.checkpoint.save_calibration_checkpoint(self.preprocessor)

    def _warmup(self, verbose=False):
        """Warm-up agent
        """
        episodes_counter = 0
        while self.agent.warmup:
            summary, _, end_of_epoch = self._run_one_episode(learn=False)
            if end_of_epoch:
                if hasattr(self.env, 'rewind'):
                    self.env.rewind()
                continue
            if verbose:
                phase = 'WARMUP'
                if summary == {}:
                    phase += ' [CALIBRATION]'
                average_reward = float(np.mean(summary.get('reward', np.nan)))
                total_reward = float(np.sum(summary.get('reward', np.nan)))
                run_log_elements = get_run_log_elements(
                    index=self.index,
                    episodes_counter=episodes_counter,
                    total_frames_seen=self.agent.total_frames_seen,
                    average_reward=average_reward,
                    total_reward=total_reward
                )
                print_log(phase, run_log_elements)
            episodes_counter += 1
        if hasattr(self.env, 'rewind'):
            self.env.rewind()

    def train(self,
              schedules,
              num_episodes=-1,
              epochs=1,
              verbose=True,
              learning_frequency=1):
        """Train the agent.

        Args:
            schedules (dict): dictionnary of Scheduler instances for agent parameters.
            num_episodes (int): Number of episodes to train for (-1 means for all data)
            epochs (int): number of time to loop through a finite data iterator
            verbose (bool): Whether to output the training process
            learning_frequency (int): Frequency at which the model learns (in frames). -1 means learning at the end of the episode

        Returns:
            (dict): Training summary (keys are reward, loss, epsilon and lr)
        """
        assert self.agent.learner, "If the agent is not a learner, use test directly."
        if not self.preprocessor.calibrated:
            self._calibrate(verbose)
        if hasattr(self.agent, 'warmup') and self.agent.warmup:
            self._warmup(verbose)
        if num_episodes == -1:
            num_episodes = np.inf
        training_summaries = {}
        epochs_counter = 0

        while epochs_counter < epochs:
            self.epoch_frames_seen = 0
            episodes_counter = 0
            training_summaries[epochs_counter] = {}
            while episodes_counter < num_episodes:
                for key in schedules.keys():
                    setattr(self.agent, key, schedules[key].next())

                summary, has_learned, end_of_epoch = self._run_one_episode(
                    learn=True,
                    learning_frequency=learning_frequency
                )
                # If end_of_epoch is triggered by env, we haven't seen anything (databreak + StopIteration so we break here.)
                if end_of_epoch:
                    if np.isfinite(num_episodes) and (episodes_counter != num_episodes):
                        raise ValueError(
                            "num_episodes ({}) is larger than expected ({}).".format(num_episodes, episodes_counter))
                    break

                if has_learned:
                    for key in schedules.keys():
                        val = getattr(self.agent, key)
                        safe_append(
                            training_summaries[epochs_counter], key, val)
                    safe_append(training_summaries[epochs_counter], 'length', len(
                        summary['reward']))
                    for key in summary.keys():
                        if key == 'index':
                            safe_append(
                                training_summaries[epochs_counter], 'index', summary['index'][0])
                            continue
                        val = np.nanmean(summary[key], axis=0)
                        if isinstance(val, np.ndarray):
                            val = val.tolist()
                        safe_append(
                            training_summaries[epochs_counter], key, val)

                    training_log_keys = [key for key in training_summaries[epochs_counter].keys()
                                         if key not in ('raw_state', 'preprocessed_state', 'action', 'reward', 'terminal')]
                if verbose:
                    log_elements = get_run_log_elements(
                        index=self.index,
                        episodes_counter=episodes_counter,
                        total_frames_seen=self.agent.total_frames_seen,
                        average_reward=float(np.mean(summary['reward'])),
                        total_reward=float(np.sum(summary['reward']))
                    )
                    if has_learned:
                        log_elements += [
                            {
                                'name': key,
                                'format': '{:+.2E}',
                                'value': training_summaries[epochs_counter][key][-1]
                            }
                            for key in training_log_keys if key != 'index'
                        ]
                    print_log(
                        phase='TRAINING [EPOCH {}]'.format(epochs_counter),
                        log_elements=log_elements
                    )
                episodes_counter += 1
            epochs_counter += 1
            # We rewind at the end of an epoch
            if epochs_counter < epochs and hasattr(self.env, 'rewind'):
                self.env.rewind()

            # One training log for all epochs
            training_summary = {}
            for epoch in training_summaries.keys():
                for key in training_summaries[epoch].keys():
                    safe_append(training_summary, key,
                                training_summaries[epoch][key])

            self.checkpoint.save_training_checkpoint(
                training_summary, self.agent.brain)
        return training_summary

    def test(self,
             num_episodes=1,
             render=False,
             render_frequency=1,
             verbose=True,
             record=False,
             record_name='trading_session',
             insample=False):
        """Test the agent without exploration

        Args:
            num_episodes (int): Number of episodes to test for (-1 means test for all data available)
            render (bool): Whether to render or not
            render_frequency (int): How often to render
            verbose (bool): Whether to output the training process
            record (bool): whether to record the rendering asa video
            record_name (str): string used to save the videofile (without extension). The episode number and extension (mp4) will be appended to the name.
            insample (bool): Will test in sample and therefore rewind the generator

        Returns:
            (dict): Testing summary
        """
        testing_summary = {}
        if num_episodes == -1:
            num_episodes = np.inf

        if not self.preprocessor.calibrated:
            self._calibrate(verbose)

        # TODO: create agent method turn_off_exploration as epsilon can be anything else in theory.
        self.agent.epsilon = 0
        if hasattr(self.agent, 'warmup') and self.agent.warmup:
            self.agent.warmup = False
            warnings.warn(
                "Turning warmup off for testing. Make sure this is intended.")

        if (not render) & record:
            print("render is False, setting record to False.")
            record = False

        if insample and hasattr(self.env, 'rewind'):
            self.env.rewind()
        episodes_counter = 0
        while episodes_counter < num_episodes:
            render_ = render & (episodes_counter % render_frequency == 0)
            summary, _, end_of_epoch = self._run_one_episode(
                learn=False,
                render=render_,
                record=record,
                record_name=record_name + '_' + str(episodes_counter)
            )

            # If end_of_epoch is triggered by env, we haven't seen anything (databreak + StopIteration so we break here.)
            if end_of_epoch:
                if np.isfinite(num_episodes) and (episodes_counter != num_episodes):
                    raise ValueError("num_episodes ({}) is larger than expected ({}).".format(
                        num_episodes, episodes_counter))
                break
            for key in ("reward", "raw_state", "preprocessed_state", "action", "terminal"):
                safe_append(testing_summary, key, summary[key])
            if hasattr(self.env, 'has_index'):
                if self.env.has_index:
                    safe_append(testing_summary, 'index', summary['index'])

            if verbose:
                log_elements = get_run_log_elements(
                    index=self.index,
                    episodes_counter=episodes_counter,
                    total_frames_seen=self.agent.total_frames_seen,
                    average_reward=float(np.mean(summary['reward'])),
                    total_reward=float(np.sum(summary['reward']))
                )
                print_log('TESTING', log_elements)
            episodes_counter += 1

        self.checkpoint.save_testing_checkpoint(testing_summary)
        return testing_summary

    def _run_one_episode(self,
                         learn,
                         learning_frequency=1,
                         render=False,
                         record=False,
                         record_name='trading_session'):
        """Run one episode.

        Args:
            learn (bool): learning phase or not
            learning_frequency (int): Frequency at which the model learns (in frames). -1 means learning at the end of the episode, 0 means no learning (redundant wrt learn)
            render (bool): Whether to render or not
            record (bool): whether to record the rendering as a video
            record_name (str): string used to save the videofile (without extension). The episode number and extension (mp4) will be appended to the name.

        Returns:
            (tuple)
                (dict): Episode summary.
                (bool): Whether has learned or not.
                (bool): Whether is end of file or not.
        """

        summary = {}
        terminal = False
        has_learned = False
        # When we reset after after a databreak, we might be at the EOF.
        # This will raise StopIteration, we catch it here.
        preprocessor_calibrated = self.preprocessor.calibrated
        try:
            raw_state = self.env.reset()  # In here, we next, and therefore rewind
            if preprocessor_calibrated:
                preprocessed_state = self.preprocessor.transform(raw_state)
            self.preprocessor.calibrate(raw_state)
        except StopIteration:
            end_of_epoch = True
            return summary, has_learned, end_of_epoch
        end_of_epoch = False
        while not terminal:
            if render:
                self.env.render(
                    savefig=record,
                    filename=record_name +
                    '_%05d.png' % self.agent.episode_frames_seen)

            if preprocessor_calibrated:
                action = self.agent.act(preprocessed_state)
                self.epoch_frames_seen += 1
            else:
                action = self._calibration_action_fun()

            next_raw_state, reward, terminal, info = self.env.step(action)

            loss_dict = {}
            transition = {}

            preprocessor_calibrated = self.preprocessor.calibrated

            if preprocessor_calibrated:
                next_preprocessed_state = self.preprocessor.transform(
                    next_raw_state)
                if self.agent.episode_frames_seen >= 1:
                    if hasattr(self.agent, 'observe'):
                        self.agent.observe(
                            next_preprocessed_state, reward, terminal)
                    transition = dict(zip(
                        ["raw_state", "preprocessed_state",
                            "action", "reward", "terminal"],
                        [raw_state, preprocessed_state, action, reward, terminal]))
                    frequency_learning = (self.epoch_frames_seen % learning_frequency == 0) and \
                        (learning_frequency != -1)
                    last_frame_learning = (
                        learning_frequency == -1) and terminal

                    if learn and (last_frame_learning or frequency_learning):
                        loss_dict, has_learned_once = self.agent.learn()
                        if has_learned_once and not has_learned:
                            has_learned = True

                preprocessed_state = next_preprocessed_state
                raw_state = next_raw_state

                for key, val in list(six.iteritems(loss_dict)) + list(six.iteritems(transition)):
                    if isinstance(val, np.ndarray):
                        val = val.tolist()
                    safe_append(summary, key, val)

                if self.index is not None:
                    safe_append(summary, 'index', self.index)

            self.preprocessor.calibrate(raw_state)
        self.preprocessor.end_episode()
        self.agent.end()

        if record:
            cmd = "ffmpeg -f image2 -r 8 -i " +\
                "{}_%05d.png".format(record_name) +\
                " -vcodec mpeg4  -qscale:v 3 -y " + record_name + ".mp4"
            os.system(cmd)
            os.system("rm " + record_name + "*.png")

        return summary, has_learned, end_of_epoch

    @property
    def index(self):
        if hasattr(self.env, 'has_index'):
            if self.env.has_index:
                return self.env.index
        else:
            return None


def run_job(root_experiment_path, name, append=False):

    assert os.path.isabs(root_experiment_path), "Path must be absolute."
    if os.path.exists(os.path.join(root_experiment_path, 'config.yaml')):
        root_config_filepath = os.path.join(
            root_experiment_path, 'config.yaml')
    elif os.path.exists(os.path.join(root_experiment_path, 'config.json')):
        root_config_filepath = os.path.join(
            root_experiment_path, 'config.json')
    else:
        raise ValueError(
            "No config file found in folder {}.".format(root_experiment_path))

    existing_experiments = get_experiments_directories(root_experiment_path)
    for experiment in existing_experiments:
        exp_config_filepath = os.path.join(
            experiment, os.path.basename(root_config_filepath))
        if not os.path.exists(exp_config_filepath):
            raise ValueError("Expecting a config file at {}.".format(
                exp_config_filepath))
        if not filecmp.cmp(root_config_filepath, exp_config_filepath):
            raise ValueError("Config files {} and {} don't match.".format(
                root_config_filepath, exp_config_filepath))

    if os.path.exists(os.path.join(root_experiment_path, name)):
        if not append:
            raise ValueError(
                "Directory {} already exists and appending not enabled.".format(root_experiment_path))
        else:
            experiment_path = os.path.join(root_experiment_path, name)
            config_filepath = os.path.join(
                experiment_path, os.path.basename(root_config_filepath))
            if not filecmp.cmp(root_config_filepath, config_filepath):
                raise ValueError("Config files {} and {} don't match.".format(
                    root_config_filepath, config_filepath))
    else:
        experiment_path = os.path.join(root_experiment_path, name)
        os.makedirs(experiment_path)
        config_filepath = os.path.join(
            experiment_path, os.path.basename(root_config_filepath))
        shutil.copyfile(root_config_filepath, config_filepath)
    print("Running from config {}".format(config_filepath))
    ext = config_filepath.split('.')[-1]
    with open(config_filepath, 'r') as f:
        if ext in ('yaml', 'json'):
            config = eval(ext).load(f)
        else:
            raise ValueError("Configuration extension not supported.")
    config['runner']['config']['checkpoint'] = {
        'class_name': 'galileo.checkpoints.Checkpoint',
        'config': {
            'experiment_path': experiment_path,
            'should_append': append}
    }
    runner = build_object(config['runner'])

    if 'train' in config and config['train']['num_episodes'] != 0:
        config['train']['schedules'] = {
            key: build_object(object_dict)
            for key, object_dict in six.iteritems(config['train']['schedules'])
        }
        print("Training started...")
        runner.train(**config['train'])

    if 'test' in config and config['test']['num_episodes'] != 0:
        print("Testing started...")
        runner.test(**config['test'])
