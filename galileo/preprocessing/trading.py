import datetime as dt
import time
from collections import Counter, deque

import numpy as np
import pytz

import dill

from ..preprocessing import (EMA, Chainer, Composer, Identity, Lambda,
                             Preprocessor)
from ..ringbuffer import RingBuffer
from ..utils import calc_spread, linear_interpolation


def returns(mode='normal', continuous=True, name=None, verbose=False):
    if mode == 'normal':
        return Lambda(
            fun=lambda x: x[1] / x[0] - 1, steps=1, continuous=continuous, name=name, verbose=verbose)
    elif mode == 'log':
        return Lambda(
            fun=lambda x: np.log(x[1] / x[0]), steps=1, continuous=continuous, name=name, verbose=verbose)


def spread(spread_coefficients, name=None, verbose=False):
    return Lambda(
        fun=lambda x: np.array(calc_spread(x, spread_coefficients)),
        steps=0,
        continuous=False)


def bid_ask_spread(name=None, verbose=False):
    return Lambda(
        fun=lambda x: np.array([x[1] - x[0]]), steps=0, continuous=False, name=name, verbose=verbose)


def squeeze(axis=None, name=None, verbose=False):
    return Lambda(
        fun=lambda x: np.squeeze(x, axis=axis), steps=0, continuous=False, name=name, verbose=verbose)


class MACDSignal(Composer):
    def __init__(self,
                 window_short=12,
                 window_long=26,
                 window_signal=9,
                 steps=0,
                 dt=1,
                 name=None,
                 verbose=False):

        if window_long < window_short:
            raise ValueError(
                'The long window is shorter than the short window.')
        if steps % 2 != 0:
            raise ValueError(
                'MACDSignal needs to be initialised with an even number of steps.')
        steps_each_ema = steps / 2
        ema_short = EMA(window=window_short, steps=steps_each_ema, dt=dt)
        ema_long = EMA(window=window_long, steps=steps_each_ema, dt=dt)
        ema_signal = EMA(window=window_signal, steps=steps_each_ema, dt=dt)
        macd = ema_short - ema_long
        super(MACDSignal, self).__init__(
            [macd, Chainer([Identity(), ema_signal])], name=name, verbose=verbose)


# TODO: for dst, include timezone.
class IntraDaySeasonality(Preprocessor):
    """Create a regularly sampled seasonality profile for the input state.

    Args:
        episodes (int): Number of episode to calibrate for. Must be strictly positive.
        frequency (float): Sampling frequency in seconds.
        continuous (bool): Will keep on calibrating after initial calibration (rolling fit)
        handle_duplicates (str): Method to handle duplicates ('last' or 'sum')
    """

    calibrated_init = False
    multi_episode = True
    seconds_in_day = 86400
    _can_be_saved = True

    def __init__(self, episodes, frequency, continuous=False, name=None, verbose=False):
        assert episodes > 0
        self.episodes = episodes
        self.continuous = continuous
        self.frequency = frequency

        super(IntraDaySeasonality, self).__init__(
            multi_episode=self.multi_episode,
            name=name,
            verbose=verbose)

        self._time_grid = np.arange(
            0, self.seconds_in_day, self.frequency)
        self._grid_len = len(self._time_grid)
        self._last_grid_index = None
        self._last_time = None
        self._seasonality = None
        self._episodes_interpolations = None

    @staticmethod
    def _get_time_from_midnight(timestamp):
        datetime = dt.datetime.utcfromtimestamp(timestamp)
        return (datetime - dt.datetime.combine(datetime.date(), dt.time(0))).total_seconds()

    def _get_grid_index(self, time):
        return int(time / self.frequency)

    def _calibrate(self, state):
        """
        Args:
            state (np.array): 1D array with first value being the timestamp
        """
        if len(state[1:]) == 0:
            ValueError("state is composed of only one value. Irregular EMA requires the first element of the state to be a timesamp, followed by the values to average.")

        time = self._get_time_from_midnight(state[0]) % self.seconds_in_day
        grid_index = self._get_grid_index(time)
        data = state[1:]

        assert len(data.shape) == 1

        if self._episodes_interpolations is None:
            self._episodes_interpolations = np.nan * \
                np.ones((self.episodes, self._grid_len, len(data)))

        if self._last_grid_index is None:
            self._last_grid_index = grid_index

        if self._last_time is None:
            self._last_time = time
            self._last_data = data

        delta_grid = grid_index - self._last_grid_index
        for i in range(delta_grid):
            grid_time = self._time_grid[grid_index - i]
            if time < self._last_time:
                time_shift = self.seconds_in_day
            else:
                time_shift = 0
            values = linear_interpolation(
                [self._last_time, time + time_shift],
                np.stack([self._last_data, data]).T,
                grid_time + time_shift
            )
            self._episodes_interpolations[self._calibration_episodes_count,
                                          grid_index - i, :] = values

        if time in self._time_grid:
            self._episodes_interpolations[self._calibration_episodes_count,
                                          grid_index, :] = data

        self._last_grid_index = grid_index
        self._last_time = time
        self._last_data = data

    def _transform(self, state):
        """
        Args:
            state (np.array): 1D array with first value being the timestamp
        """
        query_time = self._get_time_from_midnight(
            state[0]) % self.seconds_in_day
        return linear_interpolation(self._time_grid, self._seasonality.T, query_time)

    def _end_episode(self):
        self._last_grid_index = None
        self._last_time = None
        if (not self.calibrated) or (self.continuous):
            self._seasonality = np.nanmean(
                self._episodes_interpolations, axis=0)

    def save(self, root_path):
        root_path += '_' + self.__class__.__name__.lower()
        if self._can_be_saved:
            with open(root_path + '.pkl', 'w+') as file_:
                dill.dump(obj=[self._time_grid,
                               self._seasonality], file=file_)
