"""
Classes to preprocess the incoming raw signals from the environment before sending them to the agent.
"""

import warnings
# Not in core due to circular imports
from abc import ABCMeta, abstractmethod
from collections import deque
from copy import copy, deepcopy

import numpy as np

from sklearn.exceptions import NotFittedError
from sklearn.preprocessing import *
from sklearn.utils.validation import check_is_fitted

from ..ringbuffer import RingBuffer
from ..utils import calc_ema


class Preprocessor(object):
    """
    Class to preprocess the state coming from the environemnt before feeding it to the brain.

    Args:
        multi_episode (bool): Will not reset the preprocessor after end of episode
    """

    __metaclass__ = ABCMeta

    def __init__(self, multi_episode=False, name=None, verbose=False):
        if not hasattr(self, 'continuous'):
            raise ValueError(
                "A preprocessor should specify the attribute continuous (controls whether the calibration continues after the initial calibration).")
        if not hasattr(self, 'calibrated_init'):
            raise ValueError(
                "A preprocessor should specify the attribute calibrated_init (whether the preprocessor requires calibration or not).")
        if not (hasattr(self, 'steps') or hasattr(self, 'episodes')):
            raise ValueError(
                "A preprocessor should specify the attribute steps or episodes (number of steps or episodes to calibrate).")
        if not hasattr(self, 'steps'):
            self.steps = 0
        if not hasattr(self, 'episodes'):
            self.episodes = 0
        self.calibrated = self.calibrated_init
        self._calibration_steps_count = 0
        self._calibration_episodes_count = 0

        # A composer with episodes > 0 can be non multi_episode.
        self.multi_episode = multi_episode or (
            (self.steps == 0) and (not self.continuous))

        if verbose and (name is None):
            raise ValueError("If verbose is True, name must be specified.")
        self.name = name
        self.verbose = verbose

    def calibrate(self, state):
        if self.verbose:
            print('Calibrating {}'.format(self.name))
        if (not self.calibrated) | self.continuous:
            self._calibrate(state)
        if not self.calibrated:
            if self._calibration_steps_count < self.steps:
                self._calibration_steps_count += 1
            if (self._calibration_episodes_count >= self.episodes) and \
                    (self._calibration_steps_count >= self.steps):
                self.calibrated = True

    @abstractmethod
    def _calibrate(self, state):
        raise NotImplementedError()

    def transform(self, state):
        """Should NOT change the internal state"""
        if self.verbose:
            print('Transforming {}'.format(self.name))
        if not self.calibrated:
            raise ValueError(
                "Preprocessor must be calibrated before using transform method."
            )
        result = self._transform(state)
        return result

    @abstractmethod
    def _transform(self, state):
        raise NotImplementedError()

    def reset(self):
        if not self.multi_episode:
            if self.verbose:
                print('Reseting {}'.format(self.name))
            self._calibration_steps_count = 0
            self.calibrated = self.calibrated_init
            self._reset()

    def end_episode(self):
        """End of episode logic for a preprocessor.
        """
        if self.verbose:
            print('End of episode for {}'.format(self.name))
        if (not self.calibrated) and (self._calibration_episodes_count < self.episodes):
            self._calibration_episodes_count += 1
        self._end_episode()
        self.calibrated = (self._calibration_episodes_count >= self.episodes) and \
            (self._calibration_steps_count >= self.steps)
        # TODO: Lots of duplicate calls due to composed passing both end_episode and reset to levels below.
        self.reset()

    def _end_episode(self):
        pass

    def _reset(self):
        pass

    def save(self, root_path):
        pass

    def __add__(self, other):
        return self._overload_arithmetic(self, other, np.sum)

    def __mul__(self, other):
        return self._overload_arithmetic(self, other, np.prod)

    def __sub__(self, other):
        minus_other = Composer([other, Lambda(lambda x: -1 * x)])
        return self + minus_other

    def __div__(self, other):
        recip_other = Composer([other, Lambda(lambda x: np.divide(1.0, x))])
        return self * recip_other

    @staticmethod
    def _overload_arithmetic(a, b, func):
        # stacking ensures the dimensions are consistent
        stacked = Stacker([a, b], axis=0)
        arithmetic = Lambda(lambda x: func(x, axis=0))
        return Composer([stacked, arithmetic], name='overload_arithmetic', verbose=a.verbose)


class Ffill(Preprocessor):
    continuous = False
    calibrated_init = True
    steps = 0

    def __init__(self, nullfill=0, name=None, verbose=False):
        self._last_values = None
        self._nullfill = nullfill
        super(Ffill, self,).__init__(
            multi_episode=False, name=name, verbose=verbose)

    def _calibrate(self, state):
        pass

    def _transform(self, state):
        if self._last_values is None:
            self._last_values = self._nullfill * np.ones_like(state)
        transformed_state = copy(state)
        transformed_state[np.logical_not(np.isfinite(
            transformed_state))] = self._last_values[np.logical_not(np.isfinite(transformed_state))]
        self._last_values = transformed_state
        return transformed_state

    def _end_episode(self):
        self._last_values = None


class Identity(Preprocessor):
    """ Identity Class, leaves state as is
    """

    continuous = False
    calibrated_init = True
    steps = 0

    def _calibrate(self, state):
        pass

    def _transform(self, state):
        return state


class RBPreprocessor(Preprocessor):
    """
    Ring Buffer preprocessor

    Args:
        steps (int): number of calibration steps
        multi_episode (bool): whether the preprocessor must be erased at the end of the episode.
        continuous (bool): Continuously calibrate preprocessor throughout epsisode
    """
    __metaclass__ = ABCMeta

    def __init__(self, steps=0, multi_episode=False, continuous=False, name=None, verbose=False):
        self.calibrated_init = (steps == 0)
        self.steps = steps
        self.continuous = continuous
        self.calibration_state = deque(maxlen=steps)
        super(RBPreprocessor, self).__init__(
            multi_episode=multi_episode, name=name, verbose=verbose)

    def _calibrate(self, state):
        self.calibration_state.append(state)

    def _reset(self):
        self.calibration_state = deque(maxlen=self.steps)


class Lambda(RBPreprocessor):
    """
    Lambda function state preprocessor

    Args:
        continuous (bool): Continuously calibrate preprocessor throughout epsisode
        calibrated_init (bool): initial calibration state of the preprocessor
        steps (int): number of calibration steps
        fun (function): function with which to process the calibrated state
        multi_episode (bool): whether the preprocessor must be erased at the end of the episode.
    """

    def __init__(self, fun, steps=0, multi_episode=False, continuous=False, name=None, verbose=False):
        self.fun = fun
        if (steps == 0) and continuous:
            raise ValueError(
                'Lambda is not defined when steps is 0 and continuous is set to True.'
            )
        super(Lambda, self).__init__(steps=steps, multi_episode=multi_episode,
                                     continuous=continuous, name=name, verbose=verbose)

    def _transform(self, state):
        if self.steps == 0:
            return self.fun(state)
        else:
            state_with_history = np.concatenate([
                list(self.calibration_state), [state]])
            return self.fun(state_with_history)


class Cumsum(Preprocessor):
    continuous = True
    calibrated_init = True
    steps = 0

    def __init__(self, name=None, verbose=False):
        self._sum = 0
        self._first = True
        super(Cumsum, self).__init__(
            multi_episode=False, name=name, verbose=verbose)

    def _calibrate(self, state):
        if self._first:
            self._first = False
        else:
            self._sum += state

    def _transform(self, state):
        if self._first:
            self._sum += state
        return self._sum

    def _reset(self):
        self._sum = 0
        self._first = True


def diff(order=1, name=None, verbose=False):
    return compose([
        Lambda(lambda x: x[1] - x[0], steps=1,
               continuous=True, name=name, verbose=verbose)
        for _ in range(order)
    ])


def rolling_mean(window_size=60, name=None, verbose=False):
    # - 1 because we always calibrate after we transform and state is added
    return Lambda(np.mean, steps=window_size - 1, continuous=True, name=name, verbose=verbose)


class Selector(Preprocessor):
    """ Selector preprocessor.

    Args:
        selection (sorted list, numpy.array or tuple): Selector indices array or tuple of arrays as returned by numpy.where.
                                                Transform will flatten the input state unless selection is a valid mask.
        invert (bool): Will invert the selection
    """
    continuous = False
    calibrated_init = True
    steps = 0

    def __init__(self, selection, invert=False, name=None, verbose=False):
        self._mask_selection = False
        self._invert = invert
        if isinstance(selection, list):
            selection = np.array(selection)
        if isinstance(selection, np.ndarray):
            if selection.dtype == bool:
                self._mask_selection = True
        elif isinstance(selection, int):
            selection = np.array([selection])
        else:
            raise TypeError("Unsupported type for selection")
        self.selection = selection
        super(Selector, self).__init__(name=name, verbose=verbose)

    def _calibrate(self, state):
        pass

    def _build_mask(self, state):
        if not self._mask_selection:
            self.mask = np.zeros_like(state, dtype=bool)
            if len(self.selection.shape) == 1:
                self.selection = (self.selection % len(state))
                if self.selection.tolist() != sorted(
                        self.selection.tolist()):
                    raise ValueError("The selection list must be sorted.")
                # As would be returned by numpy.where when shape = (N,)
                self.selection = (self.selection, )
            self.mask[self.selection] = True
        else:
            if (self.selection.shape != state.shape):
                ValueError(
                    "The mask and the state should have the same shape.")
            self.mask = self.selection
        if self._invert:
            self.mask = np.logical_not(self.mask)

    def _transform(self, state):
        if not hasattr(self, 'mask'):
            self._build_mask(state)
        return np.atleast_1d(state[self.mask])


def select(selection, name=None, verbose=False):
    return Selector(selection, name=name, verbose=verbose)


class Composer(Preprocessor):
    """
    Class to compose several preprocessing operations between them

    Args:
        preprocessors (list): list of preprocessors
    """

    def __init__(self, preprocessors, name=None, verbose=False):
        self.steps = sum(map(lambda p: p.steps, preprocessors))
        self.episodes = sum(
            map(lambda p: p.episodes, preprocessors))
        self.continuous = any(map(lambda p: p.continuous, preprocessors))
        self.calibrated_init = all(
            map(lambda p: p.calibrated_init, preprocessors))
        self.preprocessors = map(deepcopy, preprocessors)
        multi_episode = all(map(lambda p: p.multi_episode, preprocessors))
        super(Composer, self).__init__(
            multi_episode=multi_episode, name=name, verbose=verbose)

    def _calibrate(self, state):
        state_ = copy(state)
        for preprocessor in self.preprocessors:
            if preprocessor.calibrated and (preprocessor != self.preprocessors[-1]):
                state__ = preprocessor.transform(state_)
                break_ = False
            else:
                break_ = True
            preprocessor.calibrate(state_)
            if break_:
                break
            state_ = state__

    def _transform(self, state):
        for preprocessor in self.preprocessors:
            state = preprocessor.transform(state)
        return state

    def _reset(self):
        for preprocessor in self.preprocessors:
            preprocessor.reset()
        self._calibration_steps_count = self.steps - \
            sum(map(lambda p: p.steps - p._calibration_steps_count, self.preprocessors))
        self.calibrated = self.calibrated_init

    def save(self, root_path):
        root_path += '_' + self.__class__.__name__.lower()
        cnt = 0
        for preprocessor in self.preprocessors:
            preprocessor.save(root_path + '_{}'.format(cnt))
            cnt += 1

    def _end_episode(self):
        for preprocessor in self.preprocessors:
            preprocessor.end_episode()


def compose(preprocessors, name=None, verbose=False):
    return Composer(preprocessors, name=name, verbose=verbose)

# TODO Merge some of chainer and composer logic....


class Chainer(Preprocessor):
    """Class to compose several preprocessing operations between them

    Args:
        preprocessors (list): list of preprocessors
    """

    def __init__(self, preprocessors, name=None, verbose=False):
        self.steps = max(map(lambda p: p.steps, preprocessors))
        self.episodes = max(
            map(lambda p: p.episodes, preprocessors))
        self.continuous = any(map(lambda p: p.continuous, preprocessors))
        self.calibrated_init = all(
            map(lambda p: p.calibrated_init, preprocessors))
        self.preprocessors = map(deepcopy, preprocessors)

        multi_episode = all(map(lambda p: p.multi_episode, preprocessors))
        super(Chainer, self).__init__(
            multi_episode=multi_episode, name=name, verbose=verbose)

    def _calibrate(self, state):
        for preprocessor in self.preprocessors:
            preprocessor.calibrate(state)

    def _transform(self, state):
        # TODO: Logic for  preprocessors of different dimensions
        # TODO: (related to previous) specify concatenation axis
        return np.concatenate([np.atleast_1d(preprocessor.transform(state)) for preprocessor in self.preprocessors])

    def _reset(self):
        self.calibrated = self.calibrated_init
        for preprocessor in self.preprocessors:
            preprocessor.reset()
        self._calibration_steps_count = self.steps\
            - max(map(lambda p: p.steps -
                      p._calibration_steps_count, self.preprocessors))

    def save(self, root_path):
        root_path += '_' + self.__class__.__name__.lower()
        cnt = 0
        for preprocessor in self.preprocessors:
            preprocessor.save(root_path + '_{}'.format(cnt))
            cnt += 1

    def _end_episode(self):
        for preprocessor in self.preprocessors:
            preprocessor.end_episode()


def chain(preprocessors, name=None, verbose=False):
    return Chainer(preprocessors, name=name, verbose=verbose)


class Stacker(Chainer):
    """
    Class to carry out multiple preprocessing operations and return
    results stacked along a new dimension

    Args:
        preprocessors (list): list of preprocessors
        axis (int, optional): index in array to create new dimension for stacking. Default is 0 (first).
    """

    def __init__(self, preprocessors, axis=0, name=None, verbose=False):
        self.axis = axis
        super(Stacker, self).__init__(
            preprocessors=preprocessors, name=name, verbose=verbose)

    def _transform(self, state):
        for i, preprocessor in enumerate(self.preprocessors):
            if i == 0:
                state_ = np.atleast_1d(preprocessor.transform(state))
            else:
                state_ = np.stack(
                    [
                        state_,
                        np.atleast_1d(preprocessor.transform(state))
                    ], axis=self.axis
                )
        return state_


def stack(preprocessors, name=None, verbose=False):
    return Stacker(preprocessors, name=name, verbose=verbose)


class EMA(Preprocessor):
    """
    EMA preprocessor

    Args:
        window (float): Exponential moving average half life
        steps (int): Number of steps to calibrate for.
        regular (bool): Regularly time sampled or not. When non regular the first element of the state should be a unix timestamp
    """

    continuous = True

    def __init__(self, window, steps=0, regular=True, dt=1., mode='average', name=None, verbose=False):
        self.calibrated_init = (steps == 0)
        self._tau = window / np.log(2)
        self.steps = steps
        self.regular = regular
        self.mode = mode
        self.dt = dt
        self._ema = None
        self._last_timestamp = None
        super(EMA, self).__init__(
            multi_episode=False, name=name, verbose=verbose)

    def _calibrate(self, state):
        state = np.atleast_1d(state)
        assert len(state.shape) == 1  # Must be 1D
        if self._ema is None:
            assert self._calibration_steps_count == 0
            self._ema = self._get_value_array(state)
        else:
            self._ema = calc_ema(self._get_value_array(state), self._ema, 1. /
                                 self._tau, self._get_dt(state), self.mode)
        if not self.regular:
            self._last_timestamp = state[0]

    def _transform(self, state):
        state = np.atleast_1d(state)
        assert len(state.shape) == 1  # Must be 1D
        if self._ema is None:
            assert self.calibrated_init
            self._ema = self._get_value_array(state)
            if not self.regular:
                self._last_timestamp = state[0]
        ema = calc_ema(self._get_value_array(state), self._ema,
                       1. / self._tau, self._get_dt(state), self.mode)
        return np.atleast_1d(np.array(ema))

    def _get_value_array(self, state):
        if self.regular:
            return state[:]
        else:
            return state[1:]

    def _get_dt(self, state):
        if self.regular:
            return self.dt
        else:
            return state[0] - self._last_timestamp

    def _reset(self):
        self._ema = None
        if not self.regular:
            self._last_timestamp = None

# TODO: Not tested


def EMSTD(window, steps=0, regular=True, dt=1., name=None, verbose=False):
    ema = EMA(window, steps=steps, regular=regular, dt=dt,
              name=name, verbose=verbose)
    if not regular:
        data = Selector(selection=0, invert=True)
        squared_deviation = chain(
            [
                Selector(selection=0),
                compose(
                    [
                        data - ema,
                        Lambda(fun=lambda x: np.power(x, 2))
                    ]
                )
            ]
        )
    else:
        data = Identity()
        squared_deviation = compose(
            [
                data - ema,
                Lambda(fun=lambda x: np.power(x, 2))
            ]
        )

    return compose(
        [
            squared_deviation,
            deepcopy(ema),
            Lambda(fun=np.sqrt)
        ]
    )
