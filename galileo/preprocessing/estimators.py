import warnings
from copy import copy

import numpy as np

import dill

from ..utils import lagged_features, lookahead_targets
from .base import RBPreprocessor

# TODO: To specify an estimator calibration's length in episodes, we cannot use a RBPreprocessor.


class Estimator(RBPreprocessor):
    """Wrapper for a scikit estimator (predictor or transformer).
    The calibration mehod calls fit of the scikit object.

    Args:
        estimator (object): Scikit learn predictor or transformer or filepath to dilld file
        steps (int): How many steps are used to fit
        rolling (bool): Fit on a rolling window (must be a continuous preprocessor)
        continuous (bool): Continuously calibrate preprocessor throughout epsisode
        multi_episode (bool): Will not reset the preprocessor after end of episode
    """

    def __init__(self,
                 estimator,
                 steps,
                 rolling=False,
                 continuous=False,
                 multi_episode=True,
                 name=None,
                 verbose=False):

        if type(estimator) is str:
            with open(estimator, 'r') as file_:
                estimator = dill.load(file_)
            if not self._is_fitted(estimator):
                raise ValueError("Can only load fitted estimators.")
        if self._is_fitted(estimator):
            if steps != 0:
                warnings.warn(
                    'The estimator is already fitted. We reset steps from {} to 0 (multi_episode is True as a result).'.
                    format(steps))
                steps = 0
            if rolling:
                raise ValueError(
                    "If the input estimator is already fitted, we assume rolling is False. Currently, rolling is set to True."
                )
            if continuous:
                raise ValueError(
                    "If the input estimator is already fitted, we assume continuous is False. Currently, continuous is set to True."
                )
        else:
            assert steps > 0

        self._can_be_saved = False
        if (not rolling) and (not continuous):
            self._can_be_saved = True
        self.estimator = estimator
        self.initial_estimator = copy(estimator)
        if rolling and (not continuous):
            raise ValueError(
                "When rolling is True, continuous must also be True.")
        self.rolling = rolling
        super(Estimator, self).__init__(
            steps=steps,
            continuous=continuous,
            multi_episode=multi_episode,
            name=name,
            verbose=verbose)

    def _is_fitted(self, estimator):
        return len(list(filter(lambda x: x[-1] == '_', estimator.__dict__.keys()))) > 0

    def _reset(self):
        super(Estimator, self)._reset()
        self.estimator = copy(self.initial_estimator)

    def save(self, root_path):
        root_path += '_' + self.__class__.__name__.lower()\
            + '_' + self.estimator.__class__.__name__.lower()
        if self._can_be_saved:
            with open(root_path + '.pkl', 'w+') as file_:
                dill.dump(obj=self.estimator, file=file_)
        else:
            warnings.warn(
                "Cannot be saved since rolling ({}) or continuous ({}).".
                format(self.rolling, self.continuous))


class Transformer(Estimator):

    def _calibrate(self, state):
        super(Transformer, self)._calibrate(state)
        if (self.rolling and (self._calibration_steps_count >= self.steps - 1)) or\
           ((not self.rolling) and (self._calibration_steps_count == self.steps - 1) and
                (not self.continuous)):
            X = np.array(self.calibration_state)
            self.estimator.fit(X)
        elif (not self.rolling) and self.continuous:
            X = [state]
            self.estimator.partial_fit(X)

    def _transform(self, state):
        return np.squeeze(self.estimator.transform([state]))


class Predictor(Estimator):
    """Wrapper for a scikit estimator (predictor or transformer).
    The calibration method calls fit of the scikit object.

    Args:
        estimator (scikit learn object): Scikit learn predictor or transformer object.
        target_fun (function): function to select and design model targets from state vector.
        input_fun (function): function to select and design model features from state vector. (default: lambda x: x)
        maxlen (int): Number of historical prices to stack for prediction. (default: 1)
        time_distributed (bool): Will not wrap the time axis of the stacking and let the model handle it. (default: False)
        lookahead_vector (list): Vector of forecast horizon, must be integers. (default: [0])
        lookahead_reduce (function): Reduce the vector of length lookahead to a single array. (default: lambda x: x[-1])
        input_training_filter (function): Filter the input training set before training. (default: lambda x: True)
        target_training_filter (function): Filter the target training set before training. (default: lambda x: True)
        predict_fun_name (string): Name of the prediction function to use. (default: 'predict')
    """

    def __init__(self,
                 estimator,
                 steps,
                 target_fun,
                 input_fun=lambda x: x,
                 maxlen=1,
                 time_distributed=False,
                 lookahead_vector=[0],
                 lookahead_reduce=lambda x: x[-1],
                 input_training_filter=lambda x: True,
                 target_training_filter=lambda x: True,
                 predict_fun_name='predict',
                 rolling=False,
                 continuous=False,
                 multi_episode=True,
                 save_training_set=False,
                 name=None,
                 verbose=False):
        if steps < maxlen:
            raise ValueError("steps must be greater or equal to maxlen.")
        self._maxlen = maxlen
        self._target_fun = target_fun
        self._time_distributed = time_distributed
        if isinstance(lookahead_vector, int):
            lookahead_vector = [lookahead_vector]
        self._lookahead_vector = lookahead_vector
        self._max_lookahead = max(self._lookahead_vector)
        self._save_training_set = save_training_set
        self._lookahead_reduce = lookahead_reduce
        self._input_training_filter = input_training_filter
        self._target_training_filter = target_training_filter

        assert hasattr(estimator, predict_fun_name)
        self._predict_fun_name = predict_fun_name
        self._input_fun = input_fun
        super(Predictor, self).__init__(
            estimator=estimator,
            steps=steps,
            rolling=rolling,
            continuous=continuous,
            multi_episode=multi_episode,
            name=name,
            verbose=verbose)

    def _calibrate(self, state):
        super(Predictor, self)._calibrate(state)
        if (self._calibration_steps_count >= self._maxlen):
            if ((self.rolling and (self._calibration_steps_count >= self.steps - 1)) or
                    ((not self.rolling) and (self._calibration_steps_count == self.steps - 1) and
                        (not self.continuous))):
                X = self._get_input()
                y = self._get_target()
                X, y = self._filter_training_set(X, y)
                if self._save_training_set:
                    print("Saving X and y...")
                    np.savetxt("X.csv", X, delimiter=",")
                    np.savetxt("y.csv", y, delimiter=",")
                print("Training predictor...")
                self.estimator.fit(X, y)
            elif (not self.rolling) and self.continuous:
                X = self._get_input()
                y = self._get_target()
                print("Training predictor...")
                self.estimator.partial_fit(X, y)

    def _filter_training_set(self, X, y):
        selector = [self._input_training_filter(X_i) and
                    self._target_training_filter(y_i) for X_i, y_i in zip(X, y)]
        return X[selector], y[selector]

    def _transform(self, state):
        if self._maxlen == 1:
            X = state[np.newaxis, np.newaxis, :]
        else:
            historical_states = list(self.calibration_state)[len(
                self.calibration_state) - self._maxlen + 1:]
            X = np.concatenate([historical_states, [state]])[np.newaxis, :]
        if not self._time_distributed:
            X = X.reshape(X.shape[0], -1)
        if not self.continuous:
            self.calibration_state.append(state)
        prediction = np.squeeze(
            getattr(self.estimator, self._predict_fun_name)(X))
        return prediction.reshape(len(self._lookahead_vector), -1)

    def _get_input(self):
        X = self._input_fun(np.array(self.calibration_state))
        X_lagged = lagged_features(
            X=X[:len(X) - self._max_lookahead],
            maxlen=self._maxlen,
            time_distributed=self._time_distributed
        )
        return X_lagged

    def _get_target(self):
        y = self._target_fun(np.array(self.calibration_state))
        y_lookahead = lookahead_targets(
            y[self._maxlen - 1:], lookahead_vector=self._lookahead_vector, lookahead_reduce=self._lookahead_reduce)
        return y_lookahead
