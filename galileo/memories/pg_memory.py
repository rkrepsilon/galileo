import random

import numpy as np

from ..core import Memory


class PGMemory(Memory):

    def __init__(self, size):
        super(PGMemory, self).__init__(
            size, extra_keys=["advantage", "critic_target"])

    def sample(self, maxlen, horizon=0):
        indices = self.get_valid_indices(maxlen, horizon)
        return self.get_transitions(indices, maxlen=maxlen)
