import numpy as np

from ..core import Memory


class EpisodicMemory(Memory):
    """Episodic structure transitions memory
    """

    def __init__(self, size, per=False, alpha=1, beta=1, min_samp_prob=0.01):

        self.per = per
        extra_keys = []
        if self.per:
            extra_keys = ['td_error']
            self.alpha = alpha
            self.beta = beta
            self.min_samp_prob = min_samp_prob

        super(EpisodicMemory, self).__init__(size, extra_keys=extra_keys)

    def sample(self, size, maxlen=1):

        if self.get_current_size() < size + maxlen:
            raise (ValueError(
                "size + maxlen ({}) cannot be larger than the number of element currently stored in the memory ({})".
                format(size + maxlen, self.get_current_size())))
        p = None
        valid_indices = self.get_valid_indices(maxlen=maxlen)
        if self.per:
            td_abs_errors = np.abs(self.dict['td_error'][valid_indices]
                                   ).astype(float)
            p = (td_abs_errors**self.alpha + self.min_samp_prob)
            p = p / np.sum(td_abs_errors**self.alpha + self.min_samp_prob)
            p = p.astype(float)
        sampled_indices_positions = np.random.choice(
            range(len(valid_indices)), size, replace=False, p=p)
        sampled_indices = valid_indices[sampled_indices_positions]
        weights = None
        if self.per:
            weights = (size * p[sampled_indices_positions])**(-self.beta)
            weights = weights / max(weights)
        transitions_sequences = self.get_transitions(sampled_indices, maxlen)
        return transitions_sequences, weights, sampled_indices
