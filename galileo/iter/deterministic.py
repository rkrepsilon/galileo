import numpy as np
import six

from ..core import DataIterator
from ..iter.transitions import ladder_transition, price_from_transition


class WavySignal(DataIterator):
    """Modulated sine generator

    Args:
        period_1 (float): Period of first sinusoidal
        period_2 (float): Period of second sinusoidal
        epsilon (float): Relative amplitude
        ba_spread (float): Spread
        length (float): Length of the iterator
        gamma_k (float): Gamma distribution k
        gamma_theta (float): Gamma distribution theta
        add_timer (bool): Will add a timer.
    """
    multi_episode = False
    is_finite = False

    def __init__(self,
                 period_1,
                 period_2,
                 epsilon,
                 length,
                 regular_sampling=True,
                 gamma_k=1.0,
                 gamma_theta=1.0,
                 add_timer=False,
                 ba_spread=0):
        super(self.__class__, self).__init__(length, add_timer)
        self._period_1 = period_1
        self._period_2 = period_2
        self._epsilon = epsilon
        self._ba_spread = ba_spread
        self.regular_sampling = regular_sampling
        self.n_products = 1
        self.price_cols_indices = [0, 1]
        if add_timer:
            self.timer_col_index = -1
        if regular_sampling:
            self._next_time_sampler = lambda t: t + 1
        else:
            self._next_time_sampler = \
                lambda t: t + np.random.gamma(gamma_k, gamma_theta)
        self._timestamp = 0

    def generator(self):
        self._timestamp = 0
        random_phase = np.random.randint(max([self._period_1, self._period_2]))
        while self._timestamp < self.length:
            bid_price =  \
                (1 - self._epsilon) \
                * np.sin(2 * (self._timestamp + random_phase) * np.pi
                         / self._period_1) \
                + self._epsilon \
                * np.sin(2 * (self._timestamp + random_phase) * np.pi
                         / self._period_2)
            self._timestamp = self._next_time_sampler(self._timestamp)
            if self.has_timer:
                yield bid_price, bid_price + self._ba_spread, self.length - self._timestamp
            else:
                yield bid_price, bid_price + self._ba_spread


class TriangularWalk(DataIterator):
    """Ladder walk in a tunnel

    Args:
        nsteps (int): Number of points on the ladder
        min_val (float): Minimum value
        max_val (float): Maximum value
        ba_spread (float): Spread
    """
    multi_episode = False
    is_finite = False

    def __init__(self,
                 nsteps,
                 length,
                 add_timer=False,
                 min_val=-1,
                 max_val=1,
                 ba_spread=0):
        super(self.__class__, self).__init__(length, add_timer)
        self._nsteps = nsteps
        self._min_val = min_val
        self._max_val = max_val
        self._ba_spread = ba_spread
        self.n_products = 1
        self._price_init = [0, 1]
        self.price_cols_indices = [0, 1]
        if add_timer:
            self.timer_col_index = -1

    def generator(self):
        transition = ladder_transition(self._nsteps)
        bid_price_iterator = price_from_transition(
            transition=transition, price_init=self._price_init)
        for i in reversed(range(self.length)):
            bid_price = six.advance_iterator(bid_price_iterator)
            self._price_init = [self._price_init[1], bid_price]
            bid_price = bid_price / float(self._nsteps - 1) * (
                self._max_val - self._min_val) + self._min_val
            if self.has_timer:
                yield bid_price, bid_price + self._ba_spread, i
            else:
                yield bid_price, bid_price + self._ba_spread
