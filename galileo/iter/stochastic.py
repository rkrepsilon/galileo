import numpy as np
import six

from ..core import DataIterator
from ..iter.transitions import price_from_transition, tunnel_transition


class RandomWalk(DataIterator):
    """Generator for a pure random walk

    Args:
        ba_spread (float): spread between bid/ask
    """
    multi_episode = False
    is_finite = False

    def __init__(self, length, add_timer=False, ba_spread=0):
        super(self.__class__, self).__init__(length, add_timer)
        self._ba_spread = ba_spread
        self.n_products = 1
        self.price_cols_indices = [0, 1]
        if add_timer:
            self.timer_col_index = -1

    def generator(self):
        bid_price = 0
        for i in reversed(range(self.length)):
            if self.has_timer:
                yield bid_price, bid_price + self._ba_spread, i
            else:
                yield bid_price, bid_price + self._ba_spread
            bid_price += np.random.standard_normal()


class AR1(DataIterator):
    """Generator for standardised AR1

    Args:
        a (float): AR1 coefficient
        ba_spread (float): spread between bid/ask
    """
    multi_episode = False
    is_finite = False

    def __init__(self, a, length, add_timer=False, ba_spread=0):
        super(self.__class__, self).__init__(length, add_timer)
        self._ba_spread = ba_spread
        assert abs(a) < 1
        self._a = a
        self._sigma = np.sqrt(1 - a**2)
        self.n_products = 1
        self.price_cols_indices = [0, 1]
        if add_timer:
            self.timer_col_index = -1

    def generator(self):
        bid_price = np.random.normal(scale=self._sigma)
        for i in reversed(range(self.length)):
            if self.has_timer:
                yield bid_price, bid_price + self._ba_spread, i
            else:
                yield bid_price, bid_price + self._ba_spread
            # bid_price += np.random.standard_normal()
            bid_price += (self._a - 1)\
                * bid_price\
                + np.random.normal(scale=self._sigma)


class BoundedRandomWalk(DataIterator):
    """Bounded random walk with the probabilisty transition function designed as a powerlaw to mimick the OU model
    Possible functions are restricted to the list below they all have a hard boundary at edges such that we satisfy the test

    Args:
        nsteps (int): Matrix size
        function (str): probability transition function (with respect to distance from origin).
           Can be any of:
               - None (no params) (bounded random walk)
               - 'trig' (no param)
               - 'polynomial' (no param)
               - 'powerlaw' (parameter exponent)
               - 'ornstein' (parameter sigmu_ratio)
        kwargs (dict): parameters of the chosen function
    """
    multi_episode = False
    is_finite = False

    def __init__(self,
                 nsteps,
                 length,
                 min_val=-1,
                 max_val=1,
                 function=None,
                 add_timer=False,
                 ba_spread=0,
                 **kwargs):
        super(self.__class__, self).__init__(length, add_timer)
        self._nsteps = nsteps
        self._min_val = min_val
        self._max_val = max_val
        self._function = function
        self._ba_spread = ba_spread
        self._kwargs = kwargs
        self.n_products = 1
        self.price_cols_indices = [0, 1]
        if add_timer:
            self.timer_col_index = -1

    def generator(self):
        transition = tunnel_transition(
            self._nsteps, self._function, **self._kwargs)
        bid_price_iterator = price_from_transition(
            transition=transition, price_init=[self._nsteps / 2])
        for i in reversed(range(self.length)):
            bid_price = six.advance_iterator(bid_price_iterator)
            bid_price = bid_price / float(self._nsteps - 1) * (
                self._max_val - self._min_val) + self._min_val
            if self.has_timer:
                yield bid_price, bid_price + self._ba_spread, i
            else:
                yield bid_price, bid_price + self._ba_spread
