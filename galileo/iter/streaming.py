import csv
import warnings

import numpy as np
import six

import pandas as pd
from dataserver.client import Client

from ..core import DataBreak, DataIterator


class CSVStreamer(DataIterator):
    """Data iterator from csv file.
    If the data has many episodes, a timer column can be added

    Args:
        filename (str): Filepath to a csv file.
        price_cols (list): Bid and Ask price columns names
        timer_col (str): counter column name
        header (int or None): Header row number
        index_col (str): index column name
    """
    multi_episode = True
    is_finite = True

    def __init__(self, filename, price_cols, timer_col=None, header=0, index_col=None):
        super(self.__class__, self).__init__(length=None, add_timer=True)
        self._filename = filename
        self._header = header
        self._index_col = index_col
        if not len(price_cols) % 2 == 0:
            raise ValueError("Expecting bid and ask prices for each products.")
        self.n_products = len(price_cols) / 2
        self._has_timer = timer_col is not None
        self.has_index = True
        self._cols = list(pd.read_csv(filename, header=header,
                                      index_col=index_col, nrows=1).columns)

        self.price_cols_indices = map(
            lambda x: self._cols.index(x), price_cols)
        if timer_col is None:
            self._nrows = self._get_nrows(
                filename=filename, has_header=(header is not None))
            self.timer_col_index = -1
        else:
            self._nrows = None
            self.timer_col_index = self._cols.index(timer_col)

    def generator(self):
        chunks = pd.read_csv(
            self._filename, header=self._header, index_col=self._index_col, iterator=True, chunksize=1e6)
        i = 0
        for chunk in chunks:
            for data in chunk.itertuples():
                self.index = data[0]
                data = np.array(data[1:]).astype(float)
                if not self._has_timer:
                    data = np.concatenate([data, [self._nrows - i - 1.]])
                if data[self.timer_col_index] == 0:
                    self._data_break = True
                i += 1
                yield data

    @staticmethod
    def _get_nrows(filename, has_header):
        return sum(1 for line in open(filename, 'r')) - int(has_header)
