"""Transition module used to generate data.
"""
import numpy as np


def is_transition(transition):
    """Check that all the transition probabilities sum to 0 (no transition) or 1
    """
    return np.all(
        np.isclose(np.sum(transition, axis=0).ravel(), 0) | np.isclose(
            np.sum(transition, axis=0).ravel(), 1))


def initialize_transition(price_increments, max_lag):
    """Initialise an empty transition matrix.
    """
    return np.zeros((price_increments, ) * (max_lag + 1))


def ladder_transition(height):
    """Transition matrix for the ladder data.
    """
    transition = initialize_transition(height, 2)
    for i in range(height - 1):
        n = i
        c = i + 1
        p = i + 2 if i + 2 < height else i
        transition[n, c, p] = 1
        transition[height - 1 - n, height - 1 - c, height - 1 - p] = 1
    return transition


def probability_transition_function(i, height, function, **kwargs):
    """ returns matrix elements of the transition as a OU law
    """
    size = (height - 1) / 2.
    position = (i) / size - 1
    if function == 'trig':
        return 0.5 * (np.sin(position * np.pi / 2))
    elif function == 'polynomial':
        return (-.25 * (position)**3 + .75 * position)
    elif function == 'ornstein':
        # sigmu_ratio  : is the the sigma/sqrt(2 mu) ratio which gives the lengthscale of the excursion
        # Here we need this ratio to be smaller than the height of the network while large enough to see some RW
        # We express it in terms of height and a reasonable value seems to be one half.
        # If values are larger we risk having the hard conditions playing a strong role.
        # If much smaller and height small, then we will have no RW
        try:
            return 0.5 * (np.tanh(position / kwargs['sigmu_ratio']))
        except:
            print(
                'Need to specify an argument for the ornstein function as [\'ornstein\',sigmu_ratio ]'
            )
    elif function == 'powerlaw':
        try:
            return np.sign(position) * np.abs(position)**kwargs[
                'exponent'] / 2.
        except:
            print(
                'Need to specify an argument for the power law function as [\'powerlaw\',exponent ]'
            )
    else:
        return 0


def tunnel_transition(height, function=None, **kwargs):
    """ Transition matrix for a random walk with reflective boundaries.
        for arguments see /galileo/data/generators BoundedRandomWalk
        Returns:
            numpy.array: transition matrix
    """
    transition = initialize_transition(height, 1)
    for i in range(1, height - 1):
        transition[i - 1, i] = .5 + probability_transition_function(
            i, height, function, **kwargs)
        transition[i + 1, i] = .5 - probability_transition_function(
            i, height, function, **kwargs)
    transition[1, 0] = 1
    transition[height - 2, height - 1] = 1
    return transition


def price_from_transition(transition, price_init):
    """Generator sampling data from a transition matrix.
    """
    assert is_transition(transition)
    max_lag = len(transition.shape) - 1
    assert len(price_init) == max_lag
    # Only integer indexes are allowed
    price_init = [int(x) for x in price_init]
    assert max(transition[tuple(price_init)]) != 0

    prices = price_init

    while True:
        prices_history = tuple(prices[-i] for i in range(1, max_lag + 1))
        prices.append(
            list(
                np.random.multinomial(1, transition[(slice(
                    0, len(transition)), ) + prices_history])).index(1))
        yield prices[-1]


if __name__ == '__main__':

    print("  Testing OU transition ")

    tests = [{
        'function': None,
        'params': {}
    }, {
        'function': 'trig',
        'params': {}
    }, {
        'function': 'polynomial',
        'params': {}
    }, {
        'function': 'powerlaw',
        'params': {
            'exponent': 0.1,
        }
    }, {
        'function': 'ornstein',
        'params': {
            'sigmu_ratio': 0.5,
        }
    }]
    for test in tests:
        print(test)
        transition = tunnel_transition(
            height=10, function=test['function'], **test['params'])
        print("Transition matrix reads\n", transition)
        assert is_transition(transition)
