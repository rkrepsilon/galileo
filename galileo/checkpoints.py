import json
import os
from copy import deepcopy


class Checkpoint(object):
    def __init__(self, experiment_path, should_append):
        self.experiment_path = os.path.join(experiment_path)
        self.should_append = should_append
        if self._should_append_training_report():
            with open(self._get_report_filename(), "r") as f:
                self.old_report = json.load(f)

    def save_brain(self, brain):
        brain_filename = os.path.join(self.experiment_path, 'brain.tar.gz')
        brain.save(brain_filename)

    def save_preprocessor(self, preprocessor):
        preprocessor_filename = os.path.join(
            self.experiment_path, 'preprocessor')
        preprocessor.save(preprocessor_filename)

    def _should_append_training_report(self):
        return (os.path.isfile(self._get_report_filename())) and self.should_append

    def _get_report_filename(self, report_name='train_report.json'):
        return os.path.join(self.experiment_path, report_name)

    def save_train_report(self, report):
        report_filename = self._get_report_filename()
        if self._should_append_training_report():
            old_report = deepcopy(self.old_report)
            for key in report.keys():
                assert key in old_report.keys()
                old_report[key].extend(report[key])
            report = old_report

        with open(report_filename, 'w+') as f:
            json.dump(report, f, indent=4, sort_keys=True)

    def save_test_report(self, report):
        report_filename = self._get_report_filename(
            report_name='test_report.json')
        with open(report_filename, 'w+') as f:
            json.dump(report, f, indent=4, sort_keys=True)

    def save_calibration_checkpoint(self, preprocessor):
        self.save_preprocessor(preprocessor)

    def save_training_checkpoint(self, train_report, brain):
        self.save_train_report(train_report)
        self.save_brain(brain)

    def save_testing_checkpoint(self, test_report):
        self.save_test_report(test_report)


class TmpCheckpoint(Checkpoint):
    def __init__(self):
        super(TmpCheckpoint, self).__init__(
            experiment_path='/tmp/',
            should_append=False)
