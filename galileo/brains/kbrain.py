import json
import os
import tarfile
from functools import reduce

import six

import keras.backend as K
from keras.layers import *
from keras.models import Model, Sequential, load_model
from keras.optimizers import Adam

from ..core import Brain
from ..utils import get_class_name, get_params_dict


class KerasBrain(Brain):
    """Abstract class for a brain using the Keras framework.

    Args:
        output_size (int): Output size of brain.
        input_shape (tuple): Input shape to the brain (state shape)
        loading (bool): Prepare to load a brain (doesn't compile the models)
        verbose (bool): Will print a summary of the brain when compliled
        feature_block (keras feature block): keras feature block
        optimizer (keras.optimizer): Optimizer
        time_distributed (bool): Time distributed means the input shape to the network keeps a time dimension (for recurrent network for instance)
    """

    def __init__(self,
                 output_size,
                 input_shape=None,
                 loading=False,
                 verbose=True,
                 feature_block=None,
                 optimizer=Adam(),
                 time_distributed=False,
                 **kwargs):
        params = get_params_dict(locals())
        if (not loading) & (feature_block is None):
            ValueError(
                "feature_block must be specified unless loading a previously saved brain.")
        # feature_block and optimizer cannot be serialized.
        del params['feature_block']
        params['optimizer'] = {
            "class_name": get_class_name(optimizer),
            "config": optimizer.get_config()
        }
        if (not loading) and (feature_block is None):
            raise ValueError(
                "If loading is False, feature_block must be specified.")
        self._time_distributed = time_distributed
        self._feature_block = feature_block
        self._optimizer = optimizer
        self.custom_objects = {}
        super(KerasBrain, self).__init__(**params)

    def _create_features(self):
        inputs = Input(shape=self.input_shape, name='input')
        features = reduce(lambda x, l: l(x), [inputs] + self._feature_block)
        if not self._time_distributed:
            features = Flatten()(features)
        return inputs, features

    def _build(self):
        inputs, features = self._create_features()
        self._build_models(inputs, features)
        self.built = True

    def _compile(self):
        self._compile_models()
        self.compiled = True

    def _build_models(self, inputs, features):
        raise NotImplementedError()

    def _compile_models(self):
        raise NotImplementedError()

    @staticmethod
    def _save_model_object(model, filepath, inference):
        # optimizer saved independently since we don't always compile the brain
        model.save(filepath, include_optimizer=False)
        if inference and K.backend() == 'tensorflow':
            KerasBrain._dump_model_as_pb(model, os.path.dirname(filepath))

    @staticmethod
    def _dump_model_as_pb(model, directory):
        from tensorflow.python.framework import graph_util
        from tensorflow.python.framework import graph_io
        import tensorflow as tf
        output_names = model.output_names
        input_names = model.input_names
        assert len(output_names) == 1
        assert len(input_names) == 1
        output_name = output_names[0]
        input_name = input_names[0]
        sess = K.get_session()
        tf.train.write_graph(sess.graph.as_graph_def(),
                             directory, 'network.ascii', as_text=True)
        all_names = [
            n.name for n in tf.get_default_graph().as_graph_def().node]
        tf_output_names = list(filter(
            lambda x: x.startswith(output_name), all_names))
        tf_input_names = list(
            filter(lambda x: x.startswith(input_name), all_names))
        tf_output_name = tf_output_names[0]
        tf_input_name = tf_input_names[0]
        constant_graph = graph_util.convert_variables_to_constants(
            sess, sess.graph.as_graph_def(), [tf_output_name])
        graph_io.write_graph(constant_graph, directory,
                             'network.pb', as_text=False)
        with open(os.path.join(directory, 'network.io'), 'w+') as file_:
            io_dict = dict(
                input_name={'keras': input_name, 'tf': tf_input_name},
                output_name={'keras': output_name, 'tf': tf_output_name}
            )
            json.dump(io_dict, file_, indent=4)

    @staticmethod
    def _load_model_object(filepath, custom_objects={}):
        return load_model(filepath, custom_objects=custom_objects)

    def _predict(self, state, network):
        return self.models[network]['object'].predict(state)

    def summary(self):
        if self.models == {}:
            print("Brain not yet compiled.")
            return
        for name, model in six.iteritems(self.models):
            print(
                "_________________________________________________________________"
            )
            print('Model', name)
            model['object'].summary()
