import json
import os
import shutil

import tensorflow as tf
from keras import backend as K

from . import *
from ..utils import build_object


def set_keras_backend(backend):
    if K.backend() != backend:
        os.environ['KERAS_BACKEND'] = backend
        reload(K)
        assert K.backend() == backend


def save_protobuff_file(filepath):
    set_keras_backend("tensorflow")
    brain = load_brain(filepath)
    brain.save(filepath)


def load_protobuff_graph(protobuff_file):
    graph = tf.Graph()
    graph_def = tf.GraphDef()
    with open(protobuff_file, "rb") as f:
        graph_def.ParseFromString(f.read())
    with graph.as_default():
        tf.import_graph_def(graph_def)
    return graph


def check_protobuff_dump(filepath):
    brain = load_brain(filepath, delete_folder=False)
    brain_path = os.path.join(os.path.dirname(filepath), 'brain')
    graph = load_protobuff_graph(os.path.join(brain_path, 'network.pb'))
    with open(os.path.join(brain_path, 'network.io'), 'r') as file_:
        io_dict = json.load(file_)
    input_operation = graph.get_operation_by_name(
        "import/" + io_dict['input_name']['tf'])
    output_operation = graph.get_operation_by_name(
        "import/" + io_dict['output_name']['tf'])
    x_test = np.random.randn(1, *brain.input_shape)
    for model_dict in brain.models.values():
        if model_dict['inference']:
            model = model_dict['object']
    keras_result = model.predict(x_test)
    with tf.Session(graph=graph) as sess:
        tf_result = sess.run(output_operation.outputs[0],
                             {input_operation.outputs[0]: x_test})
    shutil.rmtree(brain_path)
    assert np.all(keras_result == tf_result), "Results not matching: keras_result {}, tf_result {}".format(
        keras_result, tf_result)
