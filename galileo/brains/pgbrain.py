from copy import deepcopy

import numpy as np

import keras.backend as K
from keras.layers import Activation, Dense, Flatten, Input, Lambda
from keras.models import Model, Sequential
from keras.optimizers import Adam

from ..brains import KerasBrain
from ..core import Agent
from ..utils import get_params_dict


class PGBrain(KerasBrain):
    """Brain of the Policy Gradient agent
    Inspired from https://arxiv.org/abs/1506.02438
    The actor and the critic both share the same hidden layer,
    only the output differs.
    For generality we always build a critic, it's use and
    training are decided from the brain

    Args:
        output_size (int): Output size of brain.
        input_shape (tuple): Input shape to the brain (state shape)
        loading (bool): Prepare to load a brain (doesn't compile the models)
        verbose (bool): Will print a summary of the brain when compliled
        feature_block (keras feature block): keras feature block
        optimizer (keras.optimizer): Optimizer
        time_distributed (bool): Time distributed means the input shape to the network keeps a time dimension (for recurrent network for instance)
        epsilon (float) : entropy weight in the loss function
    """

    def __init__(self,
                 output_size,
                 input_shape=None,
                 loading=False,
                 verbose=True,
                 feature_block=None,
                 optimizer=Adam(),
                 time_distributed=False):

        params = get_params_dict(locals())
        K.set_learning_phase(0)
        super(self.__class__, self).__init__(**params)

    def _build_models(self, inputs, features):
        """ Building template fot the actor/critic brain
        The same hidden layer is used for both
        """
        actor_output = Dense(self.output_size, activation='softmax',
                             name='policy')(features)
        critic_output = Dense(1, activation='linear')(features)
        actor = Model(inputs=inputs, outputs=actor_output)
        critic = Model(inputs=inputs, outputs=critic_output)
        actor_critic = Model(inputs=inputs, outputs=[
                             actor_output, critic_output])
        self.models['actor'] = {
            'object': actor,
            'trainable': False,
            'inference': True
        }
        self.models['critic'] = {
            'object': critic,
            'trainable': False,
            'inference': False
        }
        self.models['actor_critic'] = {
            'object': actor_critic,
            'trainable': True,
            'inference': False
        }

    def _compile_models(self):
        # The following is required since we don't (really) compile the models.
        self.models['actor_critic']['object'].loss = "loss"

        # Actor loss function
        model_output = self.models['actor_critic']['object'].output
        action = K.placeholder(shape=(None, self.output_size), name="action")
        action_proba = model_output[0]
        advantage = K.placeholder(shape=(None,), name="advantage")
        epsilon = K.placeholder(shape=(None,), name="epsilon")
        # the sum of the product below is equivalent to action_proba[:,np.where(action)]
        actor_loss = - K.log(K.sum(action_proba * action, axis=1)) * advantage
        entropy = - K.mean(action_proba * K.log(action_proba + 1e-10), axis=1)
        actor_loss_and_entropy = K.mean(actor_loss - epsilon * entropy)

        # Critic loss function
        critic_target = K.placeholder(shape=(None,), name="critic_target")
        critic = K.squeeze(model_output[1], axis=1)
        critic_loss = K.mean(K.square(critic - critic_target))

        loss = actor_loss_and_entropy + critic_loss
        model_updates = self._optimizer.get_updates(
            params=self.models['actor_critic']['object'].trainable_weights,
            loss=loss)
        self._train_model = K.function(
            inputs=[self.models['actor_critic']['object'].input,
                    action,
                    advantage,
                    critic_target,
                    epsilon],
            outputs=[actor_loss_and_entropy, critic_loss],
            updates=model_updates)

    def _train(self, state, advantage, action, critic_target, epsilon):
        K.set_learning_phase(1)
        loss = self._train_model([state,
                                  action,
                                  advantage,
                                  critic_target,
                                  epsilon * np.ones_like(advantage)])[0]
        K.set_learning_phase(0)
        return {"actor_critic_loss": float(loss)}

    def get_weights(self, model='actor_critic'):
        """ Get none zero weights of the model in a 1D numpy array
        Args:
            model (str): actor or critic model

        Returns:
            (numpy.array): array of weights for each layer
        """
        weights = []
        for layer in self.models[model]['object'].layers:
            raw_weights = layer.get_weights()
            for k in range(len(raw_weights)):
                weights.append(raw_weights[k])
        return np.array(weights)

    def set_weights(self, weights, model='actor_critic'):
        """ Set none zero weights of the model in a 1D numpy array
        Args:
            model (str): actor or critic model
            weights (numpy.array): weights to update

        Returns:
            (numpy.array): array of weights for each layer
        """
        position = 0
        for layer in self.models[model]['object'].layers:
            size = len(layer.get_weights())
            if size > 0:
                layer.set_weights(weights[position:position + size])
            position += size
