import json
import tarfile
from copy import deepcopy

import numpy as np

import keras.backend as K
from keras.layers import *
from keras.layers.merge import add
from keras.models import Model, clone_model
from keras.optimizers import Adam

from ..brains import KerasBrain
from ..utils import get_params_dict


class QBrain(KerasBrain):
    """Brain for a DQNAgent.

    Args:
        output_size (int): Output size of brain.
        input_shape (tuple): Input shape to the brain (state shape)
        loading (bool): Prepare to load a brain (doesn't compile the models)
        verbose (bool): Will print a summary of the brain when compliled
        feature_block (keras feature block): keras feature block
        optimizer (keras.optimizer): Optimizer
        time_distributed (bool): Time distributed means the input shape to the network keeps a time dimension (for recurrent network for instance)
        has_target_model (bool): Will use a target model.
        is_dueling (bool): Will use the dueling architecture.
        loss (keras.loss): Specify the loss to use.
    """

    def __init__(self,
                 output_size,
                 input_shape=None,
                 loading=False,
                 verbose=True,
                 feature_block=None,
                 optimizer=Adam(),
                 time_distributed=False,
                 has_target_model=True,
                 is_dueling=True,
                 loss='mse'):
        params = get_params_dict(locals())
        self._has_target_model = has_target_model
        self._is_dueling = is_dueling
        self._loss = loss
        super(QBrain, self).__init__(**params)

    def _build_outputs(self, inputs, features):
        if self._is_dueling:
            advantage = Dense(self.output_size, activation='linear')(features)
            advantage = Lambda(
                lambda a: a - K.mean(a, keepdims=True),
                output_shape=(self.output_size, ))(advantage)
            value = Dense(1, activation='linear')(features)
            value = Lambda(
                lambda s: K.expand_dims(s[:, 0], axis=-1),
                output_shape=(self.output_size, ))(value)
            return add([value, advantage], name='value')
        else:
            return Dense(self.output_size, activation='linear', name='value')(features)

    def _build_models(self, inputs, features):
        outputs = self._build_outputs(inputs, features)
        self.models['value'] = {
            'object': Model(inputs=inputs, outputs=outputs),
            'trainable': True,
            'inference': True
        }
        if self._has_target_model:
            self.models['target'] = {
                'object': clone_model(self.models['value']['object']),
                'trainable': False,
                'inference': False
            }
            self.update_target_model()
        else:
            self.models['target'] = self.models['value']
            self.models['target']['trainable'] = False
            self.models['target']['inference'] = False

    def _compile_models(self):
        self.models['value']['object'].compile(
            loss=self._loss,
            optimizer=self._optimizer)

    def update_target_model(self, smoothness=0):
        value_weights = self.models['value']['object'].get_weights()
        target_weights = self.models['target']['object'].get_weights()
        new_weights = list(map(lambda x: np.zeros(x.shape), target_weights))
        for i in range(len(new_weights)):
            new_weights[i] = (1 - smoothness) * value_weights[i] +\
                smoothness * target_weights[i]
        self.models['target']['object'].set_weights(new_weights)

    def _train(self, state, target, w=None):
        value_loss = self.models['value']['object'].train_on_batch(
            state, target, sample_weight=w)
        return {"value_" + self.models['value']['object'].loss: float(value_loss)}
