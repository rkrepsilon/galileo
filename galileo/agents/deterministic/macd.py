"""MACDAgent Agent
Here we code a simple MACD agent. We have one short term ema and one long term ema, and takes actions when one crosses the
other.
"""
import numpy as np
import six

from ...core import Agent
from ...envs import SpreadTrading
from ...preprocessing import Lambda, MACDSignal, Selector, compose, spread


class MACDAgent(Agent):
    """This class implements the MACD agent. There are 2 emas being recorded, the short and the long one
    We use window size (integer) to calculate ema coefficient, and then calculate the price (mid) moving average.
    Variation of http://traderhq.com/ultimate-guide-to-the-macd-indicator/

    Args:
        spread_coefficients (list): spread coefficients
        window_short (int): the short EMA window size
        window_long (int): the long EMA window size
        window_signal (int): the macd EMA window size
        threshold (float): the distance between the 'crossing point' and 0
    """
    learner = False

    _actions = SpreadTrading._actions

    def __init__(self,
                 spread_coefficients,
                 window_short=12,
                 window_long=26,
                 window_signal=9,
                 threshold=0):
        """Initialisation function.
        """
        price_selection = np.arange(len(spread_coefficients) * 2)
        preprocessor = compose(
            [
                Selector(price_selection),
                spread(spread_coefficients),  # Spread
                Lambda(fun=lambda x: np.mean(x)),  # Midprice
                MACDSignal(window_short=window_short,
                           window_long=window_long,
                           window_signal=window_signal)
            ]
        )
        super(self.__class__, self).__init__(preprocessor,
                                             default_action=np.array([1, 0, 0]))

        self._threshold = threshold
        # Signals
        self._macd = None
        self._signal = None
        self._sign = None

    def _act(self, state):
        """Return the action to perform depending on the state of the market

            Args:
                state (numpy.array): State of the agent (only work for 1 product)

            Returns
                (numpy.array): action to take
        """
        self._macd, self._signal = state
        crossing = self._detect_crossing()
        action = self._decide_action(crossing)
        return action

    def _decide_action(self, crossing):
        """Decide buy / sell
        If macd is above signal and crossing is above threshold, then sell
        If macd is below signal and crossing is below -threshold, then buy

        Args:
            crossing (int): Type of crossing +/-1, or 0 if no crossing
        Returns
            (numpy.array): action to take
        """
        if (self._macd > self._threshold) & (crossing == -1):
            return self._actions['sell']
        elif (self._macd < -self._threshold) & (crossing == 1):
            return self._actions['buy']
        else:
            return self._actions['hold']

    def _detect_crossing(self):
        """
        Returns:
            (float): 1 if macd crosses up, -1 if down, 0 else.
        """
        previous_sign = self._sign
        self._sign = np.sign(self._macd - self._signal)
        if previous_sign != self._sign:
            return self._sign
        else:
            return 0


if __name__ == '__main__':

    from ...iter import AR1

    n_products = 1
    spread_coefficients = [1]
    window_short, window_long, window_signal = 12, 26, 9
    threshold = 0.01

    dtt = SpreadTrading(
        data_iterator=AR1(a=0.98, length=1000),
        spread_coefficients=spread_coefficients)

    macd_agent = MACDAgent(spread_coefficients,
                           window_short,
                           window_long,
                           window_signal,
                           threshold)

    def get_action_name(action):
        for k, v in six.iteritems(macd_agent._actions):
            if (action == v).all():
                return k

    print("Testing MACD agent")
    state = dtt.reset()
    dtt.render()
    terminal = False
    while not terminal:
        action = macd_agent.act(state)
        print("Mid price is {:+.5f}".format(
            (state[0] + state[1]) / 2), " Action is", get_action_name(action))
        state, reward, terminal, _ = dtt.step(action)
        macd_agent.observe(state, reward, terminal)
        dtt.render()
