import csv

from ...core import Agent


class Saving(Agent):

    def __init__(self, action_fun, filename):
        super(Saving, self).__init__()
        self._action_fun = action_fun
        with open(filename, 'w+') as file:
            self._writer = csv.writer(file)

    def _act(self, state):
        return self._action_fun()

    def observe(self, state, reward, terminal):
        self._writer.writerow(list(state))
