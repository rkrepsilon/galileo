from ...core import Agent


class DummyAgent(Agent):

    def __init__(self, action_fun):
        super(DummyAgent, self).__init__()
        self._action_fun = action_fun

    def _act(self, state):
        return self._action_fun()

    def observe(self, state, reward, terminal):
        pass
