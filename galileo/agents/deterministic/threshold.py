"""Threshold Agent
Here we code a threshold agent that trades at given thresholds
The theoretical optimal entry prices as derived in Leug Li 2016 are correct only
on the very long term and need to be recomputed experimentally
"""
import random
import six

import numpy as np

from ...core import Agent
from ...envs import SpreadTrading
from ...utils import calc_spread


class ThresholdAgent(Agent):
    """ Dummy Threshold agent that longs or short (when currently flat) at a fixed open_threshold price and closes positions when the close_threshold price is striked.
    Only one case (buy:open, sell:close) needs to be considered since we symmetrise in the code.
    The code convention however is buy:open, sell:close.
    """
    learner = False

    _actions = SpreadTrading._actions

    def __init__(self,
                 spread_coefficients,
                 open_threshold=-1,
                 close_threshold=1):
        """Initialisation function.

        Args:
            spread_coefficients (list): Spread coefficients
            open_threshold (float): Threshold at which to open a position
            close_threshold (float): Threshold at which to close a position
        """
        super(ThresholdAgent, self).__init__()
        self._open_threshold = open_threshold
        self._close_threshold = close_threshold
        self._spread_coefficients = spread_coefficients

    def _act(self, state):
        """Return the action to perform depending on the state of the market

            Args:
                state (numpy.array): State of the agent (only work for 1 product)

            Returns
                (numpy.array): action to take
        """

        position = state[-1]
        price_vector = state[:-2]
        spread_bid, spread_ask = calc_spread(price_vector,
                                             self._spread_coefficients)
        if position == 0:  # Flat
            if spread_ask <= self._open_threshold:
                return self._actions['buy']
            elif spread_bid >= -self._open_threshold:
                return self._actions['sell']
            else:
                return self._actions['hold']
        elif position > 0:  # Long
            if spread_bid >= self._close_threshold:
                return self._actions['sell']
            else:
                return self._actions['hold']
        elif position < 0:  # Short
            if spread_ask <= -self._close_threshold:
                return self._actions['buy']
            else:
                return self._actions['hold']
        else:
            return self._actions['hold']


if __name__ == '__main__':

    from ..iter import AR1

    spread_coefficients = [1]
    dtt = SpreadTrading(
        data_iterator=AR1(a=0.9), spread_coefficients=spread_coefficients)

    open_threshold = -1
    close_threshold = -0.2
    agent_double = ThresholdAgent(
        spread_coefficients,
        open_threshold=open_threshold,
        close_threshold=close_threshold)

    def get_action_name(action):
        for k, v in six.iteritems(agent_double._actions):
            if (action == v).all():
                return k

    print("Testing Double threshold agent ")
    state = dtt.reset()
    dtt.render()
    for i in range(100):
        action = agent_double.act(state)
        action_name = get_action_name(action)
        print("Mid price is", (state[0] + state[1]) / 2, " Action is ", action_name)
        state, reward, terminal, _ = dtt.step(action)
        action = agent_double.observe(state, reward, terminal)

        dtt.render()
