import numpy as np

from ...core import Agent


class MultiHorizonReturnsForecast(Agent):
    """Voting agent for multi-horizon returns forecast prediction.

    Args:
        ratio_threshold (float): Threshold to buy. Must be 0.5 <= threshold <= 1.
        prediction_type (str): 'classification', 'probability' or 'regression'.
        number_of_classes (int): Number of classes in the prediction vector (2 or 3). Only for classification.
        returns_labels (dict): Returns labels, relevant if classification.
    """

    def __init__(self, ratio_threshold=0.9, prediction_type='probability', number_of_classes=2, returns_labels={'flat': 0, 'up': 1, 'down': 2}):
        super(MultiHorizonReturnsForecast, self).__init__()
        assert ratio_threshold >= 0.5
        self._ratio_threshold = ratio_threshold
        self._prediction_type = prediction_type
        self._returns_labels = returns_labels
        self._flat_label = returns_labels['flat']
        assert number_of_classes in (2, 3)
        self.number_of_classes = number_of_classes
        self._up_index = \
            list(np.sort(returns_labels.values())).index(
                returns_labels['up'])
        self._down_index = \
            list(np.sort(returns_labels.values())).index(
                returns_labels['down'])

    def _act(self, state):
        """Action function

        Args:
            state (np.array): array of forecasts followed by being the position
        Returns:
            np.array: Action
        """
        forecast = state[:-1]
        position = state[-1]
        if self._prediction_type == 'classification':
            if self.number_of_classes == 2:
                assert set(forecast) == {
                    returns_labels['up'], returns_labels['down']}
            elif self.number_of_classes == 3:
                assert set(forecast) == {
                    returns_labels['flat'], returns_labels['up'], returns_labels['down']}
            kwargs = {'up_label': self._up_label,
                      'down_label': self._down_label}
        elif self._prediction_type == 'probability':
            assert len(forecast) % self.number_of_classes == 0
            forecast = forecast.reshape(-1, self.number_of_classes)
            kwargs = {'up_index': self._up_index,
                      'down_index': self._down_index}
        else:
            kwargs = {}

        integer_action = getattr(self, self._prediction_type + '_vote')(
            forecast=forecast,
            threshold=self._ratio_threshold,
            **kwargs
        )

        if (integer_action == 1) and (position <= 0):
            return [0, 1, 0]
        if (integer_action == -1) and (position >= 0):
            return [0, 0, 1]
        else:
            return [1, 0, 0]

    @staticmethod
    def classification_vote(forecast, threshold=1, up_label=1, down_label=2):
        """Clasification vote

        Args:
            forecast (numpy.ndarray): 1D array containing up/down/flat(optional) labels.
        """
        ratio_up = np.sum(forecast == up_label) * 1. / len(forecast)
        ratio_down = np.sum(forecast == down_label) * 1. / len(forecast)
        if ratio_up >= threshold:
            return 1
        if ratio_down >= threshold:
            return -1
        return 0

    @staticmethod
    def probability_vote(forecast, threshold=0.8, up_index=1, down_index=2):
        """Probability vote

        Args:
            forecast (numpy.ndarray): 2D array containing predicted probabilities for up or down
        """
        ratio_up = np.sum(forecast[:, up_index], axis=0) * 1. / len(forecast)
        ratio_down = np.sum(forecast[:, down_index],
                            axis=0) * 1. / len(forecast)
        if ratio_up >= threshold:
            return 1
        if ratio_down >= threshold:
            return -1
        return 0

    @staticmethod
    def regression_vote(forecast, threshold=0.8, *args, **kwargs):
        raise NotImplementedError()
