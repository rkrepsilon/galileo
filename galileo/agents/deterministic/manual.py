from six.moves import input

from ...core import Agent


class Human(Agent):

    def __init__(self):
        super(Human, self).__init__()

    def _act(self, state):
        action = input("Action: Buy (b) / Sell (s) / Hold (enter): ")
        if action == 'b':
            action = [0, 1, 0]
        elif action == 's':
            action = [0, 0, 1]
        else:
            action = [1, 0, 0]
        return action

    def observe(self, state, reward, terminal):
        pass
