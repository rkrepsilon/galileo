from .threshold import *
from .macd import *
from .dummy import *
from .forecast import *
from .saving import *
from .manual import *
