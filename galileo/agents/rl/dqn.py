"""Keras DQN Agent
"""
import random

import numpy as np

from ...core import RLAgent


class DQNAgent(RLAgent):
    def __init__(self,
                 brain,
                 memory,
                 default_action,
                 maxlen=1,
                 reward_clip=[-np.inf, np.inf],
                 random_action_fun=None,
                 is_double=True,
                 gamma=0.9,
                 target_update_frequency=1,
                 update_smoothness=0.9,
                 epsilon=0.1,
                 batch_size=64,
                 n_skip_frames=0):
        super(DQNAgent, self).__init__(brain=brain,
                                       memory=memory,
                                       maxlen=maxlen,
                                       default_action=default_action,
                                       random_action_fun=random_action_fun,
                                       reward_clip=reward_clip,
                                       n_skip_frames=n_skip_frames)
        self.warmup = True
        self.gamma = gamma
        self.target_update_frequency = target_update_frequency
        assert (update_smoothness >= 0) & (update_smoothness < 1), \
            "update_smoothness should be between 0 (hard update) and 1 (exclusive)."
        self.update_smoothness = update_smoothness
        self._is_double = is_double
        self._epsilon = epsilon
        if random_action_fun is None:
            self.random_action_fun = lambda: np.random.multinomial(
                1, np.ones(self.brain.output_size) / float(self.brain.output_size))
        else:
            self.random_action_fun = random_action_fun
        self.batch_size = batch_size

    def _predict_action(self, recent_states):
        # Epsilon Greedy Approach: TODO: create a policy class
        if (np.random.uniform(0, 1) < self._epsilon):
            action = self.random_action_fun()
        else:
            q_values = self.brain.predict(recent_states, network='value')
            action_index = np.argmax(q_values)
            action = np.zeros(self.brain.output_size)
            action[action_index] = 1
        return action

    def _observe(self, state, reward, terminal):
        super(DQNAgent, self)._observe(state, reward, terminal)

        # Update TD error for past transition
        if self.memory.per:
            self.memory.add('td_error', None)
            if (self.episode_frames_seen >= self.maxlen):
                recent_transitions = self.memory.get_transitions([-1],
                                                                 self.maxlen)
                target, q_value = self._temporal_difference(recent_transitions)
                action_value = np.where(
                    self.memory.dict['action'][-1] == 1)[0][0]
                td_error = target[0] - q_value[0, action_value]
                self.memory.update(key='td_error', index=-1, val=td_error)

    def _learn(self):
        transitions, weights, indices = self.memory.sample(
            size=self.batch_size, maxlen=self.maxlen)
        q_target = self.brain.predict(transitions['state'], network='target')
        target, q_value = self._temporal_difference(transitions)
        action_index = np.where(transitions['action'] == 1)
        q_target[action_index[0], action_index[1]] = target
        if self.memory.per:
            td_error = target - q_value[action_index[0], action_index[1]]
            for i in range(self.batch_size):
                self.memory.update(
                    key='td_error', index=indices[i], val=td_error[i])
        loss = self.brain.train(transitions['state'], q_target, w=weights)
        if (self.total_frames_seen % self.target_update_frequency == 0):
            self.brain.update_target_model(self.update_smoothness)
        return loss, True

    def end(self):
        if self.total_frames_seen >= self.memory.size:
            self.warmup = False
        super(DQNAgent, self).end()

    def _temporal_difference(self, transitions):
        q_value = self.brain.predict(transitions['state'], network='value')
        q_target_next = self.brain.predict(
            transitions['next_state'], network='target')
        q_value_next = self.brain.predict(
            transitions['next_state'], network='value')
        target = transitions['reward'].astype(float)
        terminal = np.logical_not(transitions['terminal'])
        if self._is_double:
            target += terminal * self.gamma * \
                q_target_next[range(len(transitions['state'])),
                              np.argmax(q_value_next, axis=1)]
        else:
            target += terminal * self.gamma * np.max(q_value_next, axis=1)
        return target, q_value

    @property
    def epsilon(self):
        return self._epsilon

    @epsilon.setter
    def epsilon(self, value):
        self._epsilon = value
