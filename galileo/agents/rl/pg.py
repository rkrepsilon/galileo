"""
We implement a policy gradient algorithm
inspired from https://arxiv.org/abs/1506.02438
The Performance Objective action reads
L = \\nabla[\log \pi_\\theta(a|s)] Q^\pi(a,s)
where
    -\pi(a,s) is the policy we are trying to optimize
    - Q^{\pi} the action value funtion, which can be evalueated in different ways
In the naive case Q^{\pi} is evaluated as the discounted reward function.
If the advantage option is activated, differrent estimators can be chosen as a proxy for Q^\pi.
"""
import random
import warnings

import gym
import numpy as np

import keras.backend as K
from keras import utils as np_utils

from ...core import RLAgent
from ...envs import SpreadTrading
from ...utils import calc_discounted_rewards, get_losses_names_list


class PolicyGradientAgent(RLAgent):
    """ Policy Gradient Agent

    Args:
        gamma (float) : discount rate
        maxlen (int): Number of past transitions to return
        epsilon (float) : entropy weight in the loss function
        horizon_clip (int) : remove horizon_clip last steps of episodes as their discountd rewards are artificially low owing to episode ending.
        critic_type (string) : Specify the advantage estimator:
                          - None: discounted rewards (REINFORCE)
                          - 'advantage' to use the advantage function
                          - and 'gae' for the generalised advantage method
        **kwargs (dict): additional parameters for the advantage function (so far only gae_lambda can be added)
    """

    def __init__(self,
                 brain,
                 memory,
                 maxlen=1,
                 default_action=None,
                 random_action_fun=None,
                 reward_clip=[-np.inf, np.inf],
                 gamma=0.95,
                 epsilon=0,
                 horizon_clip=0,
                 critic_type=None,
                 min_batch_size=None,
                 n_skip_frames=0,
                 **kwargs):
        super(PolicyGradientAgent, self).__init__(brain=brain,
                                                  memory=memory,
                                                  maxlen=maxlen,
                                                  default_action=default_action,
                                                  random_action_fun=random_action_fun,
                                                  reward_clip=reward_clip,
                                                  n_skip_frames=n_skip_frames)
        self.gamma = gamma
        self.memory = memory
        assert critic_type in [None, "advantage", "gae"]
        self.critic_type = critic_type
        self.epsilon = epsilon
        self._frames_seen_since_last_update = 0
        if min_batch_size is None:
            self.min_batch_size = self.memory.size
        else:
            assert min_batch_size <= self.memory.size
            self.min_batch_size = min_batch_size
        self.horizon_clip = horizon_clip
        # Reads any additional attribute for the advatageous part.
        for k in kwargs.keys():
            self.__setattr__(k, kwargs[k])

    def _predict_action(self, recent_states):
        action_probability = np.squeeze(
            self.brain.predict(recent_states, network='actor')[0])
        action_index = np.random.choice(
            np.arange(self.brain.output_size), p=action_probability)
        return np.squeeze(
            np_utils.to_categorical(
                action_index, num_classes=self.brain.output_size))

    def _learn(self):
        # We compute the advantage only if not already done in observe.
        if not self.memory.get_transitions(range(-1, 0), keys=["terminal"])["terminal"][0]:
            self._update_last_episode_advantage_and_critic()
        if self._frames_seen_since_last_update < self.min_batch_size:
            message = 'Memory has not been fully updated before gradient descent.\
                We have only seen {} frames. Skipping learning for now.\
                Make sure learning frequency is not too large.'\
                .format(self._frames_seen_since_last_update)
            losses_names = get_losses_names_list(self.brain.models)
            warnings.warn(message)
            return dict(zip(losses_names, [np.nan] * len(losses_names))), False
        else:
            self._frames_seen_since_last_update = 0
        transitions = self.memory.sample(maxlen=self.maxlen,
                                         horizon=self.horizon_clip)
        losses = self.brain.train(
            state=transitions["state"],
            advantage=transitions["advantage"],
            action=transitions["action"],
            critic_target=transitions["critic_target"],
            epsilon=self.epsilon)
        return losses, True

    def _observe(self, state, reward, terminal):
        super(self.__class__, self)._observe(state, reward, terminal)
        self._frames_seen_since_last_update += 1
        if terminal:
            self._update_last_episode_advantage_and_critic()

    def _update_last_episode_advantage_and_critic(self):
        for k in range(min([self.maxlen - 1, self.episode_frames_seen])):
            self.memory.add("advantage", None)
            self.memory.add("critic_target", None)
        sample_size = min([self.episode_frames_seen, self.memory.size])
        if sample_size >= self.maxlen:
            episode_transitions = self.memory.get_transitions(
                range(-sample_size + self.maxlen - 1, 0),
                maxlen=self.maxlen,
                keys=["state", "next_state", "reward"])
            advantage, critic_target =\
                self._get_advantage_and_critic_target(episode_transitions)
            for k in range(len(advantage)):
                self.memory.add("advantage", advantage[k])
                self.memory.add("critic_target", critic_target[k])

    def _get_advantage_and_critic_target(self, transitions):
        discounted_reward = calc_discounted_rewards(transitions["reward"],
                                                    self.gamma)
        if self.critic_type == None:
            # Trick to disable minimisation
            critic_target = self.brain.predict(
                transitions["state"], network='critic')[:, 0]
            advantage = discounted_reward
        elif self.critic_type == "advantage":
            critic_target = discounted_reward
            advantage = discounted_reward - \
                self.brain.predict(
                    transitions["state"], network='critic')[:, 0]
        elif self.critic_type == "gae":
            critic_target = discounted_reward
            assert self.gae_lambda, "Define gae_lambda"
            advantage = calc_discounted_rewards(self._get_advantage(transitions),
                                                self.gamma * self.gae_lambda)
        return advantage, critic_target

    def _get_advantage(self, transitions):
        reward_estimate = np.squeeze(
            self.brain.predict(transitions["state"], network='critic'))
        next_reward_estimate = np.squeeze(
            self.brain.predict(transitions["next_state"], network='critic'))
        return transitions["reward"]\
            + self.gamma * next_reward_estimate\
            - reward_estimate
