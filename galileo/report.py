import glob
import json
import os

import numpy as np

import dash
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objs as go
from docopt import docopt

app = dash.Dash()

# http://colorbrewer2.org/#type=qualitative&scheme=Paired&n=12
colors = [
    "#a6cee3",
    "#1f78b4",
    "#b2df8a",
    "#33a02c",
    "#fb9a99",
    "#e31a1c",
    "#fdbf6f",
    "#ff7f00",
    "#cab2d6",
    "#6a3d9a",
    "#ffff99",
    "#b15928"
]


def load_report(report_filepath):
    try:
        with open(report_filepath, 'r') as file_:
            return json.load(file_)
    except Exception as e:
        print(e)
    return None


def get_experiments_directories(root_directory):
    if root_directory is not None:
        root_directory = os.path.join(root_directory, '')
        config_files = glob.glob(root_directory + '*/*config.json') + \
            glob.glob(root_directory + '*/*config.yaml')
        experiments_directories = map(
            lambda x: os.path.dirname(x), config_files)
        return sorted(list(set(experiments_directories)))
    return []


def serve_layout(root_directory):
    return html.Div(
        children=[
            html.H1(children='Galileo experiment reporting tool'),
            html.Br(),

            html.Label(children='Experiments root directory'),
            dcc.Input(
                id='experiment-directory-input',
                type='text',
                value=root_directory,
                style={'width': 500}
            ),
            html.H2(children='Training summary'),
            html.Br(),

            html.Label(children='Experiments'),
            dcc.Dropdown(
                id='experiment-training-dropdown',
                options=[],
                multi=True
            ),
            html.Br(),

            html.Label(children='Reporting type'),
            dcc.RadioItems(
                id='reporting-type-radio-items',
                options=[
                    {'label': 'Aggregated', 'value': 'aggregated'},
                    {'label': 'Individual', 'value': 'individual'}
                ],
                value='aggregated',
                labelStyle={'display': 'inline-block'}
            ),
            html.Br(),

            html.Div(
                id='individual-runs-div',
                children=[
                    html.Div(
                        id='{}-runs-div'.format(os.path.basename(exp_directory)),
                        children=[
                            html.Label(
                                children=os.path.basename(
                                    exp_directory) + ' runs'
                            ),
                            dcc.Dropdown(
                                id='{}-runs-dropdown'.format(
                                    os.path.basename(exp_directory)),
                                options=[
                                    {
                                        'label': os.path.basename(run_directory),
                                        'value': run_directory
                                    }
                                    for run_directory in get_experiments_directories(exp_directory)
                                ],
                                multi=True
                            ),
                            html.Br()
                        ],
                        style={'display': 'none'}
                    )
                    for exp_directory in get_experiments_directories(root_directory)
                ]
            ),

            html.Div(id='train-graph-div'),
            html.H2(children='Testing summary'),
            html.Br(),

            html.Label(children='Experiment'),
            dcc.Dropdown(
                id='experiment-testing-dropdown',
                options=[],
                multi=False
            ),
            html.Br(),

            html.Label(children='Run name'),
            dcc.Dropdown(
                id='runs-names-dropdown',
                options=[],
                multi=False
            ),
            html.Br(),

            html.Label(children='Plot type'),
            dcc.RadioItems(
                id='figure-type-radio-item',
                options=[
                    {
                        'label': 'Separated',
                        'value': 'separated'
                    }, {
                        'label': 'Same',
                        'value': 'same'
                    }
                ],
                value='same'
            ),
            html.Br(),

            html.Label(children='Episodes'),
            dcc.RangeSlider(
                id='episodes-range-slider',
                count=1,
                min=0,
                max=0,
                value=[0, 0]
            ),
            html.Br(),

            html.Label(children='Raw Input state'),
            dcc.Checklist(
                id='raw-state-checklist',
                values=[],
                labelStyle={'display': 'inline-block'}
            ),
            html.Br(),

            html.Label(children='Preprocessed Input state'),
            dcc.Checklist(
                id='preprocessed-state-checklist',
                values=[],
                labelStyle={'display': 'inline-block'}
            ),
            html.Br(),

            html.Label(children='Action'),
            dcc.Checklist(
                id='action-checklist',
                values=[],
                labelStyle={'display': 'inline-block'}
            ),
            html.Br(),

            html.Label(children='Reward'),
            dcc.Checklist(
                id='reward-checklist',
                options=[
                    {'value': 'instantaneous', 'label': 'Instantaneous'},
                    {'value': 'cumulated', 'label': 'Cumulated'}
                ],
                values=['cumulated'],
                labelStyle={'display': 'inline-block'}
            ),
            html.Div(
                id='test-graph-div'
            )
        ]
    )


def make_update_runs_div_callback(name):
    @app.callback(
        dash.dependencies.Output('{}-runs-div'.format(name), 'style'),
        [dash.dependencies.Input('experiment-training-dropdown', 'value'),
         dash.dependencies.Input('reporting-type-radio-items', 'value')]
    )
    def callback(directories, reporting_type):
        if (directories is None) or (reporting_type == 'aggregated'):
            return {'display': 'none'}
        elif name in map(os.path.basename, directories):
            return {'display': 'block'}
        else:
            return {'display': 'none'}


def create_callbacks(root_directory):

    ##########################################
    # Training section callbacks dependencies #
    ##########################################

    @app.callback(
        dash.dependencies.Output('experiment-training-dropdown', 'options'),
        [dash.dependencies.Input('experiment-directory-input', 'value')]
    )
    def update_experiment_dropdown_options(root_directory):
        experiments_directories = get_experiments_directories(root_directory)
        return [{'label': os.path.basename(directory), 'value': directory} for directory in experiments_directories]

    @app.callback(
        dash.dependencies.Output('train-graph-div', 'children'),
        [dash.dependencies.Input('reporting-type-radio-items', 'value'),
         dash.dependencies.Input('experiment-training-dropdown', 'value')] +
        [dash.dependencies.Input('{}-runs-dropdown'.format(os.path.basename(name)), 'value')
         for name in get_experiments_directories(root_directory)]
    )
    def update_train_graph_div_children(reporting_type, directories, *args):
        data = []
        if directories is None:
            return []
        if reporting_type == 'individual':
            for exp_directory in directories:
                experiment_name = exp_directory.split('/')[-1]
                train_report_paths = glob.glob(
                    exp_directory + '*/*/train_report.json')
                for train_report_path in train_report_paths:
                    train_report = load_report(train_report_path)
                    if train_report is None:
                        continue
                    run_directory = os.path.basename(train_report_path)
                    experiment_name = os.path.basename(run_directory)
                    epochs = len(train_report['terminal'])
                    epoch_len = len(train_report['terminal'][0])
                    train_report_flattened = {}
                    for key in train_report.keys():
                        train_report_flattened[key] = [el for list_ in train_report[key]
                                                       for el in list_]
                    plotting_keys = set(train_report.keys(
                    )) - set(['index', 'raw_state', 'preprocessed_state', 'action', 'terminal'])
                    data.extend(
                        [
                            go.Scatter(y=train_report_flattened[key],
                                       name="{} ({})".format(
                                           key, experiment_name),
                                       legendgroup=key)
                            for key in plotting_keys
                        ]
                    )
        else:
            for exp_directory in directories:
                experiment_name = exp_directory.split('/')[-1]
                train_reports_paths = glob.glob(
                    exp_directory + '*/*/train_report.json')
                train_reports = [json.load(open(path))
                                 for path in train_reports_paths]
                if len(train_reports) == 0:
                    return []
                epochs = min([len(r['terminal']) for r in train_reports])
                layout = go.Layout(height=500)

                # Assumes all train reports have same structure
                epoch_len = len(train_reports[0]['terminal'][0])
                train_report_keys = train_reports[0].keys()
                plotting_keys = set(train_reports[0].keys(
                )) - set(['index', 'raw_state', 'preprocessed_state', 'action', 'terminal'])

                train_reports_means = {}
                train_reports_stdevs_top = {}
                train_reports_stdevs_bottom = {}
                for key in plotting_keys:
                    # The length of experiments may vary
                    values = [np.array(r[key]).flatten() for r in train_reports]
                    max_number_of_values = max([v.shape[0] for v in values])
                    mean = np.zeros(max_number_of_values)
                    stddev = np.zeros(max_number_of_values)
                    for i in range(max_number_of_values):
                        observed_values_at_i = np.array(
                            [v[i] for v in filter(lambda v: v.shape[0] > i, values)])
                        mean[i] = np.mean(observed_values_at_i)
                        stddev[i] = np.std(observed_values_at_i)
                    train_reports_means[key] = mean
                    train_reports_stdevs_top[key] = mean + stddev
                    train_reports_stdevs_bottom[key] = mean - stddev
                color_index = 0
                for key in plotting_keys:
                    color_index = (color_index + 1) % len(colors)
                    c = colors[color_index]
                    l_mean = go.Scatter(
                        x=[x for x in range(
                            len(train_reports_means[key]))],
                        y=train_reports_means[key],
                        line=go.Line(color=colors[color_index]),
                        name="{} mean ({})".format(key, experiment_name),
                        hoverlabel={'namelength': -1},
                        legendgroup="{}".format(key))
                    l_std = go.Scatter(
                        x=[x for x in range(len(train_reports_stdevs_top[key]))] + [
                            x for x in reversed(range(len(train_reports_stdevs_bottom[key])))],
                        y=np.concatenate(
                            (train_reports_stdevs_top[key], train_reports_stdevs_bottom[key][::-1])),
                        name="{} std ({})".format(key, experiment_name),
                        fill='tozerox',
                        line=go.Line(color=colors[color_index], width=0),
                        showlegend=False,
                        hoverinfo="none",
                        legendgroup="{}".format(key))
                    data.extend([l_mean, l_std])

        figure = go.Figure(
            data=data,
            layout=go.Layout(height=500)
        )
        return dcc.Graph(id='train-graph', figure=figure)

    ##########################################
    # Testing section callbacks dependencies #
    ##########################################

    @app.callback(
        dash.dependencies.Output('experiment-testing-dropdown', 'options'),
        [dash.dependencies.Input('experiment-directory-input', 'value')]
    )
    def update_experiment_testing_dropdown_options(root_directory):
        experiments_directories = get_experiments_directories(root_directory)
        return [{'label': os.path.basename(directory), 'value': directory} for directory in experiments_directories]

    @app.callback(
        dash.dependencies.Output('runs-names-dropdown', 'options'),
        [dash.dependencies.Input('experiment-testing-dropdown', 'value')]
    )
    def runs_names_radio_items_options(directory):
        if (directory is None):
            return []
        runs_directories = get_experiments_directories(directory)
        return [
            {
                'label': os.path.basename(run_directory),
                'value': run_directory
            } for run_directory in runs_directories
        ]

    @app.callback(
        dash.dependencies.Output('episodes-range-slider', 'marks'),
        [dash.dependencies.Input('runs-names-dropdown', 'value')]
    )
    def update_episodes_range_slider_marks(directory):
        if (directory is None):
            return []
        test_report = load_report(
            os.path.join(directory, 'test_report.json'))
        if (test_report is None):
            return []
        return [str(m) if (m % 5) == 0 else '' for m in range(len(test_report['action']))]

    @app.callback(
        dash.dependencies.Output('episodes-range-slider', 'max'),
        [dash.dependencies.Input('runs-names-dropdown', 'value')]
    )
    def update_episodes_range_slider_max(directory):
        if (directory is None):
            return 0
        test_report = load_report(
            os.path.join(directory, 'test_report.json'))
        if (test_report is None):
            return 0
        return len(test_report['action']) - 1

    @app.callback(
        dash.dependencies.Output('raw-state-checklist', 'options'),
        [dash.dependencies.Input('runs-names-dropdown', 'value')]
    )
    def update_raw_state_checklist_options(directory):
        if (directory is None):
            return []
        test_report = load_report(
            os.path.join(directory, 'test_report.json'))
        if (test_report is None):
            return []
        return [{'value': i, 'label': i} for i in range(len(test_report['raw_state'][0][0]))]

    @app.callback(
        dash.dependencies.Output('preprocessed-state-checklist', 'options'),
        [dash.dependencies.Input('runs-names-dropdown', 'value')]
    )
    def update_preprocessed_state_checklist_options(directory):
        if (directory is None):
            return []
        test_report = load_report(
            os.path.join(directory, 'test_report.json'))
        if (test_report is None):
            return []
        return [{'value': i, 'label': i} for i in range(len(test_report['preprocessed_state'][0][0]))]

    @app.callback(
        dash.dependencies.Output('action-checklist', 'options'),
        [dash.dependencies.Input('runs-names-dropdown', 'value')]
    )
    def update_action_checklist_options(directory):
        if (directory is None):
            return []
        test_report = load_report(
            os.path.join(directory, 'test_report.json'))
        if (test_report is None):
            return []
        return [{'value': i, 'label': i} for i in range(len(test_report['action'][0][0]))]

    @app.callback(
        dash.dependencies.Output('test-graph-div', 'children'),
        [dash.dependencies.Input('runs-names-dropdown', 'value'),
         dash.dependencies.Input('figure-type-radio-item', 'value'),
         dash.dependencies.Input('raw-state-checklist', 'values'),
         dash.dependencies.Input('preprocessed-state-checklist', 'values'),
         dash.dependencies.Input('action-checklist', 'values'),
         dash.dependencies.Input('reward-checklist', 'values'),
         dash.dependencies.Input('episodes-range-slider', 'value')]
    )
    def update_test_graph_div_children(directory, figure_type, raw_state_list, preprocessed_state_list, action_list, reward_type_list, episodes_range):
        if directory is None:
            return []
        test_report = load_report(
            os.path.join(directory, 'test_report.json'))
        if test_report is not None:
            n_episodes = len(test_report['reward'])
            if figure_type == 'separated':
                graphs = []
                for i in range(episodes_range[0], min(episodes_range[1] + 1, n_episodes)):

                    if 'index' in test_report.keys():
                        x = test_report['index'][i]
                    else:
                        x = range(len(test_report['raw_state'][i]))

                    data = [
                        go.Scatter(
                            x=x,
                            y=[state[j]
                                for state in test_report['raw_state'][i]],
                            name='Raw input state ({})'.format(j)
                        )
                        for j in raw_state_list
                    ] + [
                        go.Scatter(
                            x=x,
                            y=[state[j]
                                for state in test_report['preprocessed_state'][i]],
                            name='Preprocessed state ({})'.format(j)
                        )
                        for j in preprocessed_state_list
                    ] + [
                        go.Scatter(
                            x=x,
                            y=[action[j]
                                for action in test_report['action'][i]],
                            name='Action ({})'.format(j)
                        )
                        for j in action_list
                    ]
                    reward_data = test_report['reward'][i]
                    if 'instantaneous' in reward_type_list:
                        data += [
                            go.Scatter(x=x, y=reward_data,
                                       name='Reward ({})'.format('instantaneous'))
                        ]
                    if 'cumulated' in reward_type_list:
                        reward_data = np.cumsum(reward_data)
                        data += [
                            go.Scatter(x=x, y=reward_data,
                                       name='Reward ({})'.format('cumulated'))
                        ]

                    figure = go.Figure(
                        data=data,
                        layout=go.Layout(
                            height=500, title='Episode {}'.format(i))
                    )
                    graphs.append(
                        dcc.Graph(id='test-graph-{}'.format(i), figure=figure))
                return graphs
            elif figure_type == 'same':
                graphs = []

                x = []
                raw_state_data = {}
                preprocessed_state_data = {}
                action_data = {}
                reward_data = []
                for i in range(episodes_range[0], min(episodes_range[1] + 1, n_episodes)):

                    if 'index' in test_report.keys():
                        x.extend(test_report['index'][i])
                    else:
                        x.extend(range(len(test_report['raw_state'][i])))
                    for j in raw_state_list:
                        raw_state_data[j] = raw_state_data.get(j, [])
                        raw_state_data[j].extend([state[j]
                                                  for state in test_report['raw_state'][i]])

                    for j in preprocessed_state_list:
                        preprocessed_state_data[j] = preprocessed_state_data.get(j, [
                        ])
                        preprocessed_state_data[j].extend(
                            [state[j] for state in test_report['preprocessed_state'][i]])

                    for j in action_list:
                        action_data[j] = action_data.get(j, [])
                        action_data[j].extend([action[j]
                                               for action in test_report['action'][i]])

                    reward_data.extend(test_report['reward'][i])

                data = [
                    go.Scatter(
                        x=x,
                        y=raw_state_data[j],
                        name='Raw input state ({})'.format(j)
                    ) for j in raw_state_list
                ] + \
                    [
                    go.Scatter(
                        x=x,
                        y=preprocessed_state_data[j],
                        name='Preprocessed state ({})'.format(j)
                    ) for j in preprocessed_state_list
                ] + \
                    [
                    go.Scatter(
                        x=x,
                        y=action_data[j],
                        name='Action ({})'.format(j)
                    ) for j in action_list
                ]
                if 'instantaneous' in reward_type_list:
                    data += [
                        go.Scatter(x=x, y=reward_data,
                                   name='Reward ({})'.format(reward_type_list))
                    ]
                if 'cumulated' in reward_type_list:
                    reward_data = np.cumsum(reward_data)
                    data += [
                        go.Scatter(x=x, y=reward_data,
                                   name='Reward ({})'.format('cumulated'))
                    ]

                figure = go.Figure(
                    data=data,
                    layout=go.Layout(
                        height=500, title='Backtest'
                    )
                )
                return dcc.Graph(id='test-graph-0', figure=figure)
        return []


def reporting(root_directory):
    app.layout = serve_layout(root_directory)
    # app.config.supress_callback_exceptions = True
    app.css.append_css(
        {'external_url': 'https://codepen.io/chriddyp/pen/bWLwgP.css'})
    create_callbacks(root_directory)
    app.run_server(debug=True, host='0.0.0.0')
