import functools
import importlib
import json
import math
import os
import shutil
import tarfile
import warnings

import numpy as np
import six

import plotly.graph_objs as go
from scipy.signal import lfilter


def get_losses_names_list(models):
    losses_names_list = []
    for model_name, model in six.iteritems(models):
        if model['trainable']:
            if type(model['object'].loss) in (str, unicode):
                losses_names = [model_name + "_" + model['object'].loss]
            elif type(model['object'].loss) is dict:
                losses_names = [
                    loss_name + "_" + loss_type
                    for loss_name, loss_type in six.iteritems(model['object'].loss)
                ]
            else:
                raise ValueError("Unknown loss type: {} of type {}".format(
                    model['object'].loss, type(model['object'].loss)))
            losses_names_list.extend(losses_names)
    return [
        loss_name for loss_name in losses_names_list if loss_name is not None
    ]


def lambda_maker(expr):
    assert expr.startswith('lambda')
    assert ':' in expr
    fun = eval(expr)
    assert callable(fun)
    return fun


def movingaverage(interval, window_size):
    window = np.ones(int(window_size)) / float(window_size)
    return np.convolve(interval, window, 'same')


def calc_discounted_rewards(reward_vector, gamma):
    """function that returns teh discounted rewards fo a reward vector

    Args
        reward_vector (list): list of rewards.
        gamma (float): gamma < 1 the discount factor.
    Returns
        (numpy.array): array of discounted rewards
    """
    return np.array(lfilter([1], [1, -gamma], reward_vector[::-1])[::-1])


def build_object(object_dict):
    print('______________')
    print("Building {} object".format(object_dict['class_name']))
    print("With config {}".format(object_dict.get('config', '{}')))
    print('______________')
    object_path_list = object_dict['class_name'].split('.')
    module = importlib.import_module('.'.join(object_path_list[:-1]))
    if 'config' not in object_dict.keys():
        cmd = "module." + object_path_list[-1] + "()"
        return eval(cmd)
    for param_name in object_dict['config'].keys():
        # Built nested objects
        if type(object_dict['config'][param_name]) == dict:
            if 'class_name' in object_dict['config'][param_name].keys():
                object_dict['config'][param_name] = build_object(
                    object_dict['config'][param_name])
        # Built nested lists of objects
        if type(object_dict['config'][param_name]) == list:
            for i in range(len(object_dict['config'][param_name])):
                if type(object_dict['config'][param_name][i]) == dict:
                    if 'class_name' in object_dict['config'][param_name][i].keys():
                        object_dict['config'][param_name][i] = build_object(
                            object_dict['config'][param_name][i])
    cmd = "module." + object_path_list[-1] + "(**object_dict['config'])"
    return eval(cmd)


def markout(prices, actions, horizon):
    """Return markouts matrix

    Args:
        prices (numpy.ndarray): Price array (1d).
        actions (numpy.ndarray): Actions array (1d). Must only contain 0 (do nothing), +1 (buy) or -1 (sell) values.
        horizon (int): Maximum horizon.

    """
    assert set(actions).issubset({0, 1, -1})
    assert len(prices) == len(actions)
    markouts = np.array([actions[i] * (prices[i + 1:i + horizon + 1] - prices[i])
                         for i in np.where(actions[:-horizon] != 0)[0]])
    return markouts


def markout_figure(markouts, actions, ticksize=0.1, n_markouts=5, prediction_horizons=None, normed_histogram=False, colorscale='Reds'):
    colors = {0: 'rgb(0,0,0)',
              1: 'rgb(0,103,51)',
              -1: 'rgb(169,17,0)'}
    indices = np.random.choice(
        range(len(markouts)), size=n_markouts, replace=False)
    normalised_markouts = markouts / ticksize
    min_pnl = np.min(normalised_markouts[indices, :])
    max_pnl = np.max(normalised_markouts[indices, :])
    pnl_vector = np.arange(min_pnl, max_pnl + 2, 1) - 1 / 2.
    horizon_vector = range(1, normalised_markouts.shape[1] + 1)
    histograms = []
    for horizon in range(normalised_markouts.shape[1]):
        histogram = np.histogram(
            normalised_markouts[:, horizon], bins=pnl_vector)[0]
        if normed_histogram:
            histogram = histogram / np.max(histogram)
        histograms.append(histogram)
    histograms = np.array(histograms)
    data = [
        go.Heatmap(x=horizon_vector, y=pnl_vector, z=histograms.T,
                   colorscale=colorscale, showscale=False)
    ] + [
        go.Scatter(x=horizon_vector,
                   y=np.mean(normalised_markouts, axis=0),
                   line=dict(color='black', width=3),
                   name='Average')
    ] + [
        go.Scatter(x=horizon_vector,
                   y=np.median(normalised_markouts, axis=0),
                   line=dict(color='black', dash='dash', width=3),
                   name='Median')
    ] + [
        go.Scatter(x=horizon_vector,
                   y=normalised_markouts[i],
                   line=dict(color=colors[actions[actions != 0][i]], width=1),
                   name='Markout {}'.format(i),
                   legendgroup='Markout {}'.format(i)) for i in indices
    ]
    if prediction_horizons is not None:
        data += [
            go.Scatter(x=[h, h],
                       y=[min_pnl, max_pnl],
                       line=dict(dash='dot', color='grey', width=1),
                       mode='lines',
                       showlegend=False)
            for h in prediction_horizons
        ]
    layout = go.Layout(
        title='{} market states - {} trades ({} buy - {} sell).'.format(
            len(actions), len(markouts), np.sum(actions == 1), np.sum(actions == -1)),
        yaxis=dict(range=[min_pnl, max_pnl], title='P/L per trade (ticks)'),
        xaxis=dict(range=[min(horizon_vector), max(
            horizon_vector)], title='Time from entry')
    )
    return go.Figure(data=data, layout=layout)


def lagged_features(X, maxlen, time_distributed):
    X_lagged = np.stack([X[i:len(X) - maxlen + i + 1]
                         for i in range(maxlen)], axis=-2)
    if not time_distributed:
        X_lagged = X_lagged.reshape(X_lagged.shape[0], -1)
    return X_lagged


def lookahead_targets(y, lookahead_vector, lookahead_reduce=lambda x: x[-1]):
    if not len(y.shape) > 1:
        y = y[:, np.newaxis]
    lookahead_matrix = [
        np.array([
            y[i:len(y) - max(lookahead_vector) + i]
            for i in range(la + 1)
        ])
        for la in lookahead_vector
    ]
    y = np.stack(map(lookahead_reduce, lookahead_matrix), -2)
    y = y.reshape(y.shape[0], -1)
    return y


def decompress_brain(filepath):
    tar = tarfile.open(filepath, "r:gz")
    brain_path = os.path.join(os.path.dirname(filepath), 'brain')
    tar.extractall(brain_path)
    tar.close()
    return brain_path


def load_brain(filepath, delete_folder=True):
    brain_path = decompress_brain(filepath)
    with open(os.path.join(brain_path, 'brain_dict.json'), 'r') as file_:
        brain_dict = json.load(file_)
        brain_dict['config']['loading'] = True

    brain = build_object(brain_dict)
    with open(os.path.join(brain_path, 'models_dict.json'), 'r') as file_:
        models_dict = json.load(file_)
    for key in models_dict.keys():
        brain.models[key] = {}
        brain.models[key]['trainable'] = models_dict[key]['trainable']
        brain.models[key]['inference'] = models_dict[key]['inference']
        brain.models[key]['object'] = brain._load_model_object(
            os.path.join(brain_path, models_dict[key]['filename']), custom_objects=brain.custom_objects)
    brain.built = True
    brain._compile()
    if delete_folder:
        shutil.rmtree(brain_path)
    return brain


def calc_spread(prices, spread_coefficients, intercept=0, scale=1):
    """Calculate the spread based on spread_coefficients.

    Args:
        prices (numpy.array): Array containing the prices (bid, ask) of
            different products, i.e: [p1_b, p1_a, p2_b, p2_a].
        spread_coefficients (list): A list of signed integers defining how much
            of each product to buy (positive) or sell (negative) when buying or
            selling the spread.
        intercept (float): Intercept of the spread
        scale (float): standard deviation of the spread before normalisation
    Returns:
        tuple:
            - (float) spread bid price,
            - (float) spread ask price.
    """
    assert 2 * len(spread_coefficients) == len(prices)
    spread_bid = sum([
        spread_coefficients[i] * prices[2 * i
                                        + int(spread_coefficients[i] < 0)]
        for i in range(len(spread_coefficients))
    ])
    spread_ask = sum([
        spread_coefficients[i] * prices[2 * i
                                        + int(spread_coefficients[i] > 0)]
        for i in range(len(spread_coefficients))
    ])
    return (spread_bid - intercept) / scale, (spread_ask - intercept) / scale


def find_nearest_index(array, value):
    assert np.all(np.array(array) == np.sort(array))
    idx = np.searchsorted(array, value, side="left")
    if idx > 0 and (idx == len(array) or math.fabs(value - array[idx - 1]) < math.fabs(value - array[idx])):
        return idx - 1
    else:
        return idx


def find_bracket(array, value):
    nearest_index = find_nearest_index(array, value)
    if (value > max(array)) or (value < min(array)):
        raise ValueError(
            "Bracket is not defined because the value ({}) is oustide the specified range ({}).".format(value, array))
    if value > array[nearest_index]:
        return nearest_index, nearest_index + 1
    elif value < array[nearest_index]:
        return nearest_index - 1, nearest_index
    else:
        return nearest_index, nearest_index


def linear_interpolation(x, y, x0):
    x = np.array(x)
    y = np.atleast_2d(np.array(y))
    indices = find_bracket(x, x0)
    if indices[0] == indices[1]:
        return y[:, indices[0]]
    xs = map(lambda i: x[i], indices)
    ys = map(lambda i: y[:, i], indices)
    a = (ys[0] - ys[1]) / (xs[0] - xs[1])
    b = (xs[1] * ys[0] - xs[0] * ys[1]) / (xs[1] - xs[0])
    return a * x0 + b


def calc_ema(x, s, alpha, dt=1, mode='average'):
    """Calculates the ema on the fly.

    Args:
        x (float or np.array): new observation
        s (float or np.array): current ema value
        alpha (float): ema coefficient (1/tau)
        dt (float): Time difference between observations
        mode (str): average or sum
    Returns:
        (float) ema
    """
    w = np.exp(-alpha * dt)
    if mode == 'average':
        return s * w + x * (1 - w)
    elif mode == 'sum':
        return s * w + x
    else:
        raise ValueError("EMA mode ({}) unknown.".format(mode))


def get_params_dict(params):
    if 'self' in params:
        del params['self']
    if 'kwargs' in params.keys():
        params.update(params['kwargs'])
        del params['kwargs']
    if '__class__' in params.keys():
        del params['__class__']
    return params


def get_class_name(object_):
    return object_.__module__ + '.' + object_.__class__.__name__
