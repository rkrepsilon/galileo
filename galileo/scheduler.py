import warnings

import numpy as np
from scipy.optimize import fmin
from scipy.special import poch

from .core import DataIterator


class Scheduler(object):
    """ Rate schedule for learning parameters
    """

    def __init__(self, decay_mode, initial_value, **kwargs):
        """
        Function to update a scheduled parameter during the training.

        Args:
            decay_mode (str): Choice of decay mode for the parameter.
                "linear": value = value - decay_rate
                "exponential": value = value*decay_rate
                "factorial": value = value/(1+alpha*i)
            initial_value (float): initial value of scheduled parameter
            decay_rate (float, optional): decay rate can be specified, otherwise computed from n and final_rate
            alpha (float, optional): for factorial decay only.
            n_episodes (int, optional): total frames that will be seen during training, needs to be declared with final_value
            final_value (float, optional): final value can be specified alongside n

        Yields:
            (float): new scheduled parameter value
        """
        assert decay_mode in ("linear", "exponential", "factorial"),\
            "Unsupported decay mode : " + str(decay_mode)
        self.decay_mode = decay_mode
        self.initial_value = float(initial_value)
        self._current_value = float(initial_value)
        if decay_mode in ('linear', 'exponential'):
            if 'decay_rate' not in kwargs.keys():
                assert 'n_episodes' in kwargs.keys(
                ), 'If decay_rate is not specified, n_episodes must be specified.'
                assert 'final_value' in kwargs.keys(
                ), 'If decay_rate is not specified, final_value must be specified.'
                self.n_episodes = kwargs['n_episodes']
                self.final_value = kwargs['final_value']
                self.decay_rate = self._get_decay_rate()
            else:
                self.decay_rate = kwargs['decay_rate']
                if (self.decay_mode == 'linear') and (self.decay_rate > 0):
                    warnings.warn(
                        "Decay rate must be negative in the linear case. We will use {}.".
                        format(-self.decay_rate))
                    self.decay_rate = -self.decay_rate
        elif (decay_mode == 'factorial'):
            self.decay_rate = 1  # Initial decay rate
            assert 'alpha' in kwargs.keys(
            ), "When using factorial, alpha must be specified."
            self.alpha = kwargs['alpha']
            if 'final_value' in kwargs.keys():
                warnings.warn("When using factorial, final_value is not used.")
            if 'n_episodes' in kwargs.keys():
                warnings.warn("When using factorial, n_episodes is not used.")
        self._operator = self._get_operator()
        self._updates_done = 0

    def __iter__(self):
        return self

    def next(self):
        if self._updates_done == 0:
            self._current_value = self.initial_value
        else:
            if self.decay_mode == "factorial":
                self.decay_rate = 1. / (1 + self.alpha * self._updates_done)
            self._current_value = self._operator(self._current_value,
                                                 self.decay_rate)
        self._updates_done += 1
        return self._current_value

    def _get_operator(self):
        if self.decay_mode == "linear":
            return float.__add__
        else:
            return float.__mul__

    def _get_decay_rate(self):
        if self.decay_mode == "linear":
            return (self.final_value - self.initial_value) / self.n_episodes
        elif self.decay_mode == "exponential":
            return np.exp(
                np.log(self.final_value / self.initial_value) /
                self.n_episodes)
        else:
            raise ValueError("Unknown decay mode {}.".format(self.decay_mode))

    def _get_alpha(self):
        def residual_fun(alpha):
            total_lr_decay = (1. / alpha)**(self.n_episodes - 1) / \
                poch(1 + 1. / alpha, (self.n_episodes - 1))
            print(total_lr_decay, alpha, self.n_episodes)
            return (total_lr_decay - self.final_value / self.initial_value)**2

        alpha = fmin(residual_fun, 0.01)[0]
        print("Residual", residual_fun(alpha), 'Alpha found', alpha)
        assert np.isclose(residual_fun(alpha), 0, atol=1e-5)
        return alpha
