import pip
from pip.req import parse_requirements
from setuptools import find_packages, setup

requirements = parse_requirements(
    "requirements.txt", session=pip.download.PipSession())
reqs = [str(ir.req) for ir in requirements]

setup(
    name="galileo",
    version=open('repo.properties').read().strip().split('=')[1],
    author="Prediction Machines",
    author_email="info@prediction-machines.com",
    description="A framework for reinforcement learning applied to trading",
    long_description=open('README.md').read(),
    url='https://bitbucket.srv.rkr.hkg/projects/PM/repos/galileo',
    packages=find_packages(),
    include_package_data=True,
    install_requires=reqs,
    setup_requires=[
        'pytest-runner'
    ],
    tests_require=[
        "pytest-cov",
        "pytest"
    ],
    entry_points={
        'console_scripts': [
            'galileo=galileo.galileo:main'
        ]
    }
)
