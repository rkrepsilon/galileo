FROM rkr/centos:7.2.1511_15
MAINTAINER Prediction Machines <info@prediction-machines.com>

# Adding the files we need
ADD README.md /tmp/galileo/
ADD repo.properties /tmp/galileo/
ADD requirements.txt /tmp/galileo/
ADD setup.py /tmp/galileo/
ADD galileo /tmp/galileo/galileo/
ADD examples /tmp/galileo/examples/
ADD tests /tmp/galileo/tests/
ADD files/devtools-gcc6.repo /etc/yum.repos.d/

# Jupyterhub config
COPY jupyter_notebook_config.py /root/.jupyter/

# Install dependencies
RUN touch /var/lib/rpm/* && \
    yum install -y tkinter gcc-c++ gcc-6.3.0 Cython hdf5-devel krb5-devel libxslt-devel libxml2-devel && \
    yum clean all && \
    rm -rf /var/cache/yum/* && \
    pip install -r /tmp/galileo/requirements.txt && \
    rm -rf /root/.cache && \
    pushd /tmp/galileo && \
    python setup.py install && \
    pip install jupyter==1.0.0 plotly==2.2.3 virtualenv==15.1.0 && \
    rm -rf /tmp/galileo

# Environment variables
ENV KERAS_BACKEND=theano
ENV PYTHONUNBUFFERED=TRUE
