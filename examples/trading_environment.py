"""
The aim of this file is to give a standalone example of how an environment  runs.
"""

import numpy as np

from galileo.core import DataIterator
from galileo.envs import SpreadTrading
from galileo.iter.stochastic import BoundedRandomWalk
from six.moves import input

episode_length = 200

iterator = BoundedRandomWalk(
    nsteps=20, length=episode_length, function='ornstein', sigmu_ratio=3)


trading_penalty = 0.2
time_penalty = 0
# history_length number of historical states in the observation vector.
history_length = 2

environment = SpreadTrading(
    spread_coefficients=[1],
    data_iterator=iterator,
    trading_penalty=trading_penalty,
    max_entries=5)

environment.reset()
environment.render()
while True:
    action = input("Action: Buy (b) / Sell (s) / Hold (enter): ")
    if action == 'b':
        action = [0, 1, 0]
    elif action == 's':
        action = [0, 0, 1]
    else:
        action = [1, 0, 0]
    environment.step(action)
    environment.render()
