"""
In this example we show how a random iterator is coded.
All iterators inherit from the DataIterator class
The class yields tuple (bid_price,ask_price)
"""
import numpy as np
from galileo.core import DataIterator


class RandomIterator(DataIterator):

    def generator(self):
        while True:
            val = np.random.randn()
            yield val, val + 0.1


time_series_length = 10
mygen = RandomIterator(length=10, add_timer=False)
prices_time_series = [mygen.next() for _ in range(time_series_length)]
print(prices_time_series)
