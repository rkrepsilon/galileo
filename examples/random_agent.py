"""
The aim of this file is to give a standalone example of how an environment
can be integrated with a generator and a random agent.
"""
import numpy as np
from galileo.core import Agent
from galileo.iter import TriangularWalk
from galileo.envs import SpreadTrading

iterator = TriangularWalk(nsteps=5, length=100)

environment = SpreadTrading(
    spread_coefficients=[1],
    data_iterator=iterator,
    trading_penalty=0.025)


class RandomAgent(Agent):
    learner = False

    def act(self, state):
        action = np.random.choice(["buy", "sell", "hold"])
        return SpreadTrading._actions[action]


agent = RandomAgent()

state = environment.reset()
environment.render()
for _ in range(100):
    action = agent.act(state)
    state, _, _, _ = environment.step(action)
    environment.render()
