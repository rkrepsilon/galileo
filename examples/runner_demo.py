"""
In this example we show how to declare an agent, build an environment and
orchestrate them with a runner.
"""
from galileo.agents.rl import DQNAgent
from galileo.brains import QBrain
from galileo.envs import SpreadTrading
from galileo.envs.openaigym import CartPole
from galileo.iter import TriangularWalk
from galileo.iter.deterministic import WavySignal
from galileo.memories import EpisodicMemory
from galileo.preprocessing import Transformer
from galileo.runner import Runner
from galileo.scheduler import Scheduler
from keras.layers import Dense
from sklearn.preprocessing import StandardScaler

episode_length = 2000
maxlen = 2
env_params = {
    "spread_coefficients": [1],
    "data_iterator": WavySignal(period_1=25,
                                period_2=50,
                                epsilon=-0.5,
                                length=episode_length),
    "trading_penalty": .2,
}
environment = SpreadTrading(**env_params)

# environment = CartPole()

feature_block = [
    Dense(12, activation='relu'),
    Dense(5, activation='relu')
]

maxlen = 1

brain = QBrain(
    input_shape=(maxlen, ) + environment.state_shape,
    output_size=environment.n_actions,
    feature_block=feature_block,
    is_dueling=True)
memory = EpisodicMemory(size=2000, per=True)
agent = DQNAgent(
    brain=brain,
    memory=memory,
    gamma=0.9,
    target_update_frequency=5,
    maxlen=maxlen,
    is_double=True)
learning_frequency = 1

preprocessor = Transformer(estimator=StandardScaler(), steps=10000)
runner = Runner(env=environment, agent=agent, preprocessor=preprocessor)

n_episodes = 5
schedules = {
    'epsilon':
    Scheduler(
        decay_mode='exponential',
        initial_value=1,
        final_value=0.01,
        n_episodes=n_episodes)
}

runner.train(
    schedules=schedules,
    num_episodes=n_episodes,
    learning_frequency=learning_frequency)

runner.test(num_episodes=1, render=False, render_frequency=1)
