"""In this example we demonstrate how to use a state preprocessor
"""
import numpy as np

from galileo.envs import SpreadTrading
from galileo.iter import TriangularWalk
from galileo.preprocessing import diff

preprocessor = diff()

spread_coefficients = [1]
dtt = SpreadTrading(
    data_iterator=TriangularWalk(nsteps=5, length=100),
    spread_coefficients=spread_coefficients)
state = dtt.reset()
for _ in range(100):
    state, reward, done, _ = dtt.step([1, 0, 0])
    if preprocessor.calibrated:
        preprocessed_state = preprocessor.transform(state)
        print("prices", state, "raw returns", preprocessed_state)
    preprocessor.calibrate(state)
