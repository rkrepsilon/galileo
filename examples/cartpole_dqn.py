"""
The aim of this file is to demonstrate how we can integrate OpenAI environments with Galileo.
"""
import keras
import numpy as np
from keras.layers import Dense
from keras.optimizers import SGD

from galileo.agents.rl import DQNAgent, PolicyGradientAgent
from galileo.brains import QBrain
from galileo.envs.openaigym import CartPole
from galileo.memories import EpisodicMemory, PGMemory
from galileo.runner import Runner
from galileo.scheduler import Scheduler

environment = CartPole()

feature_block = [
    Dense(24, activation='relu'),
    Dense(24, activation='relu')
]

maxlen = 2

brain = QBrain(
    output_size=environment.n_actions,
    optimizer=SGD(),
    feature_block=feature_block,
    is_dueling=True,
    has_target_model=True)
memory = EpisodicMemory(size=1000, per=False)
agent = DQNAgent(
    brain=brain,
    memory=memory,
    gamma=0.99,
    batch_size=32,
    target_update_frequency=10,
    update_smoothness=0,
    maxlen=maxlen,
    default_action=[1,0],
    is_double=True)
learning_frequency = 1

runner = Runner(env=environment, agent=agent)

n_episodes = 100
schedules = {
    'epsilon':
    Scheduler(
        decay_mode='exponential',
        initial_value=1,
        final_value=0.01,
        n_episodes=n_episodes)
}

runner.train(
    schedules=schedules,
    num_episodes=n_episodes,
    learning_frequency=learning_frequency)

runner.test(num_episodes=1, render=True, render_frequency=1)
