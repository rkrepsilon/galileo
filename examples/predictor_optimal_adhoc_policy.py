"""In this example, we show how to implement a simple Agent
interacting with SpreadTrading with a state preprocessor
"""
import numpy as np

from galileo.core import Agent
from galileo.envs import SpreadTrading
from galileo.iter import TriangularWalk
from galileo.preprocessing import Predictor, Selector, compose, returns
from galileo.utils import calc_spread
from scipy.signal import argrelextrema
from sklearn.ensemble import RandomForestClassifier


def label(y):
    labels = np.zeros_like(y)
    labels[argrelextrema(y, np.greater)] = 2
    labels[argrelextrema(y, np.less)] = 1
    return np.squeeze(labels.astype(int))


lr = RandomForestClassifier()


preprocessor = compose(
    [
        Selector(selection=[0]),
        Predictor(estimator=lr, steps=1000,
                  maxlen=2, target_fun=label, lookahead_vector=[0]),
    ]
)


spread_coefficients = [1]
dtt = SpreadTrading(
    data_iterator=TriangularWalk(nsteps=10, length=1000),
    spread_coefficients=spread_coefficients)
state = dtt.reset()
action_vec = [1, 0, 0]
for _ in range(10000):
    state, reward, done, _ = dtt.step(action_vec)
    if preprocessor.calibrated:
        action = preprocessor.transform(state)
        print("prices", state, "predicted action", action)
        action_vec = np.zeros(3)
        action_vec[action] = 1
        dtt.render()
    preprocessor.calibrate(state)
