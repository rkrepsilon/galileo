"""
The aim of this file is to demonstrate how we can integrate OpenAI environments with Galileo.
"""
import numpy as np

import keras
from galileo.agents.rl import DQNAgent, PolicyGradientAgent
from galileo.brains import PGBrain
from galileo.envs.openaigym import CartPole
from galileo.memories import EpisodicMemory, PGMemory
from galileo.runner import Runner
from galileo.scheduler import Scheduler
from keras.layers import Dense
from keras.optimizers import Adam

environment = CartPole()

feature_block = [
    Dense(24, activation='relu'),
    Dense(24, activation='relu')
]

maxlen = 1

brain = PGBrain(input_shape=(maxlen,) + environment.state_shape,
                output_size=environment.n_actions,
                feature_block=feature_block,
                optimizer=Adam(lr=0.01, clipnorm=False))
memory = PGMemory(size=200)
agent = PolicyGradientAgent(brain=brain,
                            memory=memory,
                            gamma=0.95,
                            advantage_type="advantage",
                            gae_lambda=0.98,
                            maxlen=maxlen,
                            default_action=[1, 0])
learning_frequency = 200

runner = Runner(env=environment, agent=agent)

n_episodes = 1000
schedules = {
    'epsilon':
    Scheduler(
        decay_mode='linear',
        initial_value=0,
        final_value=0,
        n_episodes=n_episodes)
}

runner.train(
    schedules=schedules,
    num_episodes=n_episodes,
    learning_frequency=learning_frequency)

runner.test(num_episodes=1, render=True, render_frequency=1)
