"""In this example, we show how to implement a simple Agent
interacting with SpreadTrading.
"""
import numpy as np
import six
from galileo.core import Agent
from galileo.envs import SpreadTrading
from galileo.iter import TriangularWalk
from galileo.utils import calc_spread


class HighLowAgent(Agent):
    """ Dummy Threshold agent that buys when ask price is -1, sells when bid price is 1
    """
    learner = False

    def act(self, state):
        """ Returns the action to perform depending on the state of the market
            Args:
                state (numpy.array): state of the agent
            Returns
                (numpy.array): action to do if price is out of the buy/sell_threshold boundaries
        """
        # The state is always [bid1, ask1, bid2, ask2 ..., entry_price, flat, long, short]
        # Here we will only have one product, therefore
        # state = numpy.array([bid1, ask1, entry_price, flat, long, short])
        bid_price = state[0]
        ask_price = state[1]
        if ask_price == -1:
            return SpreadTrading._actions['buy']
        elif bid_price == 1:
            return SpreadTrading._actions['sell']
        else:
            return SpreadTrading._actions['hold']


agent = HighLowAgent()

# We have only one product, so the spread is only composed of that particular product.
spread_coefficients = [1]
dtt = SpreadTrading(
    data_iterator=TriangularWalk(nsteps=5, length=100),
    spread_coefficients=spread_coefficients)
state = dtt.reset()

action = agent.act(state)
done = False
while not done:
    state, reward, done, _ = dtt.step(action)
    action = agent.act(state)
    # translating action into a name
    for k, v in six.iteritems(SpreadTrading._actions):
        if (action == v).all():
            action_name = k
    print("Bid ({}) / Ask ({})".format(state[0],
                                       state[1]), " Action is ", action_name)
