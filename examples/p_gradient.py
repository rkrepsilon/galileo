import numpy as np

import matplotlib.pyplot as plt
from galileo.agents.rl import PolicyGradientAgent
from galileo.brains.pgbrain import PGBrain
from galileo.envs import SpreadTrading
from galileo.iter.deterministic import WavySignal
from galileo.memories.pg_memory import PGMemory
from galileo.preprocessing import Identity, Selector, chain, compose, returns
from keras.layers import Activation, Dense, Input
from keras.optimizers import Adam

# Instantiating the environmnent
episodes = 40
episode_length = 2000
maxlen = 1
env_params = {
    "spread_coefficients": [1],
    "data_iterator": WavySignal(period_1=25,
                                period_2=50,
                                epsilon=-0.5,
                                length=episode_length),
    "trading_penalty": .1,
    "exposure_penalty": 0,
    "max_entries": 1
}
environment = SpreadTrading(**env_params)


feature_block = [
    Dense(24, activation='relu'),
    Dense(24, activation='relu')
]

preprocessor = Identity()
memory = PGMemory(1500)
brain = PGBrain(
    output_size=environment.n_actions,
    feature_block=feature_block,
    optimizer=Adam(lr=.01, clipnorm=False),
)

agent_params = {
    'brain': brain,
    'memory': memory,
    'maxlen': maxlen,
    'gamma': 0.92,
    'epsilon': 0,
    "advantage_type": "gae",
    'gae_lambda': 0.98,
    'default_action': [1, 0, 0]
}
agent = PolicyGradientAgent(**agent_params)

learning_frequency = 1501
counter = 0
learned = False
for ep in range(episodes):
    state = environment.reset()

    ep_reward = 0
    for _ in range(episode_length):
        counter += 1
        action = agent.act(state)
        next_state, reward, done, _ = environment.step(action)
        agent.observe(next_state, reward, done)
        state = next_state
        ep_reward += reward
        if (counter % learning_frequency) == 0:
            loss, learned = agent.learn()
        if done:
            agent.end()
    if learned:
        print("Ep:" + str(ep)
              + "| reward: " + str(round(ep_reward, 2))
              + "| avg reward: " + str(round(ep_reward, 2) / episode_length)
              + "| loss: " + str(round(loss['actor_critic_loss'], 5)))

# Running the agent
done = False
state = environment.reset()
while not done:
    action = agent.act(state)
    state, reward, done, _ = environment.step(action)
    agent.observe(state, reward, done)
    environment.render()
