"""In this example, we show how to implement a simple Agent
interacting with SpreadTrading with a state preprocessor
"""
import numpy as np

from galileo.core import Agent
from galileo.envs import SpreadTrading
from galileo.iter import TriangularWalk
from galileo.preprocessing import Predictor, Selector, compose, returns
from galileo.utils import calc_spread
from scipy.signal import argrelextrema
from sklearn.ensemble import RandomForestRegressor

lr = RandomForestRegressor()


preprocessor = compose(
    [
        Selector(selection=[0]),
        Predictor(estimator=lr, steps=1000,
                  maxlen=2, target_fun=lambda x: x, lookahead_vector=[1]),
    ]
)


spread_coefficients = [1]
dtt = SpreadTrading(
    data_iterator=TriangularWalk(nsteps=10, length=1000),
    spread_coefficients=spread_coefficients)
state = dtt.reset()
action_vec = [1, 0, 0]
for _ in range(10000):
    state, reward, done, _ = dtt.step(action_vec)
    if preprocessor.calibrated:
        price = preprocessor.transform(state)
        print("prices", state, "predicted price", price)
        if price > state[0]:
            action_vec = [0, 1, 0]
        elif price < state[0]:
            action_vec = [0, 0, 1]
        else:
            action_vec = [1, 0, 0]
        dtt.render()
    preprocessor.calibrate(state)
