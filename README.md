# ![](https://docs.google.com/drawings/d/1N8OaHxafRoxe8spK75kxZSccA_U5hoXylXtRksYjeN4/pub?w=400)

Galileo is a framework for implementing and testing trading strategies.
It is composed of mainly three components communicating through APIs:
- Data iterators
- Environments
- Agents


## Documentation

https://bitbucket.srv.rkr.hkg/pages/PM/galileo/doc/browse/docs/_build/index.html

## Architecture overview

![](https://docs.google.com/drawings/d/1bL6Gl1hJh0PqsvHh0T5dcqcVrrxP7taN7UUL7a07zEk/pub?w=700)

## Installation

`python setup.py install`

## `Agent`

The `Agent` core class implements three methods:

- `act`: returns action picked by a deterministic algorithm (**must be implemented**).
- `observe`: updates the `frames_seen` counters.
- `end`: adds any logic at the end of an episode.

If you want to implement a simple agent that trades only according to pre-established rules, it must inherit from the `Agent` class which can be found in 'galileo/core.py'. It consists of three basic methods that need to be overridden in order to implement your own logic (Some examples are provided in the `galileo/agents/deterministic/`).

### `RLAgent`

To create your own reinforcement learning agent, it must inherit from the `RLAgent` class which itself inherits from `Agent` and can be found at 'galileo/core.py'. In addition to the the three previous methods, two new methods **must** be overridden :

- `learn`: learning process for the agent.
- `_predict_action`: logic through which the agent decides its `action`.

In addition, note that the `act` and `observe` methods have been overridden:

- `act`: calls `_predict_action` to obtain the `agent`'s `action`, records the `action` and the `state`, and returns the `action`.
- `observe`: updates counters, records the `new_state` of the environment, the `reward` received, and whether the state is `terminal`.

## The trading environment: `SpreadTrading`

`SpreadTrading` is a trading environment allowing to trade a *spread* (see https://en.wikipedia.org/wiki/Spread_trade). We feed the environment a time series of prices (bid and ask) for *n* different products (with a `DataIterator`), as well as a list of *spread coefficients*. The possible actions are then buying, selling or holding the spread. Actions cannot be taken on one or several legs in isolation. The state of the environment is defined as: prices, entry price and position (whether long, short or flat).

![](https://media.giphy.com/media/3ohhwpZvNAime6IMuY/giphy.gif)


## `DataIterator`

To create your own Data iterator, it must inherit from the `DataIterator` base class which can be found in the file 'galileo/core.py'. It consists of four methods. Only the `generator` method which defines the times series needs to be overridden. Example can be found at `examples/iterator_random.py`. For only one product, the `generator` method **must** yield a `(bid, ask)` tuple, one element at a time. For two or more products, you must return a tuple consisting of bid and ask prices for each product, concatenated. For instance for two products, the method should yield `(bid_1, ask_1, bid_2, ask_2)`. The logic for the time series is encoded there.


## `Runner`

The `Runner` class (declared in `galileo/runner.py`) helps orchestrating the interaction between an agent and an environment. It implements three stages of the interaction:

- `warmup`: fills memory if the agent is sequential and needs several previous states before it can take an action.
- `train`: handles the agent's training schedule and learning parameters updates. Not needed for deterministic agents.
- `test`: executes the agent policy and records trades for further statistics. No data for learning is recorded.
- `save_brain`: records the brain weights once training is over.

## Compatibility with OpenAI gym

Our environments API is strongly inspired by OpenAI gym. Therefore it is fairly straightforward to test your agent with any OpenAI environment. An example is given in `examples/cartpole_dqn.py`.
In order to add a OpenAI gym environment, you must implement a wrapper in the file `galileo/envs/openaigym.py`.

## Examples

Some examples are available in `examples/`

To run the `dqn_agent.py` example, you will need to also install keras with `pip install keras`. By default, the backend will be set to Theano. You can also run it with Tensorflow by installing it with `pip install tensorflow`. You then need to edit `~/.keras/keras.json` and make sure `"backend": "tensorflow"` is specified.

## Helper script

A helper script is located in `run.py` and provides a configuration file (json or yaml) interface to Galileo.
It can be run with a config file which contains all the parameters needed by the runner. See for example the `config.yaml` file which can be executed by launching `python run.py -c config.yaml`

## Documentation

The documentation is in the folder `docs`.
In order to update it, run in the root folder:

`sphinx-apidoc -f -o docs/source galileo`

followed by:

`sphinx-build docs/ docs/_build/ -a`

The documentation will be accessible from `docs/_build/index.html`

## Examples

Some examples can be found in `examples/`

- Simple agent (`examples/simple_agent.py`)
- Random agent (`examples/random_agent.py`)
- OpenAI gym integration (`examples/cartpole_dqn.py`)
- Runner example (`examples/runner_demo.py`)
- Policy gradient agent (`examples/p_gradient.py`)

## Contribution cycle

- Contributor
    - Create your own branch with an explicit name for the feature added
        - `git checkout -b feature/name_of_feature`
    - Implement and commit your changes
        - `git add file.py`
        - `git commit -m 'file.py changed'`
    - Create unittest if possible
    - Run all tests:
        - `pytest`
    - Run [YAPF](https://github.com/google/yapf) for automated code formatting:
        - `yapf -i your_changed_file`
    - Run [pylint](https://www.pylint.org/) for static code analysis:
        - `pylint galileo/`
    - Create a pull request on bitbucket from your branch to master
    - Peer review and pair programming
- Reviewer
    - Code review (style and logic)
    - Check for documentation
    - Check for tests
    - Run the code
    - Run the tests
    - Merge

---

*Copyright © 2017 RKR Epsilon UK Ltd. All rights reserved.*
